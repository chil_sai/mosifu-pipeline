pro mos_create_sky_ms,image_type,clipwl=clipwl_inp,$
    objpos=objpos,objbox=objbox,nstages=nstages_inp,$
    dithpos=dithpos,diffmode=diffmode,$
    gain=gain,rdnoise=rdnoise,$
    wdir=wdir,sky_reg_slit=sky_reg_slit,bright=bright,$
    dist_map=dist_map,ccdscl=ccdscl,maskentry=mask,lin_param=lin_param,$
    per_slit=per_slit,adj=adj,$
    sky_slit_length=sky_slit_length_inp,mask_slit_individual=mask_slit_individual,$
    dimensions=dimensions_inp,targbksp=targbksp,boxbksp=boxbksp,$
    skydegx=skydegx_inp,skydegy=skydegy_inp,$
    skylinecorr=skylinecorr,pady=pady,skyline_flux_data_arr=skyline_flux_data_arr,$
    debug=debug,status=status,_Extra=extra_kw

if(n_elements(dithpos) eq 0) then dithpos=(keyword_set(diffmode))? [0.0,0.0] : 0.0
if(n_elements(gain) ne 1) then gain=1.0
if(n_elements(rdnoise) ne 1) then rdnoise=3.0
if(n_elements(clipwl_inp) ne 1) then clipwl_inp=0.0
clipwl = clipwl_inp
if(n_params() eq 1) then image_type='obj'
if(n_elements(skydegx_inp) ne 1) then skydegx_inp=2
skydegx=skydegx_inp
if(n_elements(skydegy_inp) ne 1) then skydegy_inp=4
skydegy=skydegy_inp
if(n_elements(dimensions_inp) ne 1) then dimensions_inp=3
dimensions=dimensions_inp
zeroskyx = where(skydegx eq 0, czeroskyx)
if(czeroskyx ne 0) then dimensions[zeroskyx]=2
if(n_elements(sky_slit_length_inp) ne 1) then sky_slit_length_inp=-1
sky_slit_length=sky_slit_length_inp

if(n_elements(nstages_inp) ne 1) then nstages_inp=1
nstages = nstages_inp

status=0
if(n_elements(dist_map) eq 1) then begin
    if((size(dist_map))[2] eq 8) then $
        if(tag_exist(dist_map,'mask_y_scl')) then y_scl_arr=dist_map.mask_y_scl
endif else y_scl_arr=1.0d

;y_scl_arr=[24.555832d,24.548194d]

n_slits=n_elements(mask)
if (n_slits eq 1) and not tag_exist(mask, 'slit') then begin
    n_slits=0
endif 

f_obj=wdir+image_type+'_slits.fits'
if(file_test(f_obj)) then begin
    f_aobj=(image_type eq 'obj_diff')? wdir+'obj_slits.fits' : f_obj
    f_flat=wdir+'flatn_slits.fits'
    if(keyword_set(skylinecorr)) then f_flat_line_corr=wdir+'flat_emline_slits.fits'
endif else begin
    f_obj=wdir+image_type+'_ff.fits'
    f_aobj=(image_type eq 'obj_diff')? wdir+'obj_ff.fits' : f_obj
    f_flat=wdir+'flat_norm.fits'
    if(keyword_set(skylinecorr)) then f_flat_line_corr=wdir+'flat_emline.fits'
endelse

h_pri=headfits(f_obj)
if(sky_slit_length[0] le 0) then begin
    min_yslit=min(mask.bbox[2*findgen(4)+1],max=max_yslit)
    sky_slit_length[0]=total(max_yslit-min_yslit)*y_scl_arr[0]
    if(n_elements(mask_slit_individual) gt 0) then begin
        mask_slit_idx=where(mask_slit_individual.width gt 1,cmask_slit_idx)
        if(cmask_slit_idx gt 0) then $
            sky_slit_length[0]-=total(2*mask_slit_individual[mask_slit_idx].width)
    endif
endif

if(n_elements(targbksp) ne 1) then targbksp=lin_param.dwl/2.0
if(n_elements(boxbksp) ne 1) then boxbksp=lin_param.dwl/2.0

skydegx_cur=skydegx
skydegy_cur=skydegy
dimensions_cur=dimensions
nstages_cur=nstages
clipwl_cur=clipwl
if(clipwl_cur lt 0.01) then nstages_cur=1
for stg=0,0+2*(nstages_cur-1) do begin
    writesuff=(stg eq 0)? ((nstages_cur eq 1)? '' : '_cnt') : (stg eq 1)? '_blue' : '_red'
    file_out=wdir+'/sky_'+image_type+'_2d_bspl_slits'+writesuff+'.fits'
    writefits,file_out,0,h_pri

    if(dimensions_cur eq 2) then skydegx_cur=0
    if(stg gt 0) then begin
        dimensions_cur=2
        skydegx_cur = 0 ;;-=1
        skydegy_cur = skydegy - 1
        ;skydegx_cur = (skydegx_cur>0)
        skydegy_cur = (skydegy_cur>3)
    endif

    h_obj=headfits(wdir+image_type+'_ff.fits',ext=1)

    nx=sxpar(h_obj,'NAXIS1')
    ny=sxpar(h_obj,'NAXIS2')

    frac_pix_bspl = 10*0.8/(skydegx_cur+1)/(skydegy_cur+1) ;;; every_n/sky_slit_length
    every_n_cur = ((skydegx_cur+1)*(skydegy_cur+1)*5*10) > (0.5*sky_slit_length)

    if(skydegx_cur lt 1) then dimensions_cur=2

    mask_cur=mask
    f_dt=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
    disp_rel=mrdfits(wdir+f_dt,1,/silent)
    bad_disp_rel=where(disp_rel.wl_sol[0] le 0.01, cbad_disp_rel)
    if(cbad_disp_rel gt 0) then disp_rel[bad_disp_rel].wl_sol[*]=!values.d_nan
    disp_rel_out=disp_rel
    if(n_elements(skyline_flux_data_arr) gt 0) then begin
        message,/inf,'Using skyline_flux data array'
        fit_skyline=0
        sl_flux=0d
        for f_s=0,n_elements(skyline_flux_data_arr)-1 do begin
            skyline_flux_data_tmp=skyline_flux_data_arr[f_s].slits
            sl_flux_cur=median(skyline_flux_data_tmp.flux)
            if(sl_flux_cur gt sl_flux) then begin
                skyline_flux_data=skyline_flux_data_tmp
                g_skyline=where((finite(skyline_flux_data.wl) eq 1) and (finite(skyline_flux_data.x_mask) eq 1),cg_skyline)
                if((cg_skyline gt 10) and keyword_set(fit_skyline)) then begin
                    t_wl=robust_poly_fit(double(g_skyline),skyline_flux_data[g_skyline].wl,5)
                    skyline_flux_data[g_skyline].wl=poly(double(g_skyline),t_wl)
                endif
                mean_wl_offset=median(skyline_flux_data.wl-skyline_flux_data.wl_line)
                sl_flux=sl_flux_cur
             endif else cg_skyline=0
        endfor
        for s=0,cg_skyline-1 do begin
            disp_rel[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line) -mean_wl_offset/double(cg_skyline)
            disp_rel_out[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line) -mean_wl_offset/double(cg_skyline)
        endfor
    endif

    wlclip = (clipwl_cur gt 0 and clipwl_cur lt 0.45 and n_elements(mask_cur) gt 1)? lin_param.npix*clipwl_cur*lin_param.dwl : 0.0
    wlmin=lin_param.wl_min + wlclip
    wlmax=lin_param.wl_max - wlclip

    wllim=dblarr(2,ny)
    for y=0,ny-1 do wllim[*,y]=poly([0,4095],disp_rel[y].wl_sol)

    if(clipwl_cur ge 1 and n_elements(mask_cur) gt 1) then begin ;; clipwl_cur in percent (%)
        hist_minwl=histogram(wllim[0,where(finite(wllim[0,*]) eq 1)],nbin=50,locations=xhist_minwl)
        chist_minwl=total(hist_minwl,/cumulative)
        minwl_cut=interpol(xhist_minwl,chist_minwl,chist_minwl[n_elements(chist_minwl)-1]*(100.0-clipwl_cur)/100.0)
        hist_maxwl=histogram(wllim[1,where(finite(wllim[0,*]) eq 1)],nbin=50,locations=xhist_maxwl)
        chist_maxwl=total(hist_maxwl,/cumulative)
        maxwl_cut=interpol(xhist_maxwl,chist_maxwl,chist_maxwl[n_elements(chist_maxwl)-1]*(clipwl_cur)/100.0)
        ; print,100*(maxwl_cut-minwl_cut)/(wlmax-wlmin),'% of the total wavelength range is retained'
        if(stg eq 0) and (nstages gt 1) then begin
            wlmin=minwl_cut
            wlmax=maxwl_cut
        endif
    endif

    ;;wlmin=(wlmin>3000.0)  ;; hardcoded blue limit 
    ;;wlmax=(wlmax<10500.0) ;; hardcoded red limit
    if(stg eq 0) then begin
        if(n_elements(wlmin_stg0) ne 2) then begin
            wlmin_stg0=dblarr(2)
            wlmax_stg0=dblarr(2)
        endif
        wlmin_stg0=wlmin
        wlmax_stg0=wlmax
    endif

    if(stg eq 1) then wlmax=wlmin_stg0+200.0*targbksp
    if(stg eq 2) then wlmin=wlmax_stg0-200.0*targbksp

    print,'Generating sky model at wavelengths: '+string(wlmin,format='(i5)')+' and '+string(wlmax,format='(i5)')+' A'

    comm_targ_sky=0
    comm_box_sky=0

    targ=where(strcompress(mask.type,/remove_all) eq 'TARGET',ctarg,compl=box,ncompl=cbox)

    s_skyreg=size(sky_reg_slit)
    if(s_skyreg[2] ne 8) then begin
        sky_reg_slit=replicate({slit:0,ss_tech:1,$
                                sky_slits:bytarr(n_slits),sky_reg:bytarr(ny)},n_slits)
        targflag=bytarr(Ny)
        boxflag=bytarr(Ny)

        sky_reg_slit[*].slit=mask[*].slit
        for i=0,ctarg-1 do begin
            targflag[where(disp_rel.slit eq mask[targ[i]].slit)]=1
        endfor
        for i=0,ctarg-1 do begin
            sky_reg_slit[targ[i]].sky_reg[where((finite(disp_rel.wl_s_err) eq 1) and (targflag eq 1))]=1
            sky_reg_slit[targ[i]].sky_slits[targ]=1
        endfor

        for i=0,cbox-1 do begin
            sky_reg_slit[box[i]].ss_tech=0
            boxflag[where(disp_rel.slit eq mask[box[i]].slit)]=1
        endfor
        for i=0,cbox-1 do begin
            sky_reg_slit[box[i]].sky_reg[where((finite(disp_rel.y_mask) eq 1) and (boxflag eq 1))]=1
            sky_reg_slit[box[i]].sky_slits[box]=1
        endfor

        comm_targ_sky=1
        comm_box_sky=1
    endif

    sky_model_slit = {slit:0, sky_model:ptr_new()}
    sky_all = replicate(sky_model_slit,n_slits)
    for i=0,n_slits-1 do sky_all[i].sky_model = ptr_new(/allocate_heap)

    targ_sky_created=0
    box_sky_created=0

    ;;read,aaa
    flag_im=bytarr(Nx,Ny)
    y_scl=y_scl_arr
    if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl
    ;if(grating eq '270') then fl_thr=0.03 else fl_thr=0.1
    fl_thr=0.03

    xx=findgen(nx)

    slit_reg=get_slit_region(mask,nx=nx,ny=ny,pady=pady,dist_map=dist_map,slit_trace=slit_trace)

    for k=0,n_slits-1 do begin
    ;    print,'k, tg_sky_c, comm_tg_sky, bx_sky_c, comm_bx_sk=',k,targ_sky_created,comm_targ_sky,box_sky_created,comm_box_sky
        if((targ_sky_created eq 1) and (comm_targ_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'TARGET')) then begin
            *sky_all[k].sky_model=*sky_all[targ[0]].sky_model
            continue
        endif
        if((box_sky_created eq 1) and (comm_box_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'BOX')) then begin
            *sky_all[k].sky_model=*sky_all[box[0]].sky_model
            continue
        endif

        if(sky_reg_slit[k].ss_tech eq 1) then begin ;;; Kelson-style sky subtraction
            print,'Creating an oversampled sky vector for slit=',k,' slit type=',mask[k].type
            sky_reg_cur=where(sky_reg_slit[k].sky_reg eq 1,n_sky)
            xp=dblarr(long(nx)*long(n_sky))
            yp=xp
            zp=xp
            sky_flux=xp*!values.d_nan
            asky_flux=xp*!values.d_nan

            for j=0,n_slits-1 do begin
                if(sky_reg_slit[k].sky_slits[j] eq 0) then continue

                flat_line_slit=(keyword_set(skylinecorr))?mrdfits(f_flat_line_corr,j+1,/silent):1d
                obj_slit=mrdfits(f_obj,j+1,hdr_slit_cur,/silent)/flat_line_slit
                aobj_slit=(f_obj ne f_aobj)? mrdfits(f_aobj,j+1,/silent)/flat_line_slit : obj_slit
                flat_slit=mrdfits(f_flat,j+1,/silent)
                y_off=long(sxpar(hdr_slit_cur,'YOFFSET'))
                ny_cur=long(sxpar(hdr_slit_cur,'NAXIS2'))
                slit_tilt = (sxpar(hdr_slit_cur,'SLITTHET')-sxpar(hdr_slit_cur,'MASKPA')) mod 180.0
                tilted_slit=sxpar(hdr_slit_cur,'TILTEDSL')

                if(keyword_set(bright)) then begin
                    target_offset=(mask_cur[j].y-(mask_cur[j].bbox[5]+mask_cur[j].bbox[1])/2.0)*5.98802 ;; target offset in arcsec, 5.98802=scale in arcsec/mm
                    mask_targ=mos_mask_target(wdir=wdir,dithpos=dithpos,dist_map=dist_map,ccdscl=ccdscl,maskentry=mask,slitnum=j+1,nmasked=nmasked,offset=target_offset,diffmode=diffmode, pady=pady)
                    if(nmasked gt 0) then begin
                        obj_slit[mask_targ]=!values.f_nan
                        aobj_slit[mask_targ]=!values.f_nan
                    endif
                endif

                wlmap_slit=obj_slit*!values.d_nan
                xmaskmap_slit=obj_slit*!values.d_nan
                ymaskmap_slit=obj_slit*!values.d_nan
                ;print, 'slit j, y_off: ', j, y_off
                for i=0L,ny_cur-1L do begin
                    ;if(total(disp_rel[i+y_off].wl_sol) eq 0.) then disp_rel[i+y_off].wl_sol=!values.d_nan
                    ;if((i+y_off) gt 0 and (i+y_off) lt Ny and disp_rel[i+y_off].slit eq j+1l) then begin
                      wlmap_slit[*,i]=poly(xx,disp_rel[i+y_off].wl_sol)
                      xmaskmap_slit[*,i]=disp_rel[i+y_off].x_mask
                      ymaskmap_slit[*,i]=disp_rel[i+y_off].y_mask
                    ;endif
                   endfor
                ;if adj eq 1 then begin
                  wlmap_slit=mos_rectify_slit(wlmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
                  xmaskmap_slit=mos_rectify_slit(xmaskmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
                  ymaskmap_slit=mos_rectify_slit(ymaskmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
                ;endif
                if(n_elements(mask_slit_individual) gt 0) then begin
         ;;;           mask_slit_id=where(mask_slit_individual.side eq sides and $
                    mask_slit_id=where(mask_slit_individual.slit eq mask_cur[j].slit,cmask_slit_id) 
         ;;;                           mask_slit_individual.slit eq j,cmask_slit_id) 
                    for m=0,cmask_slit_id-1 do begin
                        mask_targ=mos_mask_target(wdir=wdir,dithpos=dithpos,dist_map=dist_map,ccdscl=ccdscl,maskentry=mask,slitnum=j+1,$
                            width=mask_slit_individual[mask_slit_id[m]].width,$
                            offset=mask_slit_individual[mask_slit_id[m]].offset,$
                            nmasked=nmasked,diffmode=diffmode, pady=pady)
                        if(nmasked gt 0) then begin
                            obj_slit[mask_targ]=!values.f_nan & print,'mask m=',mask_slit_id[m],mask_slit_individual[mask_slit_id[m]],nmasked
                            aobj_slit[mask_targ]=!values.f_nan
                        endif
                    endfor
                endif

                for i=0L,n_sky-1L do begin
    ;;                        xp[i*nx:(i+1)*nx-1]=poly(xx,disp_rel[sky_reg_cur[i]].wl_sol)
                    if(finite(disp_rel[sky_reg_cur[i]].x_mask) ne 1) then continue

                    cur_slit=disp_rel[sky_reg_cur[i]].slit-1l

                    idx=lindgen(Nx)+long(sky_reg_cur[i]-y_off)*Nx

                    good_idx=where(idx ge 0 and idx lt n_elements(obj_slit),cgood_idx)
                    if(cgood_idx gt 0) then begin
                        good_idx=where((idx ge 0) and (idx lt n_elements(obj_slit)) and (flat_slit[idx] gt fl_thr),cgood_idx)
                        if(cgood_idx gt 0) then begin
                          i_idx=(i*nx+lindgen(Nx))[good_idx]
                          sky_flux[i_idx]=obj_slit[idx[good_idx]]
                          asky_flux[i_idx]=aobj_slit[idx[good_idx]]

                          xp[i_idx]=wlmap_slit[idx[good_idx]]
                          yp[i_idx]=xmaskmap_slit[idx[good_idx]]
                          zp[i_idx]=ymaskmap_slit[idx[good_idx]]

                          flag_im[idx[good_idx]]=1
                          if(keyword_set(debug)) then begin
                              oplot,xp[i_idx],sky_flux[i_idx],psym=-4,col=i,syms=0.3
                              print,'slit,Y=',j,sky_reg_cur[i]
                              ;aaa=''
                              ;if(sky_reg_cur[i] gt 1720 and sky_reg_cur[i] lt 2650) then read,aaa
                              ;if(aaa eq 's') then stop
                          endif
                        endif
                    endif
                endfor
            endfor

            xs=sort(xp)
            x_sky=xp[xs]
            y_sky=yp[xs]
            z_sky=zp[xs]
            sky=sky_flux[xs]
            asky=sky_flux[xs]

            isky= gain^2 / (gain * abs(asky) + rdnoise^2)

            kk=where((finite(sky+isky+y_sky+z_sky) eq 1) and (x_sky gt wlmin) and (x_sky lt wlmax),ckk)
            if(ckk gt 512) then begin
                x_sky=x_sky[kk]
                y_sky=y_sky[kk]
                z_sky=z_sky[kk]
                sky=sky[kk]
                isky=isky[kk]
                n_allsky=long(n_elements(x_sky))
                if stg eq 0 then begin
                   n_allsky_stg0=n_allsky
                   skydegx_stg0=skydegx_cur
                   skydegy_stg0=skydegy_cur
                   dimensions_stg0=dimensions_cur
                endif
                if stg gt 0 then begin
                   stg_frac=(n_allsky/(wlmax -wlmin))/(n_allsky_stg0/(wlmax_stg0 -wlmin_stg0))
                   print, 'Fraction of sky area compared to center: ', stg_frac
                   if stg_frac lt 0.1 then dimensions_cur=1
                   if stg_frac ge 0.48 then begin
                      skydegx_cur=skydegx_stg0
                      skydegy_cur=(skydegy_stg0-1) > 3
                      dimensions_cur=dimensions_stg0
                   endif
                endif else stg_frac=1.0
                bkspace = (strcompress(mask[k].type,/remove_all) eq 'TARGET')? targbksp : boxbksp

                if(keyword_set(debug)) then begin
                    aaa=''
                    read,aaa
                    plot,x_sky,sky,psym=3,xs=1,xtitle='wavelength',ytitle='sky flux',title='Slit #'+string(k,format='(i3)')
                    aaa=''
                    read,aaa
                    if(aaa eq 's') then stop
                endif
                npoly=skydegx_cur ;;;;; (slit_id eq 'mos')? 2 : 3
                n2poly=skydegy_cur
                if(strcompress(mask[k].type,/remove_all) eq 'TARGET') then begin
                    case dimensions_cur of 
                       3: sset = bspline_iterfit_3d(x_sky, sky, x2=y_sky, x3=z_sky, invvar=isky, npoly=npoly, n2poly=n2poly, bkspace=bkspace,requiren=5*(npoly+1)*(n2poly+1), _Extra=extra_kw)
                       2: sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, npoly=n2poly, bkspace=bkspace, requiren=5*(n2poly+1),  _Extra=extra_kw)
                       1: sset = bspline_iterfit(x_sky, sky, invvar=isky, bkspace=bkspace, requiren=10, _Extra=extra_kw)
                       else: sset = 0
                    endcase
                endif else sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, bkspace=bkspace, requiren=10, _Extra=extra_kw)
                print, 'nsky: ', strcompress(string(n_allsky),/remove_all), ' npoly: ', string(npoly,format='(I1)'), ' n2poly: ', string(n2poly,format='(I1)'), ' dimensions_cur: ', string(dimensions_cur,format='(I1)')
                ;if stg eq 1 then stop
                if(total(sset.coeff) eq 0.0) then begin
                    message,/inf,'SKY MODEL FAILED !!! Using everyn instead of bkspace'
                    print, 'everyn: ', strcompress(string(fix(every_n_cur*stg_frac)),/remove_all)
    ;                        if(dimensions_cur eq 3)  then begin
    ;                            message,/inf,'Re-doing the sky model with dimensions_cur=2'
    ;                            dimensions_cur=2
    ;                            sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, npoly=npoly, bkspace=bkspace, _Extra=extra_kw)
                    case dimensions_cur of
                        3: sset = bspline_iterfit_3d(x_sky, sky, x2=y_sky, x3=z_sky, invvar=isky, npoly=npoly, n2poly=n2poly, everyn=fix(every_n_cur*stg_frac), _Extra=extra_kw)
                        2:  sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, npoly=n2poly, everyn=fix(every_n_cur*stg_frac), _Extra=extra_kw)
                        1:  sset = bspline_iterfit(x_sky, sky, invvar=isky, everyn=fix(every_n_cur*stg_frac), _Extra=extra_kw)
                        else: sset = bspline_iterfit(x_sky, sky, invvar=isky, everyn=fix(every_n_cur*stg_frac), _Extra=extra_kw)
                    endcase
                    if(total(sset.coeff) eq 0.0) then begin
                        message,/inf,'SKY MODEL FAILED WITH everyn !!!'
                        status=1
                    endif else status=-1
                endif
                *sky_all[k].sky_model=sset
            endif else *sky_all[k].sky_model=0.0
        endif else *sky_all[k].sky_model=0.0

        if((comm_targ_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'TARGET')) then targ_sky_created=1
        if((comm_box_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'BOX')) then box_sky_created=1
    endfor

    if(keyword_set(debug)) then writefits,wdir+'flag_im.fits',flag_im

    if(status eq 0) then $
        print,'Sky successfully created for all slits' else begin
        print,'Sky model failed at least in one slit'
    endelse

    sxaddpar,h_pri,'SKYSTAT',status,' status of the sky model 0=ok'
    sxaddpar,h_pri,'SKYDEGX',skydegx_cur,' X degree of b-spline parametrization'
    sxaddpar,h_pri,'SKYDEGY',skydegy_cur,' Y degree of b-spline parametrization'

    ;;;n_sky=long(n_elements(skyreg))
    for k=0,n_slits-1 do begin
        flat_line_slit=(keyword_set(skylinecorr))?mrdfits(f_flat_line_corr,k+1,/silent):1d
        obj_slit=mrdfits(f_obj,k+1,hdr_slit_cur,/silent)/flat_line_slit
        skymodel=obj_slit*0.0+!values.f_nan
        y_off=long(sxpar(hdr_slit_cur,'YOFFSET'))
        ny_cur=long(sxpar(hdr_slit_cur,'NAXIS2'))
        slit_tilt = (sxpar(hdr_slit_cur,'SLITTHET')-sxpar(hdr_slit_cur,'MASKPA')) mod 180.0
        tilted_slit=sxpar(hdr_slit_cur,'TILTEDSL')
        wlmap_slit=obj_slit*!values.d_nan
        xmaskmap_slit=obj_slit*!values.d_nan
        ymaskmap_slit=obj_slit*!values.d_nan
        ;print, 'slit, y_off:', k, y_off
        for i=0L,ny_cur-1L do begin
            ;if(total(disp_rel[i+y_off].wl_sol) eq 0.) then disp_rel[i+y_off].wl_sol=!values.d_nan
            ;if((i+y_off) gt 0 and (i+y_off) lt Ny and disp_rel[i+y_off].slit eq k+1l) then begin
              wlmap_slit[*,i]=poly(xx,disp_rel[i+y_off].wl_sol)
              xmaskmap_slit[*,i]=disp_rel[i+y_off].x_mask
              ymaskmap_slit[*,i]=disp_rel[i+y_off].y_mask
            ;endif
        endfor
        ;if adj eq 1 then begin
          wlmap_slit=mos_rectify_slit(wlmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
          xmaskmap_slit=mos_rectify_slit(xmaskmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
          ymaskmap_slit=mos_rectify_slit(ymaskmap_slit,dist_map,y_off,edge=~tilted_slit,inverse=1)
        ;endif
        print,'Slit #',k+1,'/',n_slits
        if(sky_reg_slit[k].ss_tech eq 1) then begin ;;; Kelson-style sky subtraction
            print,'Creating 2D sky model:',format='(a)'
                if((size(*sky_all[k].sky_model))[2] eq 8) then begin
                    if(strcompress(mask[k].type,/remove_all) eq 'TARGET') then begin
                    ; if((strcompress(mask[k].type,/remove_all) eq 'TARGET') and (slit_id eq 'mos')) then begin
                        case dimensions_cur of 
                           3: skymodel = bspline_valu_3d(wlmap_slit, $
                               x2=xmaskmap_slit, x3=ymaskmap_slit, *sky_all[k].sky_model)
                           2:  skymodel = bspline_valu(wlmap_slit, x2=ymaskmap_slit, *sky_all[k].sky_model)
                           1:  skymodel = bspline_valu(wlmap_slit, *sky_all[k].sky_model)
                           else: skymodel = bspline_valu(wlmap_slit, *sky_all[k].sky_model)
                        endcase
                    endif else begin
                        skymodel = bspline_valu(wlmap_slit, x2=ymaskmap_slit, *sky_all[k].sky_model)
                    endelse
                    badwl=where((wlmap_slit lt wlmin) or (wlmap_slit gt wlmax), cbadwl)
                    if(cbadwl gt 0) then skymodel[badwl]=!values.d_nan
                endif else skymodel=!values.d_nan
        endif else begin
            skymodel = obj_slit*0.0
        endelse
        mwrfits,float(skymodel),file_out,hdr_slit_cur,/silent
    endfor
    for i=0,n_slits-1 do ptr_free,sky_all[i].sky_model
endfor

if(nstages_cur gt 1) then mos_combine_sky_ms,logfile,image_type,nstages=nstages

end
