function distortion_map_generic,filename,gzip=gzip,$
    deg1=deg1,deg2=deg2,diff=diff,force=force,extnum=extnum,$
    cutx1=cutx1,cutx2=cutx2,cuty1=cuty1,cuty2=cuty2,$
    nxgrid=nxgrid,nygrid=nygrid,corr_thr=corr_thr,oned=oned,longslit=longslit,$
    detect_slits=detect_slits,maskentry=mask,mask_y_scl=mask_y_scl,min_slit_length=min_slit_length,$
    ystretch=ystretch,yshift=yshift,dy_ext=dy_ext,fullprof=fullprof

suff=(keyword_set(gzip))? '.gz' : ''
if(n_elements(extnum) ne 1) then extnum=0

shortspec=0
dx_prof=2

if(n_elements(deg1) ne 1) then deg1=(shortspec eq 1)? 2 : 3
if(n_elements(deg2) ne 1) then deg2=(shortspec eq 1)? 1 : 3
if(keyword_set(longslit)) then deg2=1
if(keyword_set(longslit)) then oned=0
if(n_elements(corr_thr) ne 1) then corr_thr=0.8
if(n_elements(dy_ext) ne 1) then dy_ext=1d

if(n_elements(cutx1) ne 1) then cutx1=0
if(n_elements(cutx2) ne 1) then cutx2=0
if(n_elements(cuty1) ne 1) then cuty1=0
if(n_elements(cuty2) ne 1) then cuty2=0

if(n_elements(ystretch) ne 1) then ystretch=1d
if(n_elements(mask_y_scl) ne 1) then mask_y_scl=1d
if(n_elements(yshift) ne 1) then yshift=0d
if(n_elements(min_slit_length) ne 1) then min_slit_length=30.0

img=mrdfits(filename+suff,extnum,himg,/silent)
Nx=sxpar(himg,'NAXIS1')-cutx1-cutx2
Ny=sxpar(himg,'NAXIS2')-cuty1-cuty2

if(n_elements(nxgrid) ne 1) then nxgrid=16L
wx=(shortspec eq 1 or keyword_set(longslit))? 120 : 40
p_ref=median(img[Nx/2-wx:Nx/2+wx,*],dim=1)
maxp=max((median(p_ref,19))[0.2*Ny:0.8*Ny],/nan)
cut_bslit=(median(p_ref[dx_prof:dx_prof*3.0]) gt 0.7*maxp)? 1 : 0
cut_tslit=(median(p_ref[sxpar(himg,'NAXIS2')-1-dx_prof*3.0:sxpar(himg,'NAXIS2')-1-dx_prof]) gt 0.7*maxp)? 1 : 0
prof_mid=(shift(p_ref,dx_prof)-shift(p_ref,-dx_prof))/maxp
if(~keyword_set(fullprof)) then p_ref=abs(shift(p_ref,dx_prof)-shift(p_ref,-dx_prof))

if(keyword_set(detect_slits)) then begin
    prof_mid[0:4*dx_prof-1]=0.0
    prof_mid[sxpar(himg,'NAXIS2')-1-4*dx_prof:*]=0.0
    bprof_mid=where(finite(prof_mid) ne 1,cbprof_mid)
    if(cbprof_mid gt 0) then prof_mid[bprof_mid]=0.0
    prof_mid_pos = (prof_mid > 0)
    prof_mid_neg = ((-prof_mid) > 0)

    find_1d,prof_mid_pos,x_mid_pos,flux,sharp,0.1,5.0,[0.05,1.0],/silent
    if(n_elements(x_mid_pos) gt 0) then x_mid_pos=filter_1d(x_mid_pos,min_slit_length)
    for j=0,n_elements(x_mid_pos)-1 do begin
        if(x_mid_pos[j] gt 7 and x_mid_pos[j] lt sxpar(himg,'NAXIS2')-8) then begin
            xx=findgen(15)-7+fix(x_mid_pos[j])
            prof_frag=prof_mid_pos[fix(x_mid_pos[j])-7:fix(x_mid_pos[j])+7]
            prof_frag = (prof_frag - 0.25*max(prof_frag)) > 0
            t=mpfitpeak(/gauss,/positive,xx,prof_frag,g_coeff,nterm=4)
            x_mid_pos[j]=g_coeff[1]
        endif
    endfor

    find_1d,prof_mid_neg,x_mid_neg,flux,sharp,0.1,5.0,[0.05,1.0],/silent
    if(n_elements(x_mid_neg) gt 0) then x_mid_neg=filter_1d(x_mid_neg,min_slit_length)
    for j=0,n_elements(x_mid_neg)-1 do begin
        if(x_mid_neg[j] gt 7 and x_mid_neg[j] lt sxpar(himg,'NAXIS2')-8) then begin
            xx=findgen(15)-7+fix(x_mid_neg[j])
            prof_frag=prof_mid_neg[fix(x_mid_neg[j])-7:fix(x_mid_neg[j])+7]
            prof_frag = (prof_frag - 0.25*max(prof_frag)) > 0
            t=mpfitpeak(/gauss,/positive,xx,prof_frag,g_coeff,nterm=4) ; & oplot,prof_frag,col=j*15+30
            x_mid_neg[j]=g_coeff[1]
        endif
    endfor

    if(cut_bslit eq 1) then begin
        message,/inf,'The bottom slit extends beyond the edge of the detector'
        x_mid_neg=[0.0-20.0,x_mid_neg]
    endif
    if(cut_tslit eq 1) then begin
        message,/inf,'The top slit extends beyond the edge of the detector'
        x_mid_pos=[x_mid_pos,sxpar(himg,'NAXIS2')-1+20.0]
    endif

    n_slits=((n_elements(x_mid_pos))<(n_elements(x_mid_neg)))

    mask=(n_slits gt 0)? replicate({slit:0,ra:0d,dec:0d,x:0d,y:0d,$
            target:0ll,object:'OBJECT',type:'TARGET',$
            wstart:0.0,wend:0.0,height:float(Ny),width:1.0,offset:0.0,theta:0.0,$
            bbox:dblarr(8),$
            mask_id:-1l,mask_name:'long_1.0arcsec',mask_ra:0d,mask_dec:0d,$
            mask_pa:0d,corners:[-sxpar(himg,'NAXIS1')/2.0,-sxpar(himg,'NAXIS2')/2.0,sxpar(himg,'NAXIS1')/2.0,sxpar(himg,'NAXIS2')/2.0]},n_slits) : -1

    for j=0,n_slits-1 do begin
        mask[j].slit=j+1L
        mask[j].y=-((x_mid_neg[j]+x_mid_pos[j])-sxpar(himg,'NAXIS2'))/2.0/mask_y_scl
        mask[j].object='OBJECT'+string(j+1,format='(i3.3)')
        mask[j].height=abs(x_mid_pos[j]-x_mid_neg[j])/mask_y_scl
        mask[j].width/=mask_y_scl
        mask[j].bbox=[-0.5,-1-x_mid_pos[j]+sxpar(himg,'NAXIS2')/2.0,$
                       0.5,-1-x_mid_pos[j]+sxpar(himg,'NAXIS2')/2.0,$
                       0.5,-1-x_mid_neg[j]+sxpar(himg,'NAXIS2')/2.0,$
                      -0.5,-1-x_mid_neg[j]+sxpar(himg,'NAXIS2')/2.0]/mask_y_scl
        mask[j].corners/=mask_y_scl
    endfor
    yshift=0.0 ;(n_slits gt 0)? min(x_mid_neg) : 0.0
endif

skippix=0
if(n_elements(nygrid) ne 1) then nygrid=deg2+3
nsegments=(nygrid ge deg2+3)? nygrid : deg2 + 3
if(keyword_set(longslit)) then begin
    nsegments=2
    nygrid=2
endif
if(keyword_set(oned)) then begin
    nsegments=1
    nygrid=5
    deg2=1
endif

overlap=0.2
z=(keyword_set(diff))? 20.0 : 10.0
dy_amp = (shortspec eq 1)? 15.0 : 35.0
dy=-dy_amp+findgen(2.0*dy_amp*z+1L)/z

dyarr=dblarr(nxgrid,nsegments)
c_arr=dyarr
xgrid=dblarr(nxgrid)
ygrid=dblarr(nsegments)
for xx=fix(nxgrid/2),nxgrid-1 do begin
    xcur=(xx+0.5)*(double(Nx)/nxgrid)
    xgrid[xx]=xcur
    p_cur=median(img[((xcur-wx)>0):((xcur+wx)<(Nx-1)),*],dim=1)
    sat_p_cur=where(finite(p_cur) ne 1, csat_p_cur) ;;; saturated values
    if(csat_p_cur gt 0) then p_cur[sat_p_cur]=max(p_cur,/nan)
    if(~keyword_set(fullprof)) then p_cur=abs(shift(p_cur,dx_prof)-shift(p_cur,-dx_prof))

    if(xx eq fix(nxgrid/2)) then begin
        xref_min=xcur-wx
        xref_max=xcur+wx
        prof_ref = p_cur
    endif
    if (keyword_set(diff) and (xx eq fix(nxgrid/2))) then begin
        p_ref = p_cur
        continue
    endif
    for i=0,nsegments-1 do begin
        ymin = (i eq 0)? skippix : (double(i)-overlap)*Ny/nsegments
        ymax = (i eq nsegments-1)? Ny-skippix-1 : (double(i)+1.0+overlap)*Ny/nsegments
        if(ymin lt 0) then ymin=0
        if(ymax ge Ny) then ymax=Ny-1
        ymin=fix(ymin)
        ymax=fix(ymax)
        ygrid[i]=(ymin+ymax)/2.0
        vec1=rebin(p_ref[ymin:ymax],(ymax-ymin+1L)*z)
        vec2=rebin(p_cur[ymin:ymax],(ymax-ymin+1L)*z)
        cf=c_correlate(vec1-min(vec1),vec2-min(vec2),dy*z)
        nn=max(cf,cnn)
        if((cnn eq 0) or (cnn eq (n_elements(dy)-1L)) or (nn lt corr_thr)) then begin
            dyarr[xx,i]=!values.f_nan
            c_arr[xx,i]=!values.f_nan
        endif else begin
            dyarr[xx,i]=(keyword_set(diff))? dy[cnn] : dy[cnn]
            c_arr[xx,i]=nn
        endelse
    endfor

    if(keyword_set(diff)) then p_ref=p_cur
endfor

for xx=fix(nxgrid/2),0,-1 do begin
    xcur=(xx+0.5)*(double(Nx)/nxgrid)
    xgrid[xx]=xcur
    p_cur=median(img[((xcur-wx)>0):((xcur+wx)<(Nx-1)),*],dim=1)
    sat_p_cur=where(finite(p_cur) ne 1, csat_p_cur) ;;; saturated values
    if(csat_p_cur gt 0) then p_cur[sat_p_cur]=max(p_cur,/nan)
    if(~keyword_set(fullprof)) then p_cur=abs(shift(p_cur,dx_prof)-shift(p_cur,-dx_prof))
    if (keyword_set(diff) and (xx eq fix(nxgrid/2))) then begin
        p_ref = p_cur
        continue
    endif
    for i=0,nsegments-1 do begin
        ymin = (i eq 0)? skippix : (double(i)-overlap)*Ny/nsegments
        ymax = (i eq nsegments-1)? Ny-skippix-1 : (double(i)+1.0+overlap)*Ny/nsegments
        if(ymin lt 0) then ymin=0
        if(ymax ge Ny) then ymax=Ny-1
        ymin=fix(ymin)
        ymax=fix(ymax)
        ygrid[i]=(ymin+ymax)/2.0
        vec1=rebin(p_ref[ymin:ymax],(ymax-ymin+1L)*z)
        vec2=rebin(p_cur[ymin:ymax],(ymax-ymin+1L)*z)
        cf=c_correlate(vec1,vec2,dy*z)
;        oplot,dy,cf,col=30*(i+1)
        nn=max(cf,cnn)
        if((cnn eq 0) or (cnn eq (n_elements(dy)-1L)) or (nn lt corr_thr)) then begin
            dyarr[xx,i]=!values.f_nan
            c_arr[xx,i]=!values.f_nan
        endif else begin
            dyarr[xx,i]=(keyword_set(diff))? -dy[cnn] : dy[cnn]
            c_arr[xx,i]=nn
        endelse
    endfor

    if(keyword_set(diff)) then p_ref=p_cur
endfor

if(keyword_set(oned)) then begin
    nsegments=nygrid
    dyarr=rebin(dyarr,nxgrid,nsegments)
    ygrid=skippix+(dindgen(nsegments)+0.5)*(Ny-1-skippix)/nsegments
endif

if(keyword_set(diff)) then begin
    dyarr[fix(nxgrid/2),*]=(dyarr[fix(nxgrid/2)+1,*]+dyarr[fix(nxgrid/2)-1,*])/2.0
    dyarr_orig=dyarr
    for i=0,nsegments-1 do begin
        gg=where(finite(dyarr_orig[*,i]) eq 1,cgg)
        k_t = robust_poly_fit(xgrid[gg],dyarr_orig[gg,i]/(xgrid[1]-xgrid[0]),2)
        k_t_int = [0,k_t]/[1.0,1.0,2.0,3.0]
        dyarr[*,i]=poly(xgrid,k_t_int)
    endfor
    dyarr=dyarr-median(dyarr[fix(nxgrid/2)-1:fix(nxgrid/2),*])
endif

if(keyword_set(longslit)) then begin
    dyarr_orig=dyarr
    dyarr=(congrid(dyarr_orig,nxgrid,7,/interp))[*,0:4]
    c_arr=(congrid(c_arr,nxgrid,7,/interp))[*,0:4]
    nsegments=5
    ygrid=(dindgen(nsegments)+0.5-overlap)*Ny/nsegments
endif

xgrid2d=((xgrid+cutx1) # (dblarr(n_elements(ygrid))+1.0))
ygrid2d=((dblarr(n_elements(xgrid))+1.0) # (ygrid+cuty1))
g=where(finite(dyarr) eq 1,cg)
if(cg gt 0) then begin
    darr=dblarr(3,cg)
    darr[0,*]=xgrid2d[g]
    darr[1,*]=ygrid2d[g]
    darr[2,*]=dyarr[g]
    darr[2,*]+=((ygrid2d-total(ygrid2d[0,*])/n_elements(ygrid))*(1.0-ystretch) - yshift)[g]

    e=sfit_2deg(darr,deg1,deg2,kx=kx,/irr,/max)

    polywarp,xgrid2d,ygrid2d,$
         xgrid2d,ygrid2d-poly2d(xgrid,ygrid,kx,deg1=deg1,deg2=deg2),$
         3,kxwr,kywr

    kxwr*=0d
    kxwr[0,1]=1d

    polywarp,xgrid2d,ygrid2d-poly2d(xgrid,ygrid,kx,deg1=deg1,deg2=deg2),$
         xgrid2d,ygrid2d,$
         3,kxwr_inv,kywr_inv
    kxwr_inv*=0d
    kxwr_inv[0,1]=1d

    kx[0]-=dy_ext
    kywr[0,0]+=dy_ext
    kywr_inv[0,0]-=dy_ext

    res_str={XGRID:xgrid, YGRID:ygrid, $
         KXWRAP:kxwr,KYWRAP:kywr, $
         KXWRAP_INV:kxwr_inv,KYWRAP_INV:kywr_inv, $
         dyarr:dyarr, corrarr:c_arr,$
         kx_2dfit:kx,deg1:deg1,deg2:deg2,mask_dy0:yshift,mask_y_scl:mask_y_scl}
endif else res_str=-1

return,res_str
end
