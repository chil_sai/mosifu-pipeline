pro eval_hyperbola, x, a, f, pder
    f = a[1l] / (x - a[0l]) + a[2l]
    if (n_params() ge 4l) then pder = [[a[1l]/(x-a[0l])/(x-a[0l])], [1./(x-a[0l])], [fltarr(n_elements(x))+1.]]
end

function eval_gauss_hermite, x, pars
	npars=n_elements(pars)
	w = (x - pars[1])/pars[2]
    w2 = w^2
    flux = pars[0]*exp(-0.5d*w2)/(sqrt(2d*!dpi)*pars[2])
    ; Hermite polynomials normalized as in Appendix A of van der Marel & Franx (1993).
    ; These coefficients are given e.g. in Appendix C of Cappellari et al. (2002)
    if(npars gt 3) then begin
        polyn = 1d + pars[3]/Sqrt(3d)*(w*(2d*w2-3d)) $       ; H3
                  + pars[4]/Sqrt(24d)*(w2*(4d*w2-12d)+3d)   ; H4
        if(npars eq 7) then $
            polyn = polyn + pars[5]/Sqrt(60d)*(w*(w2*(4d*w2-20d)+15d)) $      ; H5
                        + pars[6]/Sqrt(720d)*(w2*(w2*(8d*w2-60d)+90d)-15d)  ; H6
        flux = flux*polyn
    endif
	return, flux
end

pro comp_res_emline, fileflux, dirout, pos_lines=pos_lines, extnum=extnum, moments=moments, $
        hypdis=hypdis, deg=deg, veldeg=veldeg, disdeg=disdeg, h3deg=h3deg, h4deg=h4deg, $
        slit_pos=slit_pos, slit_win=slit_win, skippix=skippix, nsig=nsig, nstd=nstd, ndwl=ndwl,  wl=wl,$
		wave_arr=wave_arr, dis_arr=dis_arr, h3_arr=h3_arr, h4_arr=h4_arr,$
		velmap=velmap, dismap=dismap, h3map=h3map, h4map=h4map, plot=plot

    c0 = 2.99792458d5 ;(c); [km/s]

    if (n_elements(extnum) ne 1l) then extnum = 1
    if (n_elements(skippix) ne 1l) then skippix = 20
    if (n_elements(hypdis) ne 1l) then hypdis = 1
    if (n_elements(deg) ne 1l) then deg = 2
    if (n_elements(veldeg) ne 1l) then veldeg = deg
    if (n_elements(disdeg) ne 1l) then disdeg = deg
    if (n_elements(h3deg) ne 1l) then h3deg = deg
    if (n_elements(h4deg) ne 1l) then h4deg = deg

	inp_flux = mrdfits(fileflux, extnum, h)

	if (~keyword_set(wl)) then parse_spechdr, h, wl=wl
    nwl = n_elements(wl)
    nx = sxpar(h, 'naxis1')
    ny = sxpar(h, 'naxis2')
    dwl = sxpar(h, 'cdelt1')
    velmap_out =[[]]
    dismap_out = [[]]
    h3map_out = [[]]
    h4map_out = [[]]

for i=0, n_elements(slit_pos)-1 do begin
	if (i eq 0) then flux = inp_flux[skippix:*,slit_pos[i]:slit_pos[i]+slit_win]
	if (i eq nx-1) then flux = inp_flux[skippix:*,slit_pos[i]:slit_pos[i]]
	if (i gt 0 and i lt nx-1) then flux = inp_flux[skippix:*,slit_pos[i]:slit_pos[i]+slit_win]
	s_f = size(flux)

	wl=wl[skippix:*]

	spec = total(flux, 2, /nan)

	if (~keyword_set(pos_lines)) then begin
		find_peak, wl, spec, stddev(spec[skippix:*], /nan)*nstd, pix_peak, x_peak, y_peak, bgr_peak, n_peak
		pos_lines = x_peak
	endif else n_peak=n_elements(pos_lines)

	wave_arr = []
    vel_arr = []
    dis_arr = []
    h3_arr = []
    h4_arr = []

	for k=0, n_peak-1 do begin

		idx = where(wl gt pos_lines[k]-ndwl*dwl and wl lt pos_lines[k]+ndwl*dwl)

		if (n_elements(idx) le 6) then continue

		line_flux = spec[idx]
		line_wl = wl[idx]

		fit_ini = gaussfit(line_wl, line_flux, coef, nterms=4)
		resid = line_flux-fit_ini

        ;if((max(line_flux) lt 0.*stddev(resid)) or (coef[0] le 0.) or (max(line_flux) gt stddev(spec[skippix:*], /nan)*8)) then begin
		;	print, '================================',k,'===================================='
		;	continue
		;endif
		line_flux = line_flux-coef[3]
	
        if (keyword_set(plot)) then begin	
			plot, line_wl, line_flux, /xs, /yno
        	oplot, line_wl, fit_ini-coef[3], color=170.
		endif

		if (moments eq 2) then begin
			parinfo=[$
				{value:coef[0],fixed:0,limited:[0,0], limits:[0d, max(line_flux)*3d]},$
				{value:coef[1],fixed:0,limited:[0,0], limits:[min(line_wl), max(line_wl)]},$
				{value:coef[2],fixed:0,limited:[0,0], limits:[0d,max(line_wl)-min(line_wl)]}]
		endif
        if (moments eq 4) then begin
            parinfo=[$
                {value:coef[0],fixed:0,limited:[0,0], limits:[0d, max(line_flux)*3d]},$
                {value:coef[1],fixed:0,limited:[0,0], limits:[min(line_wl), max(line_wl)]},$
                {value:coef[2],fixed:0,limited:[0,0], limits:[0d,max(line_wl)-min(line_wl)]},$
				{value:0d,fixed:0,limited:[1,1], limits:[-0.5d,0.5d]},$
                {value:0d,fixed:0,limited:[1,1], limits:[-0.5d,0.5d]}]
		endif

		fit_params = mpfitfun('eval_gauss_hermite', line_wl, line_flux, sqrt(abs(line_flux)), parinfo=parinfo, /nan, niter=niter, /quiet)
		
		fit = eval_gauss_hermite(line_wl, fit_params)

		if (moments eq 4) then begin
        	;if(abs(fit_params[3]) gt 0.2 or abs(fit_params[4]) gt 0.2) then begin
        	;    print, '================================',k,'===================================='
    	    ;    continue
	        ;endif

			if (k eq 0) then     print, '        wl_cent         sig_lsf        h3            h4'
			print, fit_params[1], fit_params[2]/fit_params[1]*c0, fit_params[3], fit_params[4]
		endif
		if (moments eq 2) then begin
            if (k eq 0) then     print, '        wl_cent         sig_lsf'
            print, fit_params[1], fit_params[2]/fit_params[1]*c0
		endif

		if (keyword_set(plot)) then begin
			plot, line_wl, line_flux, /xs, /yno
			oplot, line_wl, fit, color=170.
		endif

		wave_arr = [wave_arr,fit_params[1]]
		dis_arr = [dis_arr, fit_params[2]/fit_params[1]*c0]
		if (moments eq 4) then begin
			h3_arr = [h3_arr, fit_params[3]]
			h4_arr = [h4_arr, fit_params[4]]
		endif
	endfor

	dis_arr_filt_coef = poly_fit(wave_arr, dis_arr, 2)
	dis_arr_filt = poly(wave_arr, dis_arr_filt_coef)
	dis_resid = dis_arr-dis_arr_filt
	gidx = where(abs(dis_resid) lt nsig*robust_sigma(dis_resid), cgidx)

	wave_arr = wave_arr[gidx]
    dis_arr = dis_arr[gidx]
    vel_arr = fltarr(cgidx)

    ;writefits, dirout+'wlarr.fts', wave_arr
    ;writefits, dirout+'velarr.fts', vel_arr
    ;writefits, dirout+'disarr.fts', dis_arr

    if (moments eq 4) then begin
        h3_arr = h3_arr[gidx]
        h4_arr = h4_arr[gidx]
		;writefits, dirout+'h3arr.fts', h3_arr
		;writefits, dirout+'h4arr.fts', h4_arr

        h3_coef = poly_fit(wave_arr, h3_arr, h3deg)
        h3map = poly(wl, h3_coef)

        h4_coef = poly_fit(wave_arr, h4_arr, h4deg)
        h4map = poly(wl, h4_coef)

		h3map_out = [h3map_out, [h3map]]
		h4map_out = [h4map_out, [h4map]]
    endif

    velmap = fltarr(nwl)

    if (keyword_set(hypdis)) then begin
        x = [min(wave_arr), mean(wave_arr), max(wave_arr)]
        y = [max(dis_arr), mean(dis_arr), min(dis_arr)]
        d = (y[0l] - y[1l]) / (y[0l] - y[2l]) * (x[2l] - x[0l]) / (x[1l] - x[0l])
        a = (x[2l] - d * x[1l]) / (1. - d)
        b = (x[0l] - a) * (x[1l] - a) * (y[0l] - y[1l]) / (x[1l] - x[0l])
        c = y[2l] - b / (x[2l] - a)
        coef = [a, b, c]
        fit = curvefit(wave_arr, dis_arr, 1./abs(dis_arr), coef, coef_err, chisq=chisqr, /double, function_name='eval_hyperbola')
        eval_hyperbola, wl, coef, dismap
    endif else begin
        dis_coef = poly_fit(wave_arr, dis_arr, disdeg)
        dismap = poly(wl, dis_coef)
    endelse

	dismap_out = [dismap_out, [dismap]]
    velmap_out = [velmap_out, [velmap]]

    if (keyword_set(plot)) then begin
        plot, wave_arr, dis_arr, /xs, /yno, color=170
        oplot, wl, dismap
    endif
endfor

    writefits, dirout+'wl.fts', wl
    writefits, dirout+'velmap.fts', velmap_out
    writefits, dirout+'dismap.fts', dismap_out

	if (moments eq 4) then begin
		writefits, dirout+'h3map.fts', h3map_out
        writefits, dirout+'h4map.fts', h4map_out
	endif else begin
        writefits, dirout+'h3map.fts', fltarr(nwl)
        writefits, dirout+'h4map.fts', fltarr(nwl)
    endelse
end
