function mos_mask_target,$
    wdir=wdir,dithpos=dithpos_inp,$
    slitnum=slitnum,width=width,$
    dist_map=dist_map,maskentry=mask,$
    offset=offset,pady=pady,$
    ccdscl=ccdscl,$
    diffmode=diffmode,linearised=linearised,$
    nmasked=nmasked,maskarray=maskarray

if(n_elements(slitnum) ne 1) then slitnum=1
if(n_elements(offset) ne 1) then offset=0.0

if(n_elements(width) ne 1) then width=4.0 ;;sxpar(log,'EXTAPW') ; defaults to the extraction aperture


if(n_elements(dithpos_inp) ne 1) then dithpos_inp=0.0
dithpos=dithpos_inp
;dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0

if(keyword_set(diffmode)) then begin
    if(n_elements(dithpos) ne 2) then message,'dithpos must have 2 elements if /diffmode is enabled'
;    val2=sxpar(log,'DITHPOS2',count=cntval2)
;    dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
;    dithpos=[dithpos,dithpos2]
endif

if(n_elements(ccdscl) ne 1) then ccdscl=0.24436 ;;; 0.24436 arcsec per pix -- hardcoded CCD scale

dithpos=dithpos+offset


if(keyword_set(linearised)) then begin
    im_cur=mrdfits(wdir+(file_test(wdir+'obj_slits_lin.fits')? 'obj_slits_lin.fits' : 'obj_lin.fits'), slitnum,h_cur,/silent)
    maskarray=byte(im_cur*0)
endif else begin
    im_cur=mrdfits(wdir+(file_test(wdir+'obj_slits.fits')? 'obj_slits.fits' : 'obj_ff.fits'), slitnum,h_cur,/silent)
    maskarray=byte(im_cur*0)

    slitnum_off=0
    nx=sxpar(h_cur,'NAXIS1')
    ny=sxpar(h_cur,'NAXIS2')
    slit_reg= get_slit_region(mask,nx=nx,ny=ny,dist_map=dist_map,slit_trace=slit_trace, pady=pady)

    cur_y_tr= 0 > (slit_trace[slitnum-slitnum_off-1].y_trace - sxpar(h_cur,'YOFFSET') + dithpos[0]/ccdscl) < (sxpar(h_cur,'NAXIS2')-1)
    maskarray_tmp=maskarray
    for i=0,sxpar(h_cur,'NAXIS1')-1 do maskarray_tmp[i,round(cur_y_tr[i])]=1

    if(keyword_set(diffmode)) then begin
        cur_y_tr2= 0 > (slit_trace[slitnum-slitnum_off-1].y_trace - sxpar(h_cur,'YOFFSET') + dithpos[1]/ccdscl) < (sxpar(h_cur,'NAXIS2')-1)
        for i=0,sxpar(h_cur,'NAXIS1')-1 do maskarray_tmp[i,round(cur_y_tr2[i])]=1
    endif
    
    maskarray=maskarray_tmp
    for i=0,fix(width) do maskarray=maskarray+shift(maskarray_tmp,0,-i)+shift(maskarray_tmp,0,+i)

    ;;; get the trace from get_slit_regions and shift it up and down
endelse

return,where(maskarray gt 0,nmasked) ;;; subscripts of pixels to mask

end
