function remove_arc_lines,lines_arc,skip_lines
   w=0.1 ;;; 0.1A
   lines_wl=transpose(lines_arc[0,*])
   flag=byte(lines_wl*0)
   if(n_params() eq 1) then skip_lines=[-1.0]
   for i=0,n_elements(skip_lines)-1 do begin 
       b_l=where(abs(lines_wl-skip_lines[i]) lt w, cb_l)
       if(cb_l gt 0) then flag[b_l]=1
   endfor
   return,where(flag eq 0)
end

function read_arc_list,linetab_list,weight_tab=weight_tab,el_lab_tab=el_lab_tab,$
        labellist=labellist
    n_tab=n_elements(linetab_list)
    if(n_elements(weight_tab) ne n_tab) then weight_tab=dblarr(n_tab)+1d
    if(n_elements(el_lab_tab) ne n_tab) then el_lab_tab=strarr(n_tab)

    labellist=[' ']
    for i=0,n_tab-1 do begin
        linelist_cur=read_asc(linetab_list[i])
        linelist_cur[2,*]*=weight_tab[i]
        if(i eq 0) then begin
            linelist=linelist_cur
        endif else begin
            linelist=transpose([transpose(linelist),transpose(linelist_cur)])
        endelse
        labellist=[labellist,replicate(el_lab_tab[i],n_elements(linelist_cur[0,*]))]
    endfor
    labellist=labellist[1:*]
    return,linelist
end

function ident_arc,arc_image,lambda0,linetab_list,pos_lines,lines,fwhm,titl,$
    wdir=wdir,weight_tab=weight_tab,el_lab_tab=el_lab_tab,head_image=head_image,skip_lines=skip_lines,wlmin=wlmin,wlmax=wlmax,wide_search=wide_search,$
    plot=plot,intens=intens,lines_arc=lines_arc,crossfit=crossfit,smy=smy,max_n_line=max_n_line,quadpoly=quadpoly,$
    curve_n_seg=curve_n_seg,mean_curve=mean_curve,pssuff=pssuff,obsflux_lines=obsflux_lines,oh=oh,tilted_slit=tilted_slit,high_curvature=high_curvature,debug=debug

;arc line spectrum identification
;arc_image		arc line image
;lambda0		initial guess for the wavelength scale
;linetab_list		arc line list filename
;pos			2D-array: position of lines
;lines			1D-array: wavelengths of lines
;titl			title of output plot

;;lambda=lambda0

if(n_elements(wdir) ne 1) then wdir='./'
n_2d= (keyword_set(wide_search))? 10 : 3
d_2d= (keyword_set(wide_search))? 0.2 : 0.1
lambda_2d=rebin(lambda0,n_elements(lambda0),n_2d*2+1)
for i=-n_2d,n_2d do lambda_2d[*,n_2d+i]=lambda0[0]+(1.0+d_2d*i/100.0)*(lambda0-lambda0[0])
status=0
if(n_elements(pssuff) ne 1) then pssuff=''
if(n_elements(curve_n_seg) ne 1) then curve_n_seg=4 ;;5

if(n_elements(smy) ne 1) then smy=0 ;;; smooth along Y direction, used only if /crossfit is set
arc_image_cur=arc_image

if(keyword_set(debug)) then begin
    !P.background=2^24-1 
    !P.color=0
endif
sw_g=0
if keyword_set(plot) and plot eq 'plot' then sw_g=1
a=size(arc_image_cur)
if(a[0] ne 2 or (a[0] eq 2 and a[2] eq 1)) then begin
    message,/inf,'Slit image is invalid -- possibly fell off the frame'
    return,-10
endif
Nx=a[1]
Ny=a[2]

if(~keyword_set(crossfit)) then arc_image_cur=median(arc_image,dim=2) # (dblarr(Ny) +1d) ; collapse along the slit for very short slits

curve_ndeg_y=(Ny lt 50)? 1 : ((Ny lt 200)? 2 : 3)
if(smy gt 0) then begin
;    for i=0,Nx-1 do arc_image_cur[i,*]=transpose(median(transpose(arc_image_cur[i,*]),smy))
    for i=0,Nx-1 do arc_image_cur[i,*]=transpose(smooth(transpose(arc_image_cur[i,*]),smy,/nan))
endif

;w=1

d_lambda_2d=transpose((lambda_2d[n_elements(lambda0)-1,*]-lambda_2d[0,*])/(n_elements(lambda0)-1))
x=findgen(Nx)
xflag=bytarr(Nx)

;background subtraction
Ndeg=5
bad_arc=where(finite(arc_image_cur) eq 0 or (arc_image_cur gt 100000.),cbad_arc)  ;; 50000.
if (cbad_arc gt 0) then arc_image_cur[bad_arc]=0
for y=0,Ny-1 do begin
    f=goodpoly(double(x),double(arc_image_cur[*,y]),Ndeg,1,bgr)
    arc_image_cur[*,y]=arc_image_cur[*,y]-bgr
endfor

robomean,arc_image_cur,3,0.5,img_level,img_rms
neg_img=where(arc_image_cur lt -5*img_rms,cneg_img)
if(cneg_img gt 0) then arc_image_cur[neg_img]=0.0

;; creating a reference spectrum
arc_obs=median(arc_image_cur[*,((Ny/2-2)>0):((Ny/2+2)<(Ny-1))],dim=2)
arc_neg_idx=where(arc_obs lt 0, carc_neg_idx)
if(carc_neg_idx gt 0) then arc_obs[arc_neg_idx]=0

;; determining the curvature of arc lines as a function of wavelength by making
;; a piece-wise cross-correlation with the reference spectrum

M = 100
x_cross=findgen(2*M+1)-M
mean_curve=fltarr(Ny)
n_seg=curve_n_seg
overlap=0.3
skippix=0.0
curve_arr=fltarr(n_seg,Ny)
curve_img=fltarr(Nx,Ny)

for L=0,Ny-1 do begin
        x_curve=fltarr(n_seg)
        for i=0,n_seg-1 do begin
            nmin=(i eq 0)? skippix : (double(i)-overlap)*Nx/n_seg
            nmax=(i eq n_seg-1)? Nx-skippix-1 : (double(i)+1.0+overlap)*Nx/n_seg
            if(nmin lt 0) then nmin=0
            if(nmax ge Nx) then nmax=Nx-1L
            x_curve[i]=(nmax+nmin)/2.0
            cross_cur=reverse(c_correlate($
                           abs(arc_image_cur[nmin:nmax,L])*cosin_apod((nmax-nmin+1),10),$
                           abs(arc_obs[nmin:nmax])*cosin_apod((nmax-nmin+1),10),x_cross))
            if(max(cross_cur,/nan) ge 0.3) then begin
                gau=mpfitpeak(x_cross,cross_cur,G,nterms=3,/gauss)
                curve_arr[i,L]=(abs(G[1]) lt max(x_cross)*0.8)? G[1] : !values.f_nan
            endif else curve_arr[i,L]=!values.f_nan
        endfor
endfor

if(~keyword_set(crossfit)) then curve_arr*=0.0

curve_ndeg_x=(0>(n_seg-2)<3)
ccs=sfit_2deg(curve_arr,curve_ndeg_x,curve_ndeg_y,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)
good_pnts=where(finite(curve_arr) eq 1,ngood_pnts)
if(ngood_pnts lt 5) then begin
    message,/inf,'Warning: wavelength solution failed, too few good pixels along the slit'
    if (sw_g eq 0 and keyword_set(debug)) then begin
        device,/close
    endif

    return,4
endif
dcurve=robust_sigma((curve_arr-ccs)[good_pnts])
bcurve=where(abs(curve_arr-ccs) gt 3*dcurve and (abs(curve_arr-ccs) gt 1.0),cbcurve) ;;; >1pix
if(cbcurve gt 0) then begin
    curve_arr_tmp=curve_arr
    curve_arr_tmp[bcurve]=!values.f_nan
    ccs=sfit_2deg(curve_arr_tmp,curve_ndeg_x,curve_ndeg_y,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)

    dcurve=(robust_sigma((curve_arr-ccs)[good_pnts]))>0.3d
    bcurve=where(abs(curve_arr-ccs) gt 3*dcurve,cbcurve)
    if(cbcurve gt 0) then begin
        curve_arr_tmp=curve_arr
        curve_arr_tmp[bcurve]=!values.f_nan
        ccs=sfit_2deg(curve_arr_tmp,curve_ndeg_x,curve_ndeg_y,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)
    endif
endif
curve_img=poly2d(findgen(Nx),findgen(Ny),kx,deg1=curve_ndeg_x,deg2=curve_ndeg_y)
mean_curve=curve_img[Nx/2,*]

;endif
y=findgen(Ny)
max_curve=abs(min(mean_curve,/nan)-max(mean_curve,/nan))
if(max_curve gt 25 and ~keyword_set(tilted_slit) and ~keyword_set(high_curvature)) then begin
    max_curve=0.0
    mean_curve=mean_curve*0.0
    curve_img=curve_img*0.0
endif

if(keyword_set(debug)) then begin
    if sw_g eq 1 then begin
        set_plot,disp_family()
        window,2,xsize=1000,ysize=550
        !P.background=2^24-1
        !P.color=0
    endif
    if (sw_g eq 0) then begin
        set_plot,'ps'
        device,file=wdir+'arc'+pssuff+'.ps',/landscape
    endif

    loadct,0 ;;; greyscale colour table
    plot,[0,Nx-1],[0,Ny-1],/nodata,xst=1,yst=1,position=[0,0.8,1,1],xcharsize=1E-10,/norm
    Npix_Y=(sw_g eq 1)? 220 : Ny
    tv,255-bytscl(congrid(arc_image_cur,1000,Npix_Y),img_level-img_rms,img_level+20*img_rms),0,0.8,xsize=1,ysize=0.2,/norm
endif

;;; finding emission lines, starting from the slit centre, going up then down the slit
pos_lines_tmp=dblarr(Ny,5000)
pos_lines_all=dblarr(Ny,5000)
obsflux_lines_all=dblarr(Ny,5000)
pos_number=intarr(Ny)
kzz=1.0 ;;; 1.0
oversample=5.0 ;;; 1.0
kzz=kzz*oversample
for k=Ny/2,Ny-1 do begin
    vector=(arc_image_cur[*,k] > 0)
    find_peak,x,vector,0,ipix,xpk,ypk,bkpk,ipk 
    s_int=reverse(sort(ypk))
    ;; centroid computation
    cntrd_1d_gauss,congrid(vector,Nx*oversample,cub=-0.5),xpk*oversample,xpk_cnt,0.8*fwhm*kzz,/silent,quadpoly=quadpoly
    ypk=ypk[s_int]
    xpk=xpk[s_int]
    if(n_elements(s_int) eq 1 and xpk_cnt[s_int[0]] lt 0) then continue
    xpk_cnt=xpk_cnt[s_int]/oversample
    pos_lines_tmp[k,0:ipk-1]=xpk_cnt-curve_img[(0 > xpk_cnt < (Nx-1)),k]
    pos_lines_all[k,0:ipk-1]=xpk_cnt
    obsflux_lines_all[k,0:ipk-1]=ypk
    if(k eq Ny/2) then intens=ypk
    pos_number[k]=ipk
endfor
for k=0,Ny/2-1 do begin
    vector=(arc_image_cur[*,k] > 0)
    find_peak,x,vector,0,ipix,xpk,ypk,bkpk,ipk
    s_int=reverse(sort(ypk))
    cntrd_1d_gauss,congrid(vector,Nx*oversample,cub=-0.5),xpk*oversample,xpk_cnt,0.8*fwhm*kzz,/silent,quadpoly=quadpoly
    ypk=ypk[s_int]
    xpk=xpk[s_int]
    if(n_elements(s_int) eq 1 and xpk_cnt[s_int[0]] lt 0) then continue
    xpk_cnt=xpk_cnt[s_int]/oversample
    pos_lines_tmp[k,0:ipk-1]=xpk_cnt-curve_img[(0 > xpk_cnt < (Nx-1)),k]
    pos_lines_all[k,0:ipk-1]=xpk_cnt
    obsflux_lines_all[k,0:ipk-1]=ypk
    pos_number[k]=ipk
endfor

;; sorting line positions
print,'Max(pos_number)=',max(pos_number)

good_pos=fltarr(400)
good_pos_all=fltarr(Ny,400)
good_obsflux_all=fltarr(Ny,400)
j=0
d_Ny=((0.33*Ny)>6) ;;; maximal number of missing pixels along the slit (33%)
;;d_Ny=0.25*Ny ;;; max.num of missing pixels along the slit

;; finding the nearest peak to a position of every emission line in the reference
;; spectrum taking into account the slit curvature
for k=0,max(pos_number) do begin
    pos_line_cur=pos_lines_tmp[Ny/2,k]
    r=where(abs(pos_lines_tmp-pos_line_cur) lt fwhm*2.0, index)
    if ((index gt Ny-d_Ny) and (pos_line_cur gt 0) and (pos_line_cur lt Nx-1-fwhm*1.0)) then begin
        good_pos[j]=pos_line_cur
        good_pos_all[*,j]=pos_lines_all[*,k]
        good_obsflux_all[*,j]=obsflux_lines_all[*,k]
        ;print,'adding k,n_r',k,index,pos_line_cur
        j=j+1
    endif
endfor
r=where(good_pos gt 0,N_line)
good_pos=good_pos[r]
good_pos_all=good_pos_all[*,r]
good_obsflux_all=good_obsflux_all[*,r]
print,'Number of accepted lines: ',N_line

if(n_elements(max_n_line) ne 1) then max_n_line=100

if(N_line gt max_N_line) then begin
    N_line=max_N_line
    good_pos=good_pos[0:max_N_line-1]
    good_pos_all=good_pos_all[*,0:max_N_line-1]
    good_obsflux_all=good_obsflux_all[*,0:max_N_line-1]
endif

if(keyword_set(debug)) then begin
    ;; plotting detected lines
    for k=0,N_line-1 do begin
        oplot,curve_img[good_pos[k],*]+good_pos[k]-fwhm,findgen(Ny) ,linestyle=1
        oplot,curve_img[good_pos[k],*]+good_pos[k]+fwhm,findgen(Ny) ,linestyle=1
    endfor
endif

m_corr_2d=dblarr(n_2d*2+1)
for i=-n_2d,n_2d do begin
    ;; creating the arc model from the initial approximation of the wavelength solution
    lambda=lambda_2d[*,i+n_2d]
    d_lambda=d_lambda_2d[i+n_2d]
    arc_mod_nn=arc_model(lambda,fwhm,linetab_list=linetab_list,weight_tab=weight_tab)
    arc_mod=arc_mod_nn/max(arc_mod_nn)
    if(n_elements(wlmin) eq 1) then begin
        blue_lambda=where(lambda lt wlmin, cbl)
        if(cbl gt 0) then arc_mod[blue_lambda]=0.0
    endif
    if(n_elements(wlmax) eq 1) then begin
        red_lambda=where(lambda gt wlmax, crl)
        if(crl gt 0) then arc_mod[red_lambda]=0.0
    endif
    
    ;; offset determination between arc_obs & arc_mod using cross-correlation
    C=1
    M=100.0
    x_cross=findgen(2*M*C+1)/C-M
    n_nm=n_elements(arc_mod)
    n_ns=n_elements(arc_obs)
    cross=reverse(c_correlate(congrid(smooth(arc_mod,0,/nan),n_nm*C,cubic=-0.5)*cosin_apod(Nx*C,10),$
                              congrid(smooth(arc_obs,0,/nan),n_ns*C,cubic=-0.5)*cosin_apod(Nx*C,10),x_cross))
    m_corr_2d[i+n_2d]=max(cross,m_idx) *(1d -i^2/(n_2d^2*3d))
    m_corr=m_corr_2d[i+n_2d]
    print,'dlam=',d_lambda*x_cross[m_idx]/C,'A',' maxcorr=',m_corr,' stretch=',d_2d*i,'%',format='(a,f6.1,a,a,f5.2,a,f5.2,a)'
    if(abs(x_cross[m_idx]) lt 45 and m_corr gt 0.35) then lambda_2d[*,i+n_2d]=lambda+x_cross[m_idx]*d_lambda/C
endfor

m_corr=max(m_corr_2d,idxmax,/nan)
lambda=lambda_2d[*,idxmax]
d_lambda=d_lambda_2d[idxmax]
arc_mod_nn=arc_model(lambda,fwhm,linetab_list=linetab_list,weight_tab=weight_tab)
arc_mod=arc_mod_nn/max(arc_mod_nn)
print,'accepting maxcorr=',m_corr,' stretch=',d_2d*(idxmax-n_2d),'%',format='(a,f5.2,a,f5.2,a)'

;aaa='' & if(idxmax gt n_2d+3) then read,aaa

;; reading the line list and removing those in the "SKIPLINE" keyword
lines_arc=read_arc_list(linetab_list,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist)
if(keyword_set(oh)) then lines_arc=filter_linelist(lines_arc,fwhm*d_lambda) ;;;; compute effective wavelength for OH lines
if(n_elements(wlmin) eq 1) then begin
    bluelinecut=where(lines_arc[0,*] ge wlmin)
    lines_arc=lines_arc[*,bluelinecut]
    labellist=labellist[bluelinecut]
endif
if(n_elements(wlmax) eq 1) then begin
    redlinecut=where(lines_arc[0,*] le wlmax)
    lines_arc=lines_arc[*,redlinecut]
    labellist=labellist[redlinecut]
endif

if(n_elements(skip_lines) eq 0) then begin
    skip_lines=[-1.0]
endif
lines_idx=remove_arc_lines(lines_arc,skip_lines)

lines=float(transpose(lines_arc[0,lines_idx]))
index_line=transpose(lines_arc[1,lines_idx])
flux_line=transpose(lines_arc[2,lines_idx])
lab_line=labellist[lines_idx]
lines_arc=lines_arc[*,lines_idx]
s_flux=reverse(sort(flux_line))
lines=lines[s_flux]
index_line=index_line[s_flux]
flux_line=flux_line[s_flux]
lab_line=lab_line[s_flux]

print,' max_curve',max_curve

;; extracting arc lines from the line list in the spectral range
left=lambda[0]-4*fwhm*d_lambda
right=lambda[Nx-1]-max_curve+(4*fwhm)*d_lambda
wlsel=where(lines gt left and lines lt right and flux_line gt 0)
lines_all=lines[wlsel]
index_line_all=index_line[wlsel]
flux_line_all=flux_line[wlsel]
lab_line_all=lab_line[wlsel]
N_line=N_elements(lines_all)

good_pos_orig=good_pos

;; performing three iterations to identify arc lines

for iter=0,2 do begin
    index=3-iter

    indsel=where(index_line_all ge index)
    lines=lines_all[indsel]
    index_line=index_line_all[indsel]
    flux_line=flux_line_all[indsel]
    lab_line=lab_line_all[indsel]

    used_lines=lines*0+1
    good_line=fltarr(N_elements(good_pos_orig))
    lab_good_line=strarr(N_elements(good_pos_orig))

    for k=0,N_elements(good_pos_orig)-1 do begin
        n_fwhm=2.0 ;;; was 4.0
        if(iter gt 0) then n_fwhm=1.0/iter
        r=where((abs(lines-lambda[good_pos_orig[k]]) lt n_fwhm*fwhm*d_lambda) and $
                 (used_lines eq 1),ind)
        if (ind eq 1) then begin
            good_line[k]=lines[r]
            lab_good_line[k]=lab_line[r]
            used_lines[r]=0
        endif
        if (ind gt 1) then begin ;; more than one line found around the position
            print,'>1 lines: lam,linepos=',lambda[good_pos_orig[k]],lines[r],lab_line[r]
            if(iter gt 0) then begin
                mm=min(abs((lines-lambda[good_pos_orig[k]])[r]),midx)
                good_line[k]=lines[r[midx]]
                lab_good_line[k]=lab_line[r[midx]]
                used_lines[r[midx]]=0
            endif else begin
                r_add=where(flux_line[r]/flux_line[r[0]] gt 0.75, cra)
                if(cra eq 1) then begin
                    good_line[k]=lines[r[r_add]]
                    lab_good_line[k]=lab_line[r[r_add]]
                    used_lines[r[r_add]]=0
                    print,'>1 lines: chosenA: ',lines[r[r_add]]
                endif else begin 
                    mm=min(abs((lines-lambda[good_pos_orig[k]])[r[r_add]]),midx)
                    good_line[k]=lines[r[r_add[midx]]]
                    lab_good_line[k]=lab_line[r[r_add[midx]]]
                    used_lines[r[r_add[midx]]]=0
                    print,'>1 lines: chosenB: ',lines[r[r_add[midx]]]
                endelse
            endelse
        endif
    endfor
    r=where(good_line gt 0,N_line)
    good_line=good_line[r]
    lab_good_line=lab_good_line[r]
    good_pos=good_pos_orig[r]
    print,N_elements(good_line),' lines identified after iteration ',iter
    if(n_elements(good_line) lt 5) then begin
        message,/inf,'Warning: wavelength solution failed, too few lines could be identified'
        if (sw_g eq 0 and keyword_set(debug)) then begin
            device,/close
        endif

        return,2
    endif

    ck = robust_poly_fit(good_pos,good_line,3)
    lambda=poly(findgen(Nx),ck)
endfor

if(keyword_set(debug)) then begin
;; plotting
    out=alog(arc_obs/max(arc_obs,/nan)+0.01)
    out=out-min(out(Nx/2-Nx/4:Nx/2+Nx/4))
    out=out/max(out(Nx/2-Nx/4:Nx/2+Nx/4))
    plot,out,xst=1,position=[0,0,1,0.75],/norm,/noerase,yst=1,yrange=[0,1.24],$
    	title=titl
    for k=0,N_line-1 do begin
        oplot, [1,1]*good_pos[k],[0,1],linestyle=1
        xyouts,good_pos[k],1,lab_good_line[k]+' '+string(good_line[k],format='(F8.2)'),/data,charsize=0.75,orientation=90
    endfor
    if (sw_g eq 0) then begin
        device,/close
        ;set_plot,disp_family()
    endif
    ;; done with the plot
endif

pos_lines=fltarr(Ny,N_line)
obsflux_lines=fltarr(Ny,N_line)
for y=0,Ny-1 do $
    pos_lines[y,*]=good_pos[*]+curve_img[(0 > good_pos < (Nx-1)),y]

lines=good_line
lab_line=lab_good_line
l_sort=sort(lines)
lines=lines[l_sort]
lab_line=lab_line[l_sort]
pos_lines=pos_lines[*,l_sort]
obsflux_lines=obsflux_lines[*,l_sort]

print,'Filling pos_lines...',format='(a,$)'
for y=0,Ny-1 do begin
    for j=0,N_line-1 do begin
        m=min(abs(pos_lines_all[y,*]-pos_lines[y,j]),cm)
        pos_lines[y,j]=(m lt 30)? pos_lines_all[y,cm] : !values.f_nan
        obsflux_lines[y,j]=(m lt 30)? obsflux_lines_all[y,cm] : !values.f_nan
    endfor
endfor
print,'done'

return, status
end
