function soar_extract_cluster,obj_abs,err_obj_abs,ymin=ymin,ymax=ymax,fluxerr=fluxerr,m_win=m_win

    if(n_elements(ymin) ne 1) then ymin=200
    if(n_elements(ymax) ne 1) then ymax=600

    if(n_elements(m_win) ne 1) then m_win=11

    s_im=size(obj_abs)
    flux=fltarr(s_im[1])
    fluxerr=fltarr(s_im[1])

    obj_med=obj_abs
    for i=0,s_im[2]-1 do obj_med[*,i]=median(obj_abs[*,i],m_win)

    ew_all=ymin+lindgen(ymax-ymin+1)
    for x=0,s_im[1]-1 do begin
        b_fl=where(finite(obj_abs[x,ew_all]) ne 1, cb_fl, compl=g_fl, ncompl=cg_fl)
        if(cb_fl gt 0 and cg_fl gt 0) then begin
            frac=total(obj_med[x,ew_all])/total(obj_med[x,ew_all[g_fl]])
            flux[x]=total(obj_abs[x,ew_all[g_fl]])*frac
            fluxerr[x]=sqrt(total(err_obj_abs[x,ew_all[g_fl]]^2))*frac
        endif else begin
            flux[x]=total(obj_abs[x,ew_all])
            fluxerr[x]=sqrt(total(err_obj_abs[x,ew_all]^2))
        endelse
    endfor

    return, flux
end
