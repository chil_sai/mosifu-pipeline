function crea_disper_generic_slit,filename_arc,slit,dis_coeff,fwhm,N_deg,$
    dis_coeff_blue=dis_coeff_blue,dis_coeff_red=dis_coeff_red,$
    wdir=wdir,xpos=xpos,ypos=ypos,filename_realarc=filename_realarc,use_oh=use_oh,$
    arc_list_config=arc_list_config,$
    oh_list_config=oh_list_config,$
    fast_cntrd=fast_cntrd,skip_lines=skip_lines,$
    dist_map=dist_map,lin_params=l_par,fullslit=fullslit,$
    PLOT=plot,NdegY=NdegY_inp,smy=smy,pssuff=pssuff,$
    ymin_out=ymin_out,ymax_out=ymax_out,quick=quick,$
    disper_table=disper_table,cd2d=kx_f,debug=debug,pady=pady,$
    slit_geom=slit_geom,high_curvature=high_curvature,$
    pos_lines_str=pos_lines_str
;+
; NAME:
;	CREA_DISPER
; PURPOSE:
;      Identification of arc/sky lines and wavelength solution computation for one slit
; DESCRIPTION:
;	TBW
;
; CALLING SEQUENCE:
;	status=CREA_DISPER_GENERIC_SLIT(logfile,slit,fwhm,N_deg,PLOT=plot,$
;                                       yfit=yfit,NdegY=NdegY)
;
; CATEGORY:
;	CfA MOS/IFU pipeline toolkit
;
; INPUTS:
;	LOG = String scalar of file name  FITS-header from the LOG observation
;       SLIT = number of the slit to work on
;	FWHM = spectral line expected fwhm in pixels
;	N_deg = degree of the polynomial along dispersion
;
; OUTPUTS:
;	 2-dimensional ( N_deg+1) x N_spectra float array coefficient dispersion curve,
;        saved to the disk in  working directory with  standard name 'disper.fits' and
;	 print in file 'disper.txt'.
;	 Last value in every string is rms approximation i px
;
; OPTIONAL OUTPUT:
;	no
;
; OPTIONAL INPUT KEYWORDS:
;	PLOT - if present 2-d errors of approximation plotted to display,
;	else save plot to POSTSCRIPT file in working directory with name 'err_line.ps'
;
; RESTRICTIONS:
;	no
; NOTES:
;	no
;
; PROCEDURES USED:
;	Functions :  TBW
;	Procedures:  TBW
;
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, CfA, May 2017
;-
;

status=0

if n_params() eq 4 then N_deg=3
if(n_elements(NdegY_inp) ne 1) then NdegY_inp=3
NdegY=NdegY_inp
if(n_elements(pady) ne 1) then pady=2
message,'Creating wavelength solution...',/cont


sw_g=keyword_set(plot)? 1 : 0

if(n_elements(pssuff) ne 1) then pssuff=''

arc_image_name=(n_elements(wdir) ne 1)? filename_arc : wdir+filename_arc
arc_image=mrdfits(arc_image_name,slit+1,h,/silent)

if(keyword_set(oh)) then begin
    realarc_image_name=(n_elements(wdir) ne 1)? filename_realarc : wdir+filename_realarc
    realarc_image=mrdfits(realarc_image_name,slit+1,h,/silent)
endif

if(n_elements(wdir) ne 1) then wdir=strmid(filename_arc,0,strpos(filename_arc,'/',/reverse_search)+1)

Nx=sxpar(h,'NAXIS1')

h0=headfits(arc_image_name)

slitnum_off=0

y_off=sxpar(h,'YOFFSET')
ys_cur=sxpar(h,'NAXIS2')
print,'SIZE(arc_image)',size(arc_image)

;;;; cosmic ray and hot pixel cleaning -- absolutely required when using night sky to compute the wavelength solution
;gain=1.0
;rdnoise=5.0
;exptime=1.0 ;;; sxpar(h,'EXPTIME')
;noise_arc = sqrt(djs_median(arc_image,width=3,boundary='reflect')*exptime*gain+2*rdnoise^2)/exptime/gain
;la_cosmic_array,arc_image,noise=noise_arc,gain=gain,readn=rdnoise,maskarr=arc_mask,sigclip=18
;bpix_arc=where(arc_mask ne 0,cbpix_arc)
;if(cbpix_arc gt 0) then begin
;    arc_image_med=arc_image*0.0
;    arc_med_prof=median(arc_image,dim=2)
;    for i=0,n_elements(arc_image[0,*])-1 do arc_image_med[*,i]=arc_med_prof
;    arc_image[bpix_arc]=arc_image_med[bpix_arc]
;endif
;;;; end of cosmic ray cleaning

if(NdegY eq -1) then begin
    if(ys_cur le 9) then NdegY=0 else $
    if(ys_cur gt 9 and ys_cur le 35) then NdegY=1 else $
    if(ys_cur gt 35 and ys_cur le 250) then NdegY=2 else $
    if(ys_cur gt 250 and ys_cur le 1000) then NdegY=3 else $
    if(ys_cur gt 1000 and ys_cur le 2000) then NdegY=4 else NdegY=5
endif

;;; wavelength limits in A
wllimblue = 3000.0
wllimred = 25000.0

if(n_elements(xpos) ne 1) then xpos=sxpar(h,'SLITX')
if(n_elements(ypos) ne 1) then ypos=sxpar(h,'SLITY')
tilted_slit=sxpar(h,'TILTEDSL')

s_dm=size(dist_map)
if(s_dm[0] eq 1 and s_dm[2] eq 8) then begin
    arc_image=mos_rectify_slit(arc_image,dist_map,y_off,edge=~tilted_slit)
    if(keyword_set(oh)) then realarc_image=mos_rectify_slit(realarc_image,dist_map,y_off,edge=~tilted_slit)
endif
slit_im_prof=total(arc_image[Nx*0.4:Nx*0.6,*],1,/nan)

;slit_id = def_slit(logfile)
;if(slit_id ne 'mos') then slit_im_prof=median(slit_im_prof,25)

med_slit_im_prof=median(slit_im_prof > 0)
if(med_slit_im_prof eq 0.0 and keyword_set(oh)) then begin
    slit_im_prof=total(realarc_image[Nx*0.4:Nx*0.6,*],1,/nan)
;    if(slit_id ne 'mos') then slit_im_prof=median(slit_im_prof,25)
    med_slit_im_prof=median(slit_im_prof > 0)
endif

if(s_dm[0] eq 1 and s_dm[2] eq 8) then begin
    y_pix_cur=ys_cur-1-(ypos*dist_map.mask_y_scl)-dist_map.mask_dy0
    y_trace=poly2d(Nx-1d -dindgen(Nx),dindgen(Nx)*0+y_pix_cur,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_pix_cur
endif else $
    y_trace=dblarr(Nx)*0+y_off+ys_cur/2

good_slit=where(slit_im_prof gt 0.3*med_slit_im_prof,cgood_slit)
if(pady gt 0) then begin
    if(good_slit[0] gt 0) then begin
        low_pad_y=good_slit[0]-1-reverse(lindgen(pady < good_slit[0]))
        cgood_slit+=n_elements(low_pad_y)
        good_slit=[low_pad_y,good_slit]
    endif
    if(good_slit[cgood_slit-1] lt n_elements(slit_im_prof)-1) then begin
        high_pad_y=good_slit[cgood_slit-1]+1+$
                       lindgen(pady < (n_elements(slit_im_prof)-good_slit[cgood_slit-1]-1))
        cgood_slit+=n_elements(high_pad_y)
        good_slit=[good_slit,high_pad_y]
    endif
endif

if(keyword_set(fullslit)) then begin
    ymin=0l
    ymax=n_elements(arc_image[0,*])-1l
endif else begin
    ymin=good_slit[0]
    ymax=good_slit[cgood_slit-1]
endelse

ymin_out=ymin+y_off
ymax_out=ymax+y_off

arc_image=arc_image[*,ymin:ymax]
sxaddpar,h,'NAXIS2',n_elements(arc_image[0,*])

Ny=sxpar(h,'NAXIS2')
Nyorig=Ny

writesuff=''

barc=where(finite(arc_image) ne 1, bcnt)
if(bcnt gt 0) then arc_image[barc]=0

if(keyword_set(oh)) then begin
    realarc_image=realarc_image[*,ymin:ymax]
    brealarc=where(finite(realarc_image) ne 1, brealcnt)
    if(brealcnt gt 0) then realarc_image[brealarc]=0
endif

slitthr=0
bad_slitthr=bytarr(Ny)
good_slitthr_idx=findgen(Ny)

mmpix=1d
if(s_dm[0] eq 1 and s_dm[2] eq 8) then $
    if(tag_exist(dist_map,'mask_y_scl')) then mmpix=1d/dist_map.mask_y_scl

lambda=poly(dindgen(Nx),dis_coeff)
if(n_elements(l_par) ne 1) then l_par={grism:'', filter:'', wl0:lambda[0], dwl:abs(lambda[Nx-1]-lambda[0])/(Nx-1d), npix:Nx, wl_min:lambda[0], wl_max:lambda[Nx-1]}
wlmin=l_par.wl_min
wlmax=l_par.wl_max

if(n_elements(dis_coeff_blue) eq n_elements(dis_coeff) and n_elements(dis_coeff_red) eq n_elements(dis_coeff)) then begin
    lambda_blue=poly(dindgen(Nx),dis_coeff_blue)
    lambda_red=poly(dindgen(Nx),dis_coeff_red)
endif else begin
    lambda_blue=lambda
    lambda_red=lambda
endelse
lambda_cnt=[min(lambda_blue) > min(lambda_red), max(lambda_blue) < max(lambda_red)]

red_end=where(lambda gt wlmax,crwl)
if(crwl gt 0) then begin ;;;; cutting red end
    Nx=red_end[0]
    arc_image=arc_image[0:Nx-1,*]
    lambda=lambda[0:Nx-1]
    if(keyword_set(oh)) then realarc_image=realarc_image[0:Nx-1,*]
endif

lambda_c=lambda[Nx/2]
d_lambda=lambda_c-lambda[Nx/2-1]

date=strcompress(sxpar(h,'date-obs'))
A=sxpar(h,'HISTORY')
A = (n_elements(A) ge 3)? A[2] : ' '

sptype=(keyword_set(oh))? 'OH' : 'arc'

titl='Spectrum '+sptype+' Slit#'+string(slit+1,format='(i4.4)')

plot_flag = (sw_g eq 1)? 'plot' : 'no'
if(n_elements(skip_lines) lt 2) then skip_lines=[-1.0]

if(n_elements(arc_list_config) eq 0) then $
    arc_list_config={$
        linetab_list:[getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/linesCuAr.tab'],$
        weight:[1.0],$
        label:['Cu']}

if(n_elements(oh_list_config) eq 0) then $
    oh_list_config={$
        linetab_list:[getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/linesOH_R2k_1050nm_corr.tab'],$
        weight:[1.0],$
        label:['OH']}

cur_list_config=(keyword_set(oh))? oh_list_config : arc_list_config

status_id=ident_arc(arc_image,lambda,cur_list_config.linetab_list,pos_lines,lines,fwhm,titl,wdir=wdir,$
    PLOT=plot_flag,intens=intens,wlmin=wlmin,wlmax=wlmax,/wide_search,high_curvature=high_curvature,$
    lines_arc=lines_arc,crossfit=(NdegY ne 0),smy=smy,skip_lines=skip_lines,quadpoly=fast_cntrd,$
    head_image=h,curve_n_seg=1+(0 > (NdegY-1) < 3),mean_curve=mean_curve,pssuff=pssuff,$
    obsflux_lines=obsflux_lines,oh=oh,weight_tab=cur_list_config.weight,el_lab_tab=cur_list_config.label,tilted_slit=tilted_slit,debug=debug)
if(status_id ne 0 and keyword_set(oh)) then begin
    message,/inf,'Wavelength solution failed in Slit #'+string(slit,format='(i4.4)')+' using OH lines. Trying to use arcs'
    status_id=ident_arc(wdir,realarc_image,arc_list_config.linetab_list,pos_lines,lines,fwhm,titl,wdir=wdir,$
        PLOT=plot_flag,intens=intens,wlmin=wlmin,wlmax=wlmax,/wide_search,high_curvature=high_curvature,$
        lines_arc=lines_arc,crossfit=(NdegY ne 0),smy=smy,skip_lines=skip_lines,quadpoly=fast_cntrd,$
        head_image=h,curve_n_seg=1+3*(NdegY gt 1),mean_curve=mean_curve,pssuff=pssuff,$
        obsflux_lines=obsflux_lines,weight_tab=arc_list_config.weight,el_lab_tab=arc_list_config.label,tilted_slit=tilted_slit,debug=debug)
    if(status_id ne 0) then begin
        message,/inf,'Wavelength solution failed in Slit #'+string(slit,format='(i4.4)')+' using arc lines. Returning NaN'
        return,2
    endif else arc_image=realarc_image
endif

N_line=N_elements(lines)

w=4
ycrd=dindgen(Ny)

itcur=0
itmax=3
cbad_pos=1
inpbadflag=intarr(n_line)

    n_sig=3.0
    n_sig_2d=5.0
    if(keyword_set(debug)) then writefits,wdir+'pos_lines_orig'+pssuff+'.fits',pos_lines
    if(keyword_set(debug)) then writefits,wdir+'obsflux_lines_orig'+pssuff+'.fits',obsflux_lines
    usable_lines=where(lines_arc[1,*] gt 0)
    badflag=bytarr(n_elements(lines))
    inpbadflag=badflag

    cbad_pos=1
    itcur=0
    itmax=10
    while(itcur lt itmax and cbad_pos gt 0) do begin
        itcur++
        print,''
        print,'Iteration ',itcur
        gf=where(badflag eq 0, cgf)
        if(cgf eq 0) then message,'Dispersion relation cannot be built, all arc lines are marked bad'

        kcfit=robust_poly_fit(pos_lines[Ny/2,gf],lines[gf],(N_deg < 5), ykcfit)
        dev_arr=lines[gf]-ykcfit
        d_stdev=robust_sigma(dev_arr)
        print,'Stdev(wave.sol.)=',d_stdev
        bad_pos = where(abs(lines[gf]-ykcfit) gt n_sig*d_stdev, cbad_pos)
        cbad_pos0=cbad_pos
        for j=0,cbad_pos0-1 do begin
            min_dlam=min(abs(lines_arc[0,usable_lines]-(lines[bad_pos[j]]+dev_arr[bad_pos[j]])),mdlidx)
            print,'n_line,min_dlam,newlam=',bad_pos[j],min_dlam,lines_arc[0,usable_lines[mdlidx]],(lines[bad_pos[j]]+dev_arr[bad_pos[j]])
            if(min_dlam lt 15.0*2) then begin
                lines[bad_pos[j]]=lines_arc[0,usable_lines[mdlidx]]
                badflag[bad_pos[j]]=0
                cbad_pos=cbad_pos-1
;;;;                gf=[gf,bad_pos[j]]
                print,'Identified as '+string(lines_arc[0,usable_lines[mdlidx]])
            endif else begin
                badflag[bad_pos[j]]=1
                print,'Left unidentified'
            endelse
        endfor
    endwhile
    gf=where(badflag eq 0, cgf)
    lines=lines[gf]
    pos_lines=pos_lines[*,gf]
    obsflux_lines=obsflux_lines[*,gf]
    n_line=n_elements(gf)
    badflag=bytarr(n_line)
    bflag_pos_lines=bytarr(Ny,n_line)

    yvec=findgen(Ny)
    for i=0,n_line-1 do begin
        gpos_cur=where(finite(pos_lines[*,i]) eq 1,cgpos_cur,compl=badlc,ncompl=cbadlc)
        if(cgpos_cur ge 5) then begin
            if(NdegY gt 0) then begin
                kl=robust_poly_fit(yvec[gpos_cur],pos_lines[gpos_cur,i],NdegY)
                fit_pos_line_cur=poly(yvec,kl)
            endif else fit_pos_line_cur=median(pos_lines[gpos_cur,i])
            d_line_cur=pos_lines[*,i]-fit_pos_line_cur
            badlc=where(abs(d_line_cur) gt 1.5,cbadlc) ;;; reject measurements offset by >1.5pix
        endif
        if(cbadlc gt 0) then bflag_pos_lines[badlc,i]=1
    endfor

    itcur=0
    n_pos_lines=n_elements(pos_lines)
    data_arr=dblarr(3,n_pos_lines)
    
    flag_arr=reform(bflag_pos_lines,n_pos_lines)
    bfl=where(flag_arr ne 0, cbfl)
    data_arr[0,*]=reform(pos_lines,1,n_pos_lines)
    data_arr[1,*]=reform(dindgen(Ny) # replicate(1.0,n_line),1,n_pos_lines)
    data_arr[2,*]=reform(replicate(1.0,Ny) # lines,1,n_pos_lines)

    lambda_red=max(lambda_cnt,min=lambda_blue)
    lines_red=where(data_arr[2,*] gt lambda_red and data_arr[2,*] lt wllimred, clines_red)
    lines_blue=where(data_arr[2,*] lt lambda_blue and data_arr[2,*] gt wllimblue, clines_blue)
    lines_cnt=where(data_arr[2,*] ge lambda_blue and data_arr[2,*] le lambda_red and data_arr[2,*] gt wllimblue and data_arr[2,*] lt wllimred, clines_cnt)
    err_data_arr=1.0/((reform(obsflux_lines,n_pos_lines) > 0.01)^0.4) ;; ^0.25
    b_err=where((finite(err_data_arr) ne 1), cb_err)
    if(cb_err gt 0) then err_data_arr[b_err]=max(err_data_arr,/nan)
    wgt_cnt=total(1.0/err_data_arr[lines_cnt]^2)
    if(clines_red gt 0) then begin
        wgt_red=total(1.0/err_data_arr[lines_red]^2)
        err_norm_red=(wgt_red/16.0 lt wgt_cnt)? sqrt(wgt_red/16.0/wgt_cnt) : 1.0
        print,'Red end error renormalization: ',err_norm_red
        err_data_arr[lines_red]*=err_norm_red
    endif
    if(clines_blue gt 0) then begin
        wgt_blue=total(1.0/err_data_arr[lines_blue]^2)
        err_norm_blue=(wgt_blue/16.0 gt wgt_cnt)? sqrt(wgt_blue/16./wgt_cnt) : 1.0
        print,'Blue error renormalization: ',err_norm_blue
        err_data_arr[lines_blue]*=err_norm_blue
    endif

    print,'Rejected ',cbfl,' measurements due to bad positions'
    if(cbfl gt 0) then data_arr[0,bfl]=!values.f_nan
    linesstd=transpose(data_arr[2,*]*0.0)
    data_line_id=reform(replicate(1l,Ny) # lindgen(n_line),n_pos_lines)
    
    gdata=where(finite(total(data_arr,1)) eq 1, cgdata)
    data_arr=data_arr[*,gdata]
    err_data_arr=err_data_arr[gdata]

    data_line_id=data_line_id[gdata]
    linesstd=linesstd[gdata]
    print,'Fitting 2D surface using N points; N=',cgdata
    c_out = 1
    while(itcur lt itmax and c_out gt 0) do begin
        itcur++
        zero_coeff=bytarr(NdegY+1,N_deg+1)
        if(itcur eq 1 and NdegY gt 0) then zero_coeff[1:*,1:*]=1
        bfit=sfit_2deg(data_arr, err=err_data_arr, N_deg, NdegY, zero_coeff=zero_coeff, kx=kx_f, /irreg, la_lsq_method=1)
        resid=data_arr[2,*]-bfit
        for i=0,n_line-1 do begin
            glc=where(data_line_id eq i,cglc)
            if(cglc gt 0) then begin
                linesstd[glc]=robust_sigma(data_arr[2,glc]-bfit[glc])
;                print,'Line, stdev(pix):',lines[i],linesstd[glc[0]]
            endif else badflag[i]=1
        endfor
        medlstd=median(linesstd)
        print,'Median(linesstd)=',medlstd

        res_std=robust_sigma(resid)
        outl=where(((abs(resid) gt n_sig_2d*res_std) or $
                    (linesstd gt medlstd*n_sig_2d*2)) and (data_line_id ne 0), c_out, compl=goodfit)
;;        outl=where((abs(resid) gt n_sig_2d*res_std) and data_line_id ne 0, c_out, compl=goodfit)
        print,'Iteration, res_std, c_out=',itcur,res_std,c_out
        gdata=gdata[goodfit]
        data_arr=data_arr[*,goodfit]
        err_data_arr=err_data_arr[goodfit]
        data_line_id=data_line_id[goodfit]
        linesstd=linesstd[goodfit]
    endwhile
    resid=data_arr[2,*]-bfit[goodfit]

    mask_posline=fltarr(n_elements(pos_lines))+!values.f_nan
    mask_posline[gdata]=0.0
    pos_lines=pos_lines+mask_posline

    mean_lines_orig=poly2d((reform(pos_lines,n_pos_lines)),$
                           (reform(dindgen(Ny)#replicate(1.0,n_line),n_pos_lines)),$
                           kx_f,deg1=N_deg,deg2=NdegY,/irreg)
    mean_lines_orig=reform(mean_lines_orig,Ny,n_pos_lines/Ny)
    mean_lines=mean_lines_orig
    disper_par=dblarr(N_deg+2,Ny)
    yvec=dindgen(Ny)
    for i=0,Ny-1 do begin
        for j=0,N_deg do begin
            disper_par[j,i]=poly(yvec[i],kx_f[*,j])
        endfor
        disper_par[N_deg+1,i]=0.00001

    endfor

    pos_lines_orig=pos_lines
    err_pos_lines=pos_lines*0.0+0.001

mean_lines_orig=pos_lines_orig*0
for i=0,Ny-1 do mean_lines_orig[i,*]=poly(pos_lines_orig[i,*],disper_par[0:n_deg,i])
resistant_mean,disper_par[N_deg+1,*],3,disp_rms

if(keyword_set(debug)) then begin
    ;plot result of approximation
    if sw_g eq 1 then window,3,xsize=600,ysize=850
    if sw_g eq 0 then begin
        set_plot,'ps'
        device,file=wdir+'err_approx'+pssuff+'.ps',xsize=22,ysize=28,xoffset=0,yoffset=1,/portrait
    endif
    !p.background=16777215
    !p.color=0
    dy=4
    ymin_plot=-dy
    ymax_plot=dy*N_line
    plot,[0,Ny-1],[ymin_plot,ymax_plot],xst=1,yst=1,/nodata,$
    	ytitle='Residual deviations, px',$
    	xtitle='Slit position',$
    	position=[0.09,0.05,0.75,0.82],/norm
    print,'Lines identified'
    print,'Lambda','dLam','RMS','pos','e_pos',format='(a9,a7,a5,2a9)'
    openw,u,wdir+'lines_id'+pssuff+'.txt',/get_lun
    printf,u,'Lines identified'
    printf,u,'Lambda','dLam','RMS','pos','e_pos',format='(a9,a7,a5,2a9)'

    for j=0,N_line-1 do begin
        def_full=mean_lines_orig[*,j]-lines[j]
        def=mean_lines[*,j]-lines[j]
    ;;;    aaa='' & read,aaa & if aaa eq 's' then stop
        robomean,def[good_slitthr_idx],3,0.5,mean_def,rms
    ;;;    resistant_mean,def[good_slitthr_idx],3,mean_def,rms
        oplot,10*def/d_lambda+dy*j,psym=6,symsize=0.2,col=128
        oplot,10*def_full/d_lambda+dy*j,psym=-4,symsize=0.2
        oplot,[0,Ny],[1,1]*dy*j,color=150
        correction=mean_def
        if correction lt 0 then S='  -'+string(abs(correction),format='(F4.2)')
        if correction gt 0 then S='  +'+string(abs(correction),format='(F4.2)')
        if correction eq 0 then S='        '
        flagval=(badflag[j] eq 0)?' ':'*'
        xyouts,Ny+5,dy*j,string(lines[j],format='(F8.2)')+$
            S+string(rms,format='(F7.2)'),charsize=0.75,/data
        print,lines[j],correction,rms,pos_lines[Ny/2,j],err_pos_lines[Ny/2,j],flagval,format='(f9.2,f7.2,f5.2,f9.2,f8.3,1x,a2)'
        printf,u,lines[j],correction,rms,pos_lines[Ny/2,j],err_pos_lines[Ny/2,j],flagval,format='(f9.2,f7.2,f5.2,f9.2,f8.3,1x,a2)'
    endfor
    close,u
    free_lun,u
    x_hist=findgen(21)*0.025
    plot,x_hist,histogram(disper_par[N_deg+1,*]/d_lambda,min=0,max=.5,binsize=.025),$
    	position=[0.09,0.86,0.38,0.98],/noerase,/norm,$
    	xst=1,yst=1,psym=10,$
    	xtitle='error, px'
    xyouts,0.45,0.975,'ACCURACY OF THE 2D WAVELENGTH SOLUTION',/norm
    xyouts,0.4,0.95,'date of observation   '+date,/norm
    xyouts,0.4,0.93,'Arc spectrum file:'+ a,/norm
    xyouts,0.4,0.91,'Grating '+strcompress(sxpar(h,'disperse'))+$
    	' TILT='+STRING(SXPAR(H,'TILTPOS'),format='(F6.1)'),/norm
    cw=0
    disp=total(disper_par[1,*])/Ny
    for j=0,N_deg do cw=cw+disper_par[j,Ny/2]*(Nx/2)^J
    xyouts,0.4,0.89,'Central wavelength'+string(cw,format='(F8.2)')+$
    	' A Mean dispersion'+string(disp,format='(F6.2)')+' A/px',/norm
    xyouts,0.4,0.87,'Number of lines'+string(N_line,format='(I3)')+$
    	'   Average error approximation '+string(disp_rms/d_lambda,format='(F4.2)')+' px',/norm
    xyouts,0.78,0.82,'!7k!3(A)    !7Dk!3  rms !7k!3',/norm
    if sw_g eq 0 then begin
        device,/close
        set_plot,disp_family()
    endif
endif

;saving dispersion relation
if(keyword_set(debug)) then writefits,wdir+'disper'+writesuff+'.fits',disper_par
if(keyword_set(debug)) then writefits,wdir+'pos_lines'+writesuff+'.fits',pos_lines
ys_pos=n_elements(pos_lines[*,0])
pos_lines_str=replicate({wl:0d,$
                                xpix:dblarr(ys_pos),$
                                ypix:dblarr(ys_pos),$
                                xmask:dblarr(ys_pos)+!values.d_nan,$
                                ymask:dblarr(ys_pos)+!values.d_nan,$
                                flux:dblarr(ys_pos)},$
                                n_elements(pos_lines[0,*]))
slit_tilt = (sxpar(h,'SLITTHET')-sxpar(h,'MASKPA')) mod 180.0
for i=0,n_elements(lines)-1 do begin
    pos_lines_str[i].wl=lines[i]
    pos_lines_str[i].xpix=(pos_lines[*,i])
    pos_lines_str[i].ypix=dindgen(ys_pos)+sxpar(h,'YOFFSET')
    dy_mm = interpol((y_trace-(ys_cur-1d)/2d)*mmpix,dindgen(sxpar(h,'NAXIS1')),median(pos_lines[*,i]))
    dx_mm = -(0*sxpar(h,'SLITHEIG')/2.0+(dindgen(ys_pos)-(ys_pos-1d)/2d)*mmpix)*tan(slit_tilt/!radeg)
    pos_lines_str[i].xmask=xpos + dblarr(ys_pos) + dx_mm
    pos_lines_str[i].ymask=ypos + (dindgen(ys_pos)-(ys_pos-1d)/2d)*mmpix + dy_mm
    pos_lines_str[i].flux=(obsflux_lines[*,i])
endfor

if(keyword_set(debug)) then begin
    writefits,wdir+'pos_lines'+pssuff+'.fits',pos_lines
    mwrfits,pos_lines_str,wdir+'pos_lines'+pssuff+'.fits'
    a=size(disper_par)
    out_format='(F10.2,F10.5,'+string(a(1)-3,format='(I1)')+'E16.6,F6.2)'
    openw, UNIT,wdir+'disper'+writesuff+'.txt', /GET_LUN
    for k=0,a(2)-1 do begin
        printf, UNIT,disper_par(*,k),format=out_format
    endfor
    close, UNIT
    free_lun,UNIT
endif

message,'dispersion relation mean error ='+string(disp_rms/d_lambda,format='(F5.2)')+' px',/cont

disper_table=replicate({y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
    w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1,$
    wl_sol:dblarr(N_deg+1)+!values.d_nan,wl_s_err:!values.d_nan},Ny)
disper_table[*].wl_sol=disper_par[0:N_deg,*]
disper_table[*].wl_s_err=0.00001
if(n_elements(slit_geom) gt 0) then begin
    disper_table.y_pix=slit_geom.ycrd+ymin
    disper_table.x_mask=slit_geom.x_mask
    disper_table.y_mask=slit_geom.y_mask
    disper_table.w_pix=slit_geom.w_pix
    disper_table.h_pix=slit_geom.h_pix
    disper_table.slit=slit_geom.slit
endif else begin
    disper_table.y_pix=ycrd+ymin
    disper_table.x_mask=median(pos_lines_str.xmask,dim=2)
    disper_table.y_mask=median(pos_lines_str.ymask,dim=2)
    disper_table.w_pix=1.0
    disper_table.h_pix=Ny
    disper_table.slit=slit+1L
endelse

return,status

end
