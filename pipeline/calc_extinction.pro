function calc_extinction,lambda,z,a=a,c=c
    ; extinction calculation
    ; lambda in A; Z in deg

    if(n_elements(a) ne 1) then a=0.008
    if(n_elements(c) ne 1) then c=0.115
    extin=(a*1./((lambda/10000.)^4.)+c)/2.3
    extin=10.^(-extin/cos(z*!DTOR))
    return,extin
end
