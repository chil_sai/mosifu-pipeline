function slit_measure_emline,slit_lin,wl,wl_line,win_line,nterms=nterms,status=status,$
    slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,slit_medw=slit_medw,slit_bspl=slit_bspl,slit_nbkpts=slit_nbkpts
    if(n_elements(nterms) ne 1) then nterms=4
    if(n_elements(slit_nbkpts) ne 1) then slit_nbkpts=20
    if(n_elements(slit_poly) ne 1) then slit_poly=-1
    if(n_elements(slit_medw) ne 1) then slit_medw=1
    status=1
    s_im=size(slit_lin)
    ny=(s_im[0] eq 2)? s_im[2] : 1
    res_arr=dblarr(nterms,ny)+!values.d_nan

    gwl=where(wl ge wl_line-win_line and wl le wl_line+win_line,cgwl)
    if(cgwl lt 10) then begin
        status=2
        return,res_arr
    endif
    gline=0
    for y=0,ny-1 do begin
        gpix=where(finite(slit_lin[gwl,y]) eq 1, cgpix)
        if(cgpix lt 8) then continue
        pfit=mpfitpeak(wl[gwl[gpix]],slit_lin[gwl[gpix],y],a,nterms=nterms)
        res_arr[*,y]=a
        gline++
    endfor
    if(gline gt 0) then status=0
    gline_idx=where(finite(res_arr[0,*]) eq 1, cgline_idx)
    if(keyword_set(auto_slit_poly)) then begin
        if(gline lt 50) then slit_poly=0
        if(gline ge 50 and gline lt 150) then slit_poly=1
        if(gline ge 150 and gline lt 400) then slit_poly=2
        if(gline ge 400 and gline lt 1500) then slit_poly=3
        if(gline ge 1500 and gline lt 3000) then slit_poly=4
        if(gline ge 3000) then slit_poly=5
    endif
    if(cgline_idx gt 0 and keyword_set(slit_bspl)) then begin
        for i=0,nterms-1 do begin
            if(slit_medw gt 1) then begin
                res_arr_cur_tmp=transpose(res_arr[i,gline_idx])
                res_arr_cur_med=median(res_arr_cur_tmp,slit_medw)
                rsig=robust_sigma(res_arr_cur_med-res_arr_cur_tmp)
                b_res=where(abs(res_arr_cur_med-res_arr_cur_tmp) gt 3.0*rsig,cb_res,compl=g_res,ncompl=cg_res)
                gline_idx=gline_idx[g_res]
                cgline_idx=cg_res
            endif
            res_arr_cur=transpose(res_arr[i,*])
            sset=bspline_iterfit(double(gline_idx),res_arr_cur[gline_idx],nbkpts=slit_nbkpts)
            res_arr[i,*]=bspline_valu(dindgen(ny),sset)
        endfor
    endif
    if(slit_poly eq 0 and cgline_idx gt 0) then res_arr[*,gline_idx]=(median(res_arr,dim=2)) # (dblarr(gline)+1d)
    if(slit_poly gt 0 and cgline_idx gt 0) then begin
        if(gline gt slit_poly+2) then begin
            for i=0,nterms-1 do begin
                p_k=robust_poly_fit(double(gline_idx),res_arr[i,gline_idx],slit_poly)
;                res_arr[i,gline_idx]=poly(double(gline_idx),p_k)
                res_arr[i,*]=poly(dindgen(ny),p_k)
            endfor
        endif
    endif

    return,res_arr
end

function mos_measure_emline,image_type,wl_line,win_line=win_line,slitsuff=slitsuff,$
    wdir=wdir,mask=mask,slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,$
    slit_medw=slit_medw,slit_bspl=slit_bspl,slit_nbkpts=slit_nbkpts,force_image_type_meta=force_image_type_meta,nterms=nterms,nowlshift=nowlshift

    if(n_params() eq 1) then begin
        wl_line=image_type
        image_type='obj'
    endif

    if(n_elements(slitsuff) ne 1) then slitsuff='' ;; '_slits'
    if(n_elements(slit_medw) ne 1) then slit_medw=1

    if(n_elements(slit_poly) ne 1) then slit_poly=-1

    if(n_elements(win_line) ne 1) then win_line=10.0 ;;; in Anstroem by default, gets corrected later using parse_spechdr
    
    image_type_meta=(image_type eq 'obj-sky' or $
                     image_type eq 'obj_abs' or $
                     image_type eq 'obj_abs_err' or $
                     image_type eq 'obj_counts' or $
                     image_type eq 'obj_counts_err' or $
                     ~file_test(wdir+image_type+'_dark.fits'))? 'obj' : image_type
    if(n_elements(force_image_type_meta) eq 1) then image_type_meta=force_image_type_meta
    h0=headfits(wdir+image_type_meta+'_dark.fits',ext=1)

    n_slits=n_elements(mask)

    ny=sxpar(h0,'NAXIS2')
    disper=mrdfits(wdir+'disper_table.fits',1,/silent)

    line_str={wl_line:wl_line,$
        wl:!values.d_nan,flux:!values.d_nan,sigma:!values.d_nan,bgr:!values.d_nan,$
        x_mask:!values.f_nan,y_mask:!values.f_nan,slit:-1l}
    line_par=replicate(line_str,ny)

  if n_slits gt 0 then begin
    for i=0,n_slits-1 do begin
        print,'Computing line flux in slit: ',i+1
        slit_lin=mrdfits(wdir+image_type+slitsuff+'_lin.fits',i+1,h,/silent)
        ny_cur=sxpar(h,'NAXIS2')
        y_off=sxpar(h,'YOFFSET')
        parse_spechdr,h,wl=wl,unitwl=unitwl
        wluscl=(unitwl eq 'nm')? 0.1d : 1d
        val_arr=slit_measure_emline(slit_lin,wl,wl_line*wluscl,win_line,status=status,slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,slit_medw=slit_medw,slit_bspl=slit_bspl,slit_nbkpts=slit_nbkpts,nterms=nterms)
        if(status ne 0) then continue
        gline=where(finite(val_arr[0,*]) eq 1,cgline)
        yidx=y_off+lindgen(ny_cur)
        line_par[gline+y_off].wl_line=wl_line
        line_par[yidx].wl=(keyword_set(nowlshift))? wl_line : transpose(val_arr[1,*])
        line_par[yidx].flux=transpose(val_arr[0,*])
        line_par[yidx].sigma=transpose(val_arr[2,*])
        line_par[yidx].bgr=transpose(val_arr[3,*])
        line_par[yidx].x_mask=disper[yidx].x_mask
        line_par[yidx].y_mask=disper[yidx].y_mask
        line_par[yidx].slit=disper[yidx].slit
    endfor
  endif
  
  return,{slits:line_par}

end
