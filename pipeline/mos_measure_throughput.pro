function bin_hr_spec,wl_hr,spec_hr,wl_lr
    n_wl=n_elements(wl_lr)
    s_s=size(spec_hr)
    spec_lr=(s_s[0] eq 1)? dblarr(n_wl) : (s_s[0] eq 2)? dblarr(n_wl,s_s[2]) : dblarr(n_wl,s_s[2],s_s[3])
    wl_lr_cur=dblarr(n_wl)+!values.d_nan
    spec_hr_cnt=histogram(wl_hr,min=wl_lr[0]-(wl_lr[1]-wl_lr[0])/2d,$
                                max=wl_lr[n_wl-1]+(wl_lr[n_wl-1]-wl_lr[n_wl-2])/2d,$
                                binsize=(wl_lr[1]-wl_lr[0]),reverse_ind=ind_spec_hr)
    n_bins=(n_elements(spec_hr_cnt) < n_wl)

    for i=1L,n_bins-1L do $
        if((ind_spec_hr[i]-ind_spec_hr[i-1]) gt 0) then begin
            case s_s[0] of
                1 : spec_lr[i-1]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                2 : spec_lr[i-1,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                3 : spec_lr[i-1,*,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*,*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
            endcase
            
            wl_lr_cur[i-1]=total(wl_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
        endif
    g_wl=where(finite(wl_lr_cur) eq 1, cg_wl)
    if(cg_wl eq n_elements(wl_lr_cur)) then return, spec_lr

    case s_s[0] of
        1 : spec_lr=interpol(spec_lr[g_wl],wl_lr_cur[g_wl],wl_lr,/spl)
        2 : for x=0,s_s[2]-1 do $
                spec_lr[*,x]=interpol(spec_lr[g_wl,x],wl_lr_cur[g_wl],wl_lr,/spl)
        3 : for x=0,s_s[2]-1 do $
                for y=0,s_s[3]-1 do $
                    spec_lr[*,x,y]=interpol(spec_lr[g_wl,x,y],wl_lr_cur[g_wl],wl_lr,/spl)
    endcase

    return, spec_lr
end

function mos_measure_throughput,star,absstar,n_slit=n_slit,plot=plot,lintails=lintails,$
    noext=noext,star_extracted=star_obs,box=box,wl_off=wl_off,blue_thr=blue_thr,n_wl_seg=n_wl_seg,$
    mirror_area=mirror_area,gain=gain,hdrext=hdrext,flatflux=flatflux,$
    expcorr=expcorr,telluric=telluric,Vmag_tell=Vmag_tell,el=el$
    outputfile=outputfile
;+
; NAME:
;	mos_measure_throughput
; PURPOSE:
;       Spectral throughput measurement
; DESCRIPTION:
;       Binospec throughput measurement using a standard star spectrum
; CALLING SEQUENCE:
;	mos_measure_throughput,'star_slits_lin.fits','calib_Bino/stdstars/fg191b2b_lowres.dat,/plot,n_slit=1
; CATEGORY:
;	CfA MOS/IFU data reduction
; INPUTS:
;	star: linearized rectified stellar spectrum
;       absstar: a filename with the absolute spectrophotometry
; OUTPUTS:
;	a structure containing the throughput + conversion from counts to erg/cm^2/s/A
; OPTIONAL OUTPUT:
;	no
; OPTIONAL INPUT KEYWORDS:
;       n_slit: the slit number to contain the stellar spectrum, defaults to 1
;	plot: plot the result on the screen
; RESTRICTIONS:
;	no
; NOTES:
;	no;
; PROCEDURES USED:
;	Functions :  
;	Procedures:  
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, SAO, 2017/11/11

    if(n_elements(n_slit) ne 1) then n_slit=1
    if(n_elements(wl_off) ne 1) then wl_off=0.0
    if(n_elements(n_wl_seg) ne 1) then n_wl_seg=20

    h_pri=headfits(star)
    star_lin=mrdfits(star,n_slit,h_star,/silent)
    if(keyword_set(hdrext)) then h_pri=h_star

    if(n_elements(flatflux) eq sxpar(h_star,'NAXIS1')) then star_lin=star_lin*rebin(flatflux,sxpar(h_star,'NAXIS1'),sxpar(h_star,'NAXIS2'))


    Nx=sxpar(h_star,'NAXIS1')
    Ny=sxpar(h_star,'NAXIS2')

    star_prof=median(star_lin[0.45*nx:0.55*nx,*],dim=1)
    max_prof=max(star_prof,idxmax,/nan)
    w_star=(30 < (Ny-idxmax-1) < idxmax)
    
    star_lin=star_lin[*,idxmax-w_star:idxmax+w_star]
    Ny=w_star*2+1
    sxaddpar,h_star,'NAXIS2',Ny
    xvec=findgen(Ny)
    star_lin_orig=star_lin
    star_prof=median(star_lin[0.45*nx:0.55*nx,*],dim=1)
    goodpix=where(finite(star_prof) eq 1,cgood,compl=badpix,ncompl=cbad)
    if(cgood gt 10) then begin
        t=mpfitpeak(xvec[goodpix],star_prof[goodpix],gg,nterms=4)
        peakpos=gg[1]
    endif

    w_peak=25 < (w_star-3)
    for x=0,nx-1 do begin
        goodpix=where(finite(star_lin[x,*]) eq 1,cgood,compl=badpix,ncompl=cbad)
        if(cgood gt 10) then begin
            t=mpfitpeak(xvec[goodpix],transpose(star_lin[x,goodpix]),gg,nterms=4)
            t1=mpfitpeak(xvec[goodpix],transpose(star_lin[x,goodpix]),gg1,/moffat,nterms=5)
            if(cbad gt 0) then begin
                ;gauss_funct,xvec,gg,gauss_model,pder_model
                gauss_model=gg[0]*exp(-((xvec-gg[1])/gg[2])^2/2d)+gg[3]
                star_lin[x,badpix]=gauss_model[badpix]
            endif
            star_lin[x,*]=star_lin[x,*]-gg1[4]
        endif else star_lin[x,*]=!values.f_nan
    endfor

    parse_spechdr,h_star,wl=wl,unitwl=unitwl
    wl+=wl_off
    if(unitwl eq 'nm') then wl*=10.0
    dlam=wl-shift(wl,1)
    clip_wl_pix=8 ;;; 8pix clipping on either side of a spectrum

    Texp=sxpar(h_pri,'EXPTIME')
    if(n_elements(expcorr) eq 1) then Texp*=expcorr
	if(n_elements(el) eq 0) then begin
	    el=sxpar(h_pri,'EL',count=n_el)
	    if(n_el eq 0) then el=sxpar(h_pri,'MOUNT_EL',count=n_el)
	    if(n_el eq 0) then el=double(sxpar(h_pri,'ALT',count=n_el)) ;; CMO
	    if(n_el eq 0) then el=90.0
	endif
    Z=90.0-float(el)

    object_name=strcompress(string(sxpar(h_star,'slitobj')),/remove_all)
    date_obs=strcompress(sxpar(h_pri,'DATE-OBS'))

    if(n_elements(gain) ne 1) then gain=1.0
    S=(n_elements(mirror_area) eq 1)? mirror_area : !pi*((650.0/2)^2-(100.0/2)^2) ;total square mirror of telescope in cm^2, defaults to Magellan/MMT

    if(keyword_set(telluric)) then begin
        if(absstar eq '') then absstar=getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/stdstars/vega_kurucz_R20000_atm.fits.gz'
        star_model=mrdfits(absstar,1,/silent)

        if(n_elements(Vmag_tell) eq 1) then begin
            v_b90 = read_asc(getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/stdstars/V_B90.dat',1) ;;; Bessel 1990 V filter
            model_v=bin_hr_spec(star_model.wave,star_model.flux,transpose(v_b90[0,*]))
            model_flux_v=total(model_v*v_b90[1,*])/total(v_b90[1,*])
            k_norm = model_flux_v/3.63d-9*10d^(0.4*Vmag_tell)
            star_flux=star_model.flux/k_norm ;;; now the star is in F_lambda erg/cm^2/s/A
            star_flux=star_flux*gain*mirror_area*5.034e+7*star_model.wave ;;wl ;;/(wl[1]-wl[0]) ;;; erg/s/A to photons/s/A conversion, wavelength is in A
        endif else begin
            star_flux=star_model.flux/median(star_model[where(star_model.wave gt wl[clip_wl_pix] and star_model.wave lt wl[nx-clip_wl_pix-1l])])
        endelse
        flux_std=[transpose(star_model.wave),transpose(star_flux)]
    endif else flux_std=read_asc(absstar)

    wl_orig=transpose(flux_std[0,*])
    new_wl_bin=5d ;; bin everything into 5A intervals
    if(wl_orig[1]-wl_orig[0] lt new_wl_bin) then begin
        wl_orig_new=ceil(wl_orig[0]/new_wl_bin)*new_wl_bin + $
            dindgen(floor(wl_orig[n_elements(wl_orig)-1]/new_wl_bin) - ceil(wl_orig[0]/new_wl_bin) + 1l)*new_wl_bin
        flux_orig_new=bin_hr_spec(wl_orig,transpose(flux_std[1,*]),wl_orig_new)
        flux_std=[transpose(wl_orig_new),transpose(flux_orig_new)]
        wl_orig=wl_orig_new
    endif

    g_wl_orig=where(wl_orig ge wl[clip_wl_pix] and wl_orig le wl[nx-clip_wl_pix-1l], cg_wl_orig)
    if(cg_wl_orig gt 0) then begin  ;;; clipping standard star wavelength scale to that of a spectrum
        if(g_wl_orig[0] gt 0) then begin 
            g_wl_orig=[g_wl_orig[0]-1l,g_wl_orig]
            cg_wl_orig++
        endif
        if(g_wl_orig[cg_wl_orig-1l] lt n_elements(wl_orig)-1l) then begin 
            g_wl_orig=[g_wl_orig,g_wl_orig[cg_wl_orig-1l]+1l]
            cg_wl_orig++
        endif
        wl_orig=wl_orig[g_wl_orig]
        flux_std=flux_std[*,g_wl_orig]
    endif

    atm_trans=read_asc(getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/stdstars/atm_trans_zenith.dat')
    atm_trans_cur=interpol(((1.0/((1.0/transpose(atm_trans[1,*])-1.0)/cos(Z/!radeg)+1.0))),transpose(atm_trans[0,*]),wl_orig)
    ;;;flux_std[1,*]=flux_std[1,*]*transpose(atm_trans_cur)
    flux_orig=transpose(flux_std[1,*])*1e-16
    flux_obswl=interpol(flux_std[1,*],flux_std[0,*],wl)*1e-16
    counts_orig=flux_orig*S*wl_orig*1d-8/(6.625d-27*2.99792458d10) ;;; (wl/5500.0)*2.767e11
    counts_obs=flux_obswl*S*wl*1d-8/(6.625d-27*2.99792458d10)

    star_obs=fltarr(Nx)
    ;computing the stellar spectrum in e-/A/sec
    w=3 ;; 15
    print,'Star: 0%',format='(a,$)'
    perc0=0
    for k=w,Nx-w-1 do begin
        perc=100.0*(k-w+1)/double(Nx-2*w)
        if(perc ge perc0+2) then begin
            perc0=fix(perc)
            if(20*(perc0/20) eq perc0 and perc0 lt 100) then $
                print,string(perc0,format='(i2)')+'%',format='(a,$)' $
            else $
                print,'.',format='(a,$)'
        endif
        star_obs[k]=total(star_lin[k,*],/nan)
    endfor
    print,'100%'
    star_obs_orig=star_obs

    ;sigma clipping on a star
    med_w=5
    star_med=median(star_obs,med_w)
    goodstar=where(finite(star_obs-star_med) eq 1,compl=badstar,ncompl=cbadstar)
    goodmedstar=where(finite(star_med) eq 1 and star_med gt 0.0,compl=badmedstar,ncompl=cbadmedstar)
    sig_star=robust_sigma((star_obs-star_med)[goodstar])
    star_bad=where((abs(star_obs-star_med) gt 8*sig_star) $
                    or (star_obs eq 0) $
                    or (finite(star_obs) ne 1),b_cnt,comp=star_good)
    if((b_cnt gt 0) and (total(star_good) ge 0)) then begin
        s_g=interpol(smooth(star_obs[star_good],med_w,/nan),star_good,findgen(n_elements(star_obs)))
        star_obs=s_g
    endif
    if(star_good[0] gt 0 and star_good[0] lt 10) then begin
        c_f=robust_poly_fit(double(star_good[0:5]),star_obs[star_good[0:5]],1)
        star_obs[0:star_good[0]-1]=poly(findgen(star_good[0]),c_f)
        star_med=median(star_obs,med_w)
        goodstar=where(finite(star_obs-star_med) eq 1,compl=badstar,ncompl=cbadstar)
        goodmedstar=where(finite(star_med) eq 1 and star_med gt 0.0,compl=badmedstar,ncompl=cbadmedstar)
    endif

    ext_vec=calc_extinction(wl,Z)
    if(n_elements(outputfile) eq 1) then begin
        openw,u,outputfile,/get_lun
        wlgood=where(wl gt 3700 and wl lt 10000,cwlgood)
        printf,u,'   WL(A)       e-          e-/A/s   1/ext'
        for i=wlgood[0],wlgood[cwlgood-1] do printf,u,wl[i],star_obs[i],star_obs[i]*gain/Texp/dlam[i],ext_vec[i],format='(f9.3,2f12.1,f9.5)'
        close,u
        free_lun,u
    endif
    star_obs=star_obs*gain/Texp ;;/(dlam)
    if(cbadmedstar gt 0) then star_obs[badmedstar]=!values.f_nan

    ; correction for the atmospheric extinction
    if(~keyword_set(noext)) then star_obs=star_obs/ext_vec


    ; calculation of the quantum efficiency and spectral sensitivity
    star_wl=bin_hr_spec(wl,star_obs,wl_orig)/atm_trans_cur
    n_wl_lr=n_elements(star_wl)
    dqebin=star_wl/counts_orig

    ; smoothing
    gbin_t=where(finite(dqebin) eq 1,cgbin_t)
    if(gbin_t[0] gt 0 and ~keyword_set(lintails)) then dqebin[0:gbin_t[0]-1]=dqebin[gbin_t[0]]
    if(gbin_t[cgbin_t-1] lt n_elements(wl_orig)-1 and ~keyword_set(lintails)) then dqebin[gbin_t[cgbin_t-1]:*]=dqebin[gbin_t[cgbin_t-1]]

    star_wl_mask=bytarr(n_wl_lr)+1

    ;;; mask Balmer, Paschen, and Brackett lines lines
    n_hyd=30 ;; first 30 lines in every series
    w_hyd=30/((~keyword_set(telluric))*2+1.0) ;; 30A around every line; x1.5 for h-alpha/pa-alpha/br-alpha
    wl_vac_h  = 1d/((1/2d^2-1/(3+dindgen(n_hyd))^2)*1.09677583d-3)
    wl_vac_pa = 1d/((1/3d^2-1/(4+dindgen(n_hyd))^2)*1.09677583d-3)
    wl_vac_br = 1d/((1/4d^2-1/(5+dindgen(n_hyd))^2)*1.09677583d-3)
    for l=0,n_hyd-1 do begin
        idx_bad = where(abs(wl_orig-wl_vac_h[l])  lt w_hyd*(1.0+0.5*(l le 5)-0.4*(l ge 5)-0.5*(l ge 8)-0.1*(l ge 12)) or $ ; double width for H-alpha/beta/gamma/delta
                        abs(wl_orig-wl_vac_pa[l]) lt w_hyd*(1.0+0.5*(l eq 0)) or $
                        abs(wl_orig-wl_vac_br[l]) lt w_hyd*(1.0+0.5*(l eq 0)), cidx_bad)
        if(cidx_bad gt 0) then star_wl_mask[idx_bad]=0
    endfor
        
    if(keyword_set(telluric)) then begin ;;; mask deep telluric bands
        idx_bad_tell=where(abs(wl_orig-6890.) lt 40. or $
                           abs(wl_orig-7600.) lt 60. or $
                           abs(wl_orig-7170.) lt 60, cidx_bad_tell)
        if(cidx_bad_tell gt 0) then star_wl_mask[idx_bad_tell]=0
    endif
    gbin=where(finite(dqebin) eq 1 and star_wl_mask eq 1,cgbin,compl=bbin,ncompl=cbbin)

    dqebin_int=interpol(dqebin[gbin],wl_orig[gbin],wl_orig)
;    dqebin1=smooth_lowess((dindgen(n_wl_lr))[gbin],dqebin[gbin],round(n_wl_lr/60.0))
    dqebin1=poly_smooth(dqebin_int,round(n_wl_lr/float(n_wl_seg)),deg=2) ;; less aggressive smoothing

    if(n_elements(blue_thr) ne 1) then blue_thr=4000.0
    blue_wl4000=where(wl_orig lt blue_thr and finite(dqebin) eq 1, cblue_wl4000)
    if(cblue_wl4000 gt 0) then begin
        dqebin1_poly=poly_smooth(dqebin[gbin],round(n_wl_lr/60.0))
        dqebin1[blue_wl4000]=dqebin1_poly[blue_wl4000]
        idx1=cblue_wl4000-1
        idx2=cblue_wl4000-1+10
        ww=findgen(idx2-idx1+1)/(idx2-idx1)
        dqebin1[idx1:idx2]=dqebin1_poly[idx1:idx2]*(1-ww)+dqebin1[idx1:idx2]*ww
    endif

    dqe=interpol(dqebin,wl_orig,wl,/spline)
;    dqe_sm=interpol(dqebin1,wl_orig[gbin],wl) ;,/spline)
    dqe_sm=interpol(dqebin1,wl_orig,wl,/spline)
    good_dqe_sm=where(finite(dqe_sm) eq 1,cgood_dqe_sm)
    if(cgood_dqe_sm gt 0) then dqe[good_dqe_sm]=dqe_sm[good_dqe_sm]

    if(cbadmedstar gt 0) then dqe[badmedstar]=!values.f_nan

    if(keyword_set(plot)) then begin
        plot,wl,dqe*100,xst=1,ys=1,yr=[0,((1.1*max(dqe*100,/nan))<100)],xtitle='Wavelength, A',ytitle='Throughput %'
        oplot,wl_orig,star_wl*100/counts_orig,linestyle=0,col=80,psym=10
        oplot,wl,star_obs*100/counts_obs,linestyle=1,col=128
        oplot,wl_orig,dqebin1*100,linestyle=0,col=254,psym=10
    endif

    if(keyword_set(flatflux)) then dqe=dqe/flatflux
    flux_erg=1d/(dqe*S*wl*1d-8/(6.625d-27*2.99792458d10))
    flux_mJy=flux_erg*wl^2*3.3356E7
    flat_vec=(keyword_set(flatflux))? flatflux : 1d

    dqe_struct={wave:wl,flux:star_obs*flux_erg*1e17/flat_vec,error:sqrt(star_obs>0)*flux_erg*1e17/flat_vec,$
                throughput:dqe/dlam,c_flux_erg:flux_erg,c_flux_mjy:flux_mJy}

    return,dqe_struct
end
