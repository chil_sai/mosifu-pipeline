;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Rectification of MOS 2D slit images
;
; /INVERSE -- if set, the inverse transformation is done
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function mos_rectify_slit,slit_img,dist_map,y_off,edge=edge,inverse=inverse

    s_slit_img=size(slit_img)
    if(s_slit_img[0] ne 2) then begin
        message,/inf,'Only 2-dimensional images are supported. Returning NaN'
        return,slit_img*!values.f_nan
    endif

    xval=(dindgen(15)/14.0)*s_slit_img[1]
    yval=dindgen(s_slit_img[2])+y_off
    yval_dist=poly2d(xval,yval,dist_map.kywrap)
;;    yval_dist=poly2d(xval,yval,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2)+y_off+dist_map.mask_y_scl
    if(keyword_set(inverse)) then $
        polywarp,(xval # (dblarr(s_slit_img[2])+1d)),$
                 ((dblarr(15)+1d) # yval)-y_off,$
                 (xval # (dblarr(s_slit_img[2])+1d)),$
                 yval_dist-y_off,$
             3,kxwr_slit,kywr_slit $
    else $
        polywarp,(xval # (dblarr(s_slit_img[2])+1d)),$
                 yval_dist-y_off,$
                 (xval # (dblarr(s_slit_img[2])+1d)),$
                 ((dblarr(15)+1d) # yval)-y_off,$
                 3,kxwr_slit,kywr_slit
    if(keyword_set(edge)) then begin
        slit_img_p1=shift(slit_img,0,1)
        slit_img_m1=shift(slit_img,0,-1)
        gp1_slit_img=where(finite(slit_img) ne 1 and finite(slit_img_p1) eq 1, cgp1_slit_img)
        gm1_slit_img=where(finite(slit_img) ne 1 and finite(slit_img_m1) eq 1, cgm1_slit_img)
        if(cgp1_slit_img gt 0) then slit_img[gp1_slit_img]=slit_img_p1[gp1_slit_img]
        if(cgm1_slit_img gt 0) then slit_img[gm1_slit_img]=slit_img_p1[gm1_slit_img]
    endif

;;    slit_r=rotate(poly_2d(rotate(slit_img,5),kxwr_slit,kywr_slit,1,missing=!values.f_nan),5)
    slit_r=poly_2d(slit_img,kxwr_slit,kywr_slit,1,missing=!values.f_nan)

    return, slit_r
end
