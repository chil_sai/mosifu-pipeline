pro reduce_lrisred_ls,rawfilename,bias=rawbias,flat=rawflat,arc=rawarc,skyflat=rawskyflat,tmpdir=tmpdir,$
    dist_map_frame=dist_map_frame,stdstar=stdstar,stddata=stddata,crhfill=crhfill,detect_slits=detect_slits,$
    sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat,split1d=split1d,noadjust=noadjust, $
    ndeg_wl=ndeg_wl,hgar=hgar,skylinecorr=skylinecorr,wl_skyline=wl_skyline,lsfcorr=lsfcorr,barycorr=barycorr,$
    skysubtarget=skysubtarget,bright=bright,extapw=extapw,skysubalgorithm=skysubalgorithm,oh=oh,$
    extract=extract,extr_optimal=extr_optimal,extr_detect=extr_detect,extr_estimate=extr_estimate,$
    abscal=abscal,series=series,n_stack=n_stack,n_img_clean=n_img_clean,n_apwmax=n_apwmax,$
    mask_slit_individual=mask_slit_individual,skipsteps=skipsteps,smooth_overscan=smooth_overscan,debug=debug, dithpos=dithpos

    if(n_elements(tmpdir) ne 1) then tmpdir='../test_data/test_dir/'
    if(NOT file_test(tmpdir,/directory)) then file_mkdir, tmpdir
    if(n_elements(extapw) ne 1) then extapw=2
    if(n_elements(skysubalgorithm) ne 1) then skysubalgorithm=1 ;; non-local Kelson
    if(n_elements(wl_skyline) lt 1) then skylinecorr=0
    if(n_elements(skipsteps) eq 0) then skipsteps=['none']
    if(n_elements(n_stack) eq 0) then n_stack=1

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_bias=n_elements(rawbias)
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
    n_dist_map=n_elements(dist_map_frame)
    n_stdstar=n_elements(stdstar)

    if(~keyword_set(skippri)) then begin
        if(n_bias gt 0) then begin
            bias_0=lrisred_preproc(float(readfits(rawbias[0],h_bias,/silent)),h_bias)
            
            bias_cube=fltarr(sxpar(h_bias,'NAXIS1'),sxpar(h_bias,'NAXIS2'),n_bias)
            gain=sxpar(h_bias,'GAIN')
            if(gain eq 0.0) then gain=1.0
            bias_cube[*,*,0]=bias_0*gain
            for i=1,n_bias-1 do bias_cube[*,*,i]=lrisred_prepreoc(float(readfits(rawbias[i],h_bias_cur,/silent))*gain,h_bias_cur)
            bias=(median(bias_cube,dim=3))[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*] ;;; should do resistant_mean instead but takes too long
            writefits,tmpdir+'bias_i.fits',bias,h_bias
            bias_cube=0.0
        endif else bias=0.0
        if(n_obj gt 1) then begin
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    ccd_combine,rawfilename[findgen(n_stack)+n_stack*i+1],tmpdir+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=15.0,bias=bias,mosaic_function='lrisred_preproc'
                ccd_combine,tmpdir+'obj_dark_'+string(findgen(n_obj_total),format='(i2.2)')+'.fits',tmpdir+'obj_dark.fits',nsig=10.0,bias=bias,mosaic_function='lrisred_preproc'
            endif else begin
                ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=7.0,series=series,n_img_clean=n_img_clean,bias=bias,mosaic_function='lrisred_preproc'
            endelse
        endif else ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=10.0,bias=bias,mosaic_function='lrisred_preproc'
        if(n_flat gt 0) then ccd_combine,rawflat,tmpdir+'flat_dark.fits',nsig=10.0,bias=bias,mosaic_function='lrisred_preproc'
        if(n_arc gt 0) then ccd_combine,rawarc,tmpdir+'arc_dark.fits',nsig=35.0,bias=bias,mosaic_function='lrisred_preproc'
        if(n_skyflat gt 0) then ccd_combine,rawskyflat,tmpdir+'skyflat_dark.fits',nsig=7.0,/norm,bias=bias,mosaic_function='lrisred_preproc'
        if(n_dist_map gt 0) then ccd_combine,dist_map_frame,tmpdir+'distortion_frame.fits',nsig=7.0,bias=bias,mosaic_function='lrisred_preproc'
        if(n_stdstar gt 0) then ccd_combine,stdstar,tmpdir+'stdstar_dark.fits',nsig=7.0,bias=bias,mosaic_function='lrisred_preproc'
    endif

    h1=headfits(tmpdir+'obj_dark.fits',ext=1)
    grating=strcompress(sxpar(h1,'GRANAME'),/remove_all)
    wl_c=sxpar(h1,'WAVELEN')

    if not keyword_set(dithpos) then dithpos=strcompress(sxpar(h1,'IELOFF'),/remove_all) else dithpos=strcompress(string(dithpos),/remove);get telescope instrument offset from header in arcsec
    logfile=tmpdir+'test_logfile.txt'
    openw,u,logfile,/get_lun
    printf,u,'SIMPLE  =                                 T / FITS-like header'
    printf,u,'LONGSTRN= ''OGIP 1.0''           / The OGIP long string convention may be used.'
    printf,u,'RAW_DIR = ''/tmp/'''
    printf,u,'R_DIR   = '''+tmpdir+''''
    printf,u,'W_DIR   = '''+tmpdir+''''
    printf,u,'RAWEXT  = ''.gz'''
    printf,u,'INSTRUME= ''SOAR-GOODMAN'' / spectrograph name'
    printf,u,'SLIT    = ''1.0'' / slit name or MOS'
    printf,u,'GRATING = '''+grating+''' / grating'
    printf,u,'FILTER  = '''' / filter'
    printf,u,'DITHPOS = '+dithpos+' / current dithering position in arcsec'
    printf,u,'BRIGHT  = '+string(keyword_set(bright),format='(i1)')
    printf,u,'EXTAPW  = '+string(extapw,format='(i2)')
    printf,u,'END'
    close,u
    free_lun,u

    ndeg=(n_elements(ndeg_wl) eq 1)? ndeg_wl : ((grating eq 'YZY_930')? 4 : 3)

    h_a=headfits(tmpdir+'obj_dark.fits',ext=1)
    nx=sxpar(h_a,'NAXIS1')
    ny=sxpar(h_a,'NAXIS2')

    sky_slit_length_a=ny
    y_ndeg=-1

    ;if(n_dist_map eq 1) then dist_map=distortion_map_generic(tmpdir+'distortion_frame.fits',extnum=1,cuty1=10,cuty2=120,deg2=2)
    s_mask=size(mask)
    dist_map=(s_mask[0] eq 1 and s_mask[2] eq 8)? $
        distortion_map_generic(tmpdir+'flat_dark.fits',extnum=1) : $
        distortion_map_generic(tmpdir+'flat_dark.fits',extnum=1,detect_slits=detect_slits,maskentry=mask)

    wl_ini=(grating eq '600/7500')? [6323.3044+(wl_c-7347.6694),1.2264976, 3.1311246e-05, -7.6633745e-09] : $  ;;; [6329.07, 1.18807, 3.05336e-05, 1.51321e-09] : $
          ((grating eq 'KOSI_600')? [3601.3712,1.3178084,3.4097229e-06,-3.8326294e-09] : [3601.3712,1.3178084,3.4097229e-06,-3.8326294e-09])

    wl0_lin=(grating eq 'YZY_930'? 3683.0 : 3600.0)
    dwl_lin=(grating eq 'YZY_930'? 0.85 : 1.32)
    lin_param={grism:grating, filter:'', wl0:wl0_lin, dwl:dwl_lin, npix:nx, wl_min:wl0_lin, wl_max:wl0_lin+nx*dwl_lin}

    exp_flat=['obj','arc','flat']
    if(n_skyflat gt 0) then exp_flat=[exp_flat,'skyflat']
    if(n_stdstar gt 0) then exp_flat=[exp_flat,'stdstar']

    if(keyword_set(series)) then exp_flat=[exp_flat,'obj_clean'+string(lindgen(n_obj),format='(i03)')]
    ;mask={slit:0,ra:0d,dec:0d,x:0d,y:0d,$
    ;        target:0ll,object:sxpar(h_a,'OBJECT'),type:'TARGET',$
    ;        wstart:0.0,wend:0.0,height:float(Ny),width:1.0,offset:0.0,theta:0.0,$
    ;        bbox:dblarr(8),$
    ;        mask_id:-1l,mask_name:'long_1.0arcsec',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
    ;        mask_pa:0d,corners:[-0.5,-Ny/2,0.5,Ny/2]}

    flat_fielding_mosifu, exp_flat, wdir=tmpdir, dist_map=dist_map, norm_slit=-1, mask=mask, sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat, /fring

    mos_extract_2d_slits,logfile,'obj', wdir=tmpdir,dist_map=dist_map,pady=pady
    if(keyword_set(series)) then for i=0,n_obj-1 do mos_extract_2d_slits,logfile,'obj_clean'+string(i,format='(i03)'), wdir=tmpdir,dist_map=dist_map,pady=pady
    mos_extract_2d_slits,logfile,'arc', wdir=tmpdir,dist_map=dist_map,pady=pady
    mos_extract_2d_slits,logfile,/nflat, wdir=tmpdir,dist_map=dist_map,pady=pady
    if(n_skyflat gt 0) then mos_extract_2d_slits,logfile,'skyflat', wdir=tmpdir,dist_map=dist_map,pady=pady

stop
    if(~keyword_set(skipwl)) then begin
        arc_list_config=(keyword_set(oh))? $
            {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesOI_O2.tab','linesOH_R2k_1050nm_corr.tab'],weight:[1.0,1.0],label:['OI','OH']} : $
            {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesNe_NIST.tab','linesAr_NIST.tab'],weight:[1.0,1.0],label:['NeI','ArI']}

        status=crea_disper_generic_slit(tmpdir+'arc_dark.fits',0,arc_list_config=arc_list_config,$
            wl_ini,3.0,ndeg,smy=3,ndegy=y_ndeg,dist_map=dist_map,use_oh=oh,debug=debug,/fast,disper_table=disper_table,$
            skip_lines=(keyword_set(hgar)? [-1.0,4044.42,4198.32,4960.17,6234.35] : [-1.0]))
    endif

    writefits,tmpdir+'disper_table.fits',0
    mwrfits,disper_table,tmpdir+'disper_table.fits'

    obs_type_arr=['obj','arc']
    if(n_skyflat gt 0) then obs_type_arr=[obs_type_arr,'skyflat']
    if(n_stdstar gt 0) then obs_type_arr=[obs_type_arr,'stdstar']
    for i=0,n_elements(obs_type_arr)-1 do begin
        im=mrdfits(tmpdir+obs_type_arr[i]+'_ff.fits',1,hh)
        if(keyword_set(crhfill)) then begin
           im_m3=median(im,3)
           im_m5=median(im,5)
           b_m3=where(finite(im) ne 1, cb_m3)
           if(cb_m3 gt 0) then im[b_m3]=im_m3[b_m3]
           b_m5=where(finite(im) ne 1, cb_m5)
           if(cb_m5 gt 0) then im[b_m5]=im_m5[b_m5]
        endif
        im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param)
        ;im_lin[*,840:*]=!values.f_nan
        writefits,tmpdir+obs_type_arr[i]+'_lin.fits',0
        mwrfits,im_lin,tmpdir+obs_type_arr[i]+'_lin.fits',hh_out
        if(keyword_set(skylinecorr)) then begin
            if((obs_type_arr[i] eq 'obj') or (obs_type_arr[i] eq 'stdstar')) then begin
                file_delete,tmpdir+'flat_emline*_slits.fits',/allow_nonexistent,/quiet
                skyline_flux_data_arr=mos_create_emline_flat(wl_skyline,win_line_arr=10.0*2.0,wdir=tmpdir,mask=mask,dist_map=dist_map,image_type=obs_type_arr[i],slit_poly=4)
            endif
            im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,skyline_flux_data_arr=skyline_flux_data_arr,lin_param=lin_param)
            ;im_lin[*,840:*]=!values.f_nan
            writefits,tmpdir+obs_type_arr[i]+'_lin.fits',0
            mwrfits,im_lin,tmpdir+obs_type_arr[i]+'_lin.fits',hh_out
        endif
    endfor

    if(skysubalgorithm gt 0) then begin
        ;; simple sky subtraction
        obj_lin=mrdfits(tmpdir+'obj_lin.fits',1,hlin)
        nx_lin=sxpar(hlin,'NAXIS1')
        parse_spechdr,hlin,wl=wl
        p_obj=median(obj_lin[Nx_lin*0.4:Nx_lin*0.6,*],dim=1)
        p_zero=where(p_obj eq 0,cp_zero)
        if(cp_zero gt 0) then p_obj[p_zero]=!values.f_nan
        ;p_obj[0:10]=!values.f_nan
        ;p_obj[830:*]=!values.f_nan
        outer_p_obj=median([p_obj[0:200],p_obj[629:*]])
        min_p_obj=min(p_obj,/nan)
        nbin_p=11
        h_prof=histogram(p_obj,min=min_p_obj,max=2*outer_p_obj-min_p_obj,nbin=nbin_p)
        p_fit=mpfitpeak(min_p_obj+dindgen(nbin_p)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.0),h_prof,g_coeff)
    
;    sky_idx=where(p_obj gt min_p_obj and $
;                  p_obj le ((min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.)) < outer_p_obj), csky)
        sky_idx=where(p_obj gt min_p_obj and $
                      p_obj le min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.), csky)
        resistant_mean,obj_lin[*,sky_idx],3.0,skyvec,dim=2
        writefits,tmpdir+'obj-sky_lin.fits',0
        mwrfits,obj_lin-rebin(skyvec,nx_lin,sxpar(hlin,'NAXIS2')),tmpdir+'obj-sky_lin.fits',hlin
    endif

    flat_norm=mrdfits(tmpdir+'flat_norm.fits',1,hh,/silent)
    flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map)
    flatflux=median(flat_lin[*,sxpar(hh,'NAXIS2')/2-20:sxpar(hh,'NAXIS2')/2+20],dim=2)
    gfl=where(finite(flatflux) eq 1,cgfl,compl=bfl,ncompl=cbfl)
    if(cbfl gt 0 and cgfl gt 10) then flatflux=interpol(flatflux[gfl],float(gfl),findgen(cbfl+cgfl))
    flatflux[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]=(poly_smooth(flatflux,(cbfl+cgfl)/20.0,deg=2))[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]

    obj_lin=mrdfits(tmpdir+'obj_lin.fits',1,hlin,/silent)
    err_obj_lin=sqrt((sxpar(hlin,'RDNOISE')+obj_lin*flat_lin)>0)/flat_lin

    if(n_stdstar gt 0 and n_elements(stddata) eq 1) then begin
        print,'Computing a throughput curve using a standard star spectrum'
        fluxcal_data=mos_measure_throughput(tmpdir+'stdstar_lin.fits',stddata,$
            mirr=!pi*((410.0/2)^2-(130.0/2)^2),/hdrext,n_wl_seg=10,$
            flatflux=flatflux,blue_thr=wl0_lin,expcorr=1.0/n_stdstar,/lintails)

        if(keyword_set(abscal)) then begin
            parse_spechdr,hlin,wl=wllin
            ext_vec=calc_extinction(wllin,90.0-float(sxpar(hlin,'MOUNT_EL',count=n_el)))
            sxaddpar,hlin,'BUNIT','1.0E-17 erg/cm^2/s/A'
            obj_lin_abs=obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
            writefits,tmpdir+'obj_lin_abs.fits',0
            mwrfits,obj_lin_abs,tmpdir+'obj_lin_abs.fits',hlin

            err_obj_abs=err_obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
            writefits,tmpdir+'err_obj_abs.fits',0
            mwrfits,err_obj_abs,tmpdir+'err_obj_abs.fits',hlin

            if(file_test(tmpdir+'obj-sky_lin.fits')) then begin
                obj_sky=mrdfits(tmpdir+'obj-sky_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
                obj_abs=obj_sky*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
                writefits,tmpdir+'obj_abs.fits',0
                mwrfits,obj_abs,tmpdir+'obj_abs.fits',hsky
            endif

            if(file_test(tmpdir+'stdstar_lin.fits')) then begin
                stdstar_lin=mrdfits(tmpdir+'stdstar_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
                stdstar_abs=stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
                writefits,tmpdir+'stdstar_lin_abs.fits',0
                mwrfits,stdstar_abs,tmpdir+'stdstar_lin_abs.fits',hsky
            endif
        endif
    endif

end
