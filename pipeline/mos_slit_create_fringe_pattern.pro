function mos_slit_create_fringe_pattern,flat_slit,nmin=nmin,med_w=med_w
    if(n_elements(nmin) ne 1) then nmin=0
    if(n_elements(med_w) ne 1) then med_w=0 ;; 3
    ymar=2
    s_f=size(flat_slit)
    n_slice=(s_f[0] eq 3)? s_f[3] : 1
    fringes_slit=flat_slit*0.0

    for i=(nmin<(n_slice-1)),n_slice-1 do begin
        fringes_slit[*,*,i]=flat_slit[*,*,i]/mos_slit_process_flat(flat_slit[*,*,i],/nopixvar)-1d
        if(ymar gt 0) then begin
            for y=0,ymar-1 do begin
                fringes_slit[*,y,i]=fringes_slit[*,ymar,i]
                fringes_slit[*,s_f[2]-y-1,i]=fringes_slit[*,s_f[2]-ymar-1,i]
            endfor
        endif
        if(med_w gt 1) then fringes_slit[*,*,i]=median(fringes_slit[*,*,i],med_w)
    endfor

    return,fringes_slit
end
