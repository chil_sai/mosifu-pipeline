function mos_slit_correct_fringes,data,fringe_pattern,smw=smw,nmin=nmin
    if(n_elements(nmin) ne 1) then nmin=0
    if(n_elements(smw) ne 1) then smw=41
    data_corr=data
    s_d=size(data)
    n_slice=(s_d[0] eq 3)? s_d[3] : 1

    x=dindgen(s_d[1])
    for i=(nmin<(n_slice-1)),n_slice-1 do begin
        ord_sm=data[*,*,i]
        for y=0,s_d[2]-1 do begin
            g=where(finite(ord_sm[*,y]) eq 1,cg)
            if(cg gt 200) then begin
                sset=bspline_iterfit(x[g],ord_sm[g,y],ivar=dblarr(cg)+1,bkspace=smw)
                ord_sm[*,y]=bspline_valu(x,sset)
            endif else begin
                ord_sm[*,y]=median(ord_sm[*,y],smw)
            endelse
        endfor
        data_corr[*,*,i]=data[*,*,i]-ord_sm*fringe_pattern[*,*,i]
    endfor

    return,data_corr
end
