; order2d: a 2-dimensional array, linearized and rectified single order

function extract_lin_order,order2d,var2d=var2d,psf,sky=sky,error=error,clean=clean,thres=thres,sum_extraction=sum_extraction

    s_ord=size(float(order2d))
    if(n_elements(var2d) ne n_elements(order2d)) then var2d=order2d*0d +1d
    s_psf=size(float(psf))
    if(n_elements(thres) ne 1) then thres=0.01

    if(not (array_equal(s_ord,s_psf) or ((s_psf[0] eq 1) and (s_ord[2] eq s_psf[1])))) then begin
        message,/inf,'PSF and the input order image have incompatible dimensions'
        return,-1
    endif

    psf2d=(s_psf[0] eq 1)? congrid(transpose(psf),s_ord[1],s_ord[2]) : psf
    ord_extr_fit=dblarr(s_ord[1])+!values.d_nan
    sky=ord_extr_fit
    error=ord_extr_fit
    sum_extraction=total(order2d*psf2d,2,/nan)

    n_maxprof=1.5
    for x=0,s_ord[1]-1 do begin
        prof_cur=transpose(order2d[x,*])
        ;eprof_cur=sqrt((abs(transpose(psf2d[x,*])))>1d-5) ;;;; prof_cur*0d +1d ;;; has to be correctly computed
        eprof_cur=transpose(var2d[x,*])
        prof_maxval=max((median(abs(prof_cur)/eprof_cur,3))[1:s_ord[2]-2],/nan)
        gdata=where((finite(prof_cur/eprof_cur) eq 1) and (abs(prof_cur)/eprof_cur lt n_maxprof*prof_maxval), cgdata)
        if(cgdata lt 10) then continue
        c=regress(transpose(psf2d[x,gdata]),prof_cur[gdata],/double,chisq=chi2,const=c0,status=status,measure_errors=sqrt(eprof_cur[gdata]))
        if(status ne 0) then continue
        if(keyword_set(clean)) then begin
            dprof=(c0+c[0]*transpose(psf2d[x,*])-prof_cur[*])/total(abs(prof_cur),/nan)
            gdata1=where(((finite(dprof) eq 1) and (abs(prof_cur)/eprof_cur lt n_maxprof*prof_maxval)) and ((abs(dprof) lt thres) or $
                          abs(c0+c[0]*transpose(psf2d[x,*])-prof_cur[*]) lt 10d*sqrt(eprof_cur)), cgdata1)
            if(cgdata1 lt 10) then continue
;if(x eq 1526) then stop
;if (cgdata1 lt s_ord[2]) then print,'x,cgdata1=',x,cgdata1
            c=regress(transpose(psf2d[x,gdata1]),prof_cur[gdata1],/double,chisq=chi2,const=c0,status=status,measure_errors=sqrt(eprof_cur[gdata1]))
            gdata=gdata1
        endif
        ;ord_extr_fit[x]=c[0]
        ord_extr_fit[x]=total(transpose(psf2d[x,gdata])*prof_cur[gdata]/eprof_cur[gdata])/total(transpose(psf2d[x,gdata])^2/eprof_cur[gdata])
        error[x]=sqrt(total(transpose(abs(psf2d[x,gdata])))/total(transpose(psf2d[x,gdata])^2/eprof_cur[gdata]))
        sky[x]=c0
    endfor
    return,ord_extr_fit
end
