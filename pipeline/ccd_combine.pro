function parse_secstr,secstr,ccdsum=ccdsum
;;; parses fits keywords in the format: '[1:8192,1:4112]' into lonarr(4)
    if(n_elements(ccdsum) ne 1) then ccdsum='1 1'
    ccdbin=(strsplit(ccdsum,' ',/extr))[0:1]
    res_arr=long(strsplit(strmid(secstr,1,strlen(secstr)-2),'([:,])',/regex,/extract))
    res_arr[0]=(res_arr[0]-1)/ccdbin[0]+1
    res_arr[2]=(res_arr[2]-1)/ccdbin[1]+1
    res_arr[1]/=ccdbin[0]
    res_arr[3]/=ccdbin[1]
    return,res_arr
end

pro ccd_combine,inpfilelist,outfile,norm=norm,nsig=nsig,gain=gain,readn=readn,$
    series=series,n_img_clean=n_img_clean,extnum=extnum,xtraext=xtraext,$
    trimx1=trimx1,trimy1=trimy1,trimx2=trimx2,trimy2=trimy2,bias=bias,$
    dark_expnorm=dark_expnorm,rotate=rotdir,filter_negative=filter_negative,difference=difference,$
    mosaic_function=mosaic_function,$
    overscan_remove=overscan_remove,subtract_overscan=subtract_overscan,$
    overscan_directions=overscan_directions,overscan_datasec=overscan_datasec,$
    overscan_smooth=overscan_smooth,overscan_median=overscan_median,overscan_noclean=overscan_noclean

    n_inp=n_elements(inpfilelist)

;    trimx1=(n_elements(trimx1) ne 1)? 0 : trimx1_in
;    trimx2=(n_elements(trimx2) ne 1)? 0 : trimx2_in
;    trimy1=(n_elements(trimy1) ne 1)? 0 : trimy1_in
;    trimy2=(n_elements(trimy2) ne 1)? 0 : trimy2_in
	if(n_elements(trimx1) eq 0 ) then trimx1=0
    if(n_elements(trimx2) eq 0 ) then trimx2=0
    if(n_elements(trimy1) eq 0 ) then trimy1=0
    if(n_elements(trimy2) eq 0 ) then trimy2=0

    if(n_elements(extnum) eq 0) then extnum=0

    if(n_elements(rotdir) ne 1) then rotdir=0
    print, 'trimx1: '+string(trimx1)
    print, 'trimy1: '+string(trimy1)
    print, 'trimx2: '+string(trimx2)
    print, 'trimy2: '+string(trimy2)
    mkhdr,hdrtmp,dblarr(20,20),/image ;; temporary image header
    h_pri=headfits(inpfilelist[0])
    for e=0,n_elements(extnum)-1 do begin
        h_a=headfits(inpfilelist[0],ext=extnum[e])
        if(strcompress(string(sxpar(h_a,'XTENSION')),/remove_all) eq 'BINTABLE') then $
            if(strcompress(string(sxpar(h_a,'TTYPE1')),/remove_all) eq 'COMPRESSED_DATA') then begin
                rtmp=mrdfits(inpfilelist[0],extnum[e],h_a,/silent)
            endif
        obzero=sxpar(h_a,'BZERO')
        sxaddpar,h_a,'BZERO',0
        if(extnum[e] eq 0) then h_0=headfits(inpfilelist[0]) else begin
            h_0=h_a
            sxdelpar,h_0,'NAXIS1'
            sxdelpar,h_0,'NAXIS2'
            sxdelpar,h_0,'NAXIS3'
            sxdelpar,h_pri,'NAXIS1'
            sxdelpar,h_pri,'NAXIS2'
            sxdelpar,h_pri,'NAXIS3'
            h_a[0]=hdrtmp[0]
            sxaddpar,h_a,'EXTNAME','SPECTRUM01'
        endelse
        if(n_elements(gain) ne 1) then gain=sxpar(h_0,'GAIN')
        if(gain eq 0.0) then gain=1.0
        if(n_elements(readn) ne 1) then readn=sxpar(h_0,'RDNOISE')
        if(readn eq 0.0) then readn=1.0
        nx_a=sxpar(h_a,'NAXIS1')
        ny_a=sxpar(h_a,'NAXIS2')

        if(keyword_set(overscan_remove)) then begin
            if(n_elements(overscan_datasec) ne 4) then begin
                datasec_str=sxpar(h_a,'DATASEC',count=ndatasec)
                if(ndatasec eq 0) then datasec_str=sxpar(h_a,'TRIMSEC',count=ndatasec)
                overscan_datasec=(ndatasec eq 0)? [1,nx_a,1,ny_a] : parse_secstr(datasec_str)
            endif
            nx_a=overscan_datasec[1]-overscan_datasec[0]
            ny_a=overscan_datasec[3]-overscan_datasec[2]
        endif

        nx_a=nx_a-trimx1-trimx2
        ny_a=ny_a-trimy1-trimy2

        c_a=dblarr(nx_a,ny_a,n_inp)
        exptime_a=0.0

        for i=0,n_inp-1 do begin
            img=(float(mrdfits(inpfilelist[i],extnum[e],h_a_c,/silent)))
            if(n_elements(mosaic_function) eq 1) then img=call_function(mosaic_function,img,h_a_c)
            if(keyword_set(overscan_remove)) then $
                img=remove_overscan(img,overscan_datasec,subtract=subtract_overscan,$
                        directions=overscan_directions,sm_w=overscan_smooth,$
                        median=overscan_median,noclean=overscan_noclean) $
            else img+=sxpar(h_a_c,'BZERO')
            if(i eq 0 and n_elements(mosaic_function) eq 1) then begin
                s_new=size(img)
                nx_a=s_new[1]-trimx1-trimx2
                ny_a=s_new[2]-trimy1-trimy2
                c_a=dblarr(nx_a,ny_a,n_inp)
            endif
            c_a[*,*,i]=img[trimx1:trimx1+nx_a-1,trimy1:trimy1+ny_a-1]
            c_a[*,*,i]*=gain
            sxaddpar,h_a_c,'BZERO',0
            exp_cur=sxpar(h_a_c,'EXPTIME',count=cexp_cur)
            if(cexp_cur eq 0) then exp_cur=sxpar(h_a_c,'EXPOSURE',count=cexp_cur)
            if(cexp_cur eq 0) then exp_cur=1.0
            if(n_elements(dark_expnorm) eq n_elements(c_a[*,*,i])) then c_a[*,*,i]-=dark_expnorm*exp_cur
            sxaddpar,h_a_c,'EXPTIME',exp_cur
            exptime_a+=exp_cur
        endfor

        if(keyword_set(norm)) then begin
            dx_c=(500 < (nx_a/4))
            dy_c=(500 < (ny_a/4))
            n_a=median(c_a[nx_a/2-dx_c:nx_a/2+dx_c,ny_a/2-dy_c:ny_a/2+dy_c,0])
            for i=1,n_inp-1 do begin
                n_a_cur=median(c_a[nx_a/2-dx_c:nx_a/2+dx_c,ny_a/2-dy_c:ny_a/2+dy_c,i])
                c_a[*,*,i]=c_a[*,*,i]/n_a_cur*n_a
            endfor
        endif
        sxaddpar,h_a,'EXPTIME',exptime_a

        comb_a=(keyword_set(series))? $
            clean_cosmic_stack(c_a,nsig=nsig,readn=readn,out_img_cube=clean_c_a,n_img_clean=n_img_clean,bias=bias,difference=difference,filter_negative=filter_negative): $
            clean_cosmic_stack(c_a,nsig=nsig,readn=readn,bias=bias,difference=difference,filter_negative=filter_negative)

        outfile_all=(keyword_set(series))? strarr(n_inp+1)+outfile : outfile
        if(keyword_set(series)) then begin
            outfile_base=strmid(outfile_all[0],0,strpos(outfile_all[0],'_',/reverse_search))
            outfile_suff=strmid(outfile_all[0],strpos(outfile_all[0],'_',/reverse_search))
            outfile_all[1:*]=outfile_base+'_clean'+string(lindgen(n_inp),format='(i03)')+outfile_suff
        endif

        for i=0,n_elements(outfile_all)-1 do begin
            if(e eq 0) then writefits,outfile_all[i],0;,h_pri
            if keyword_set(series) and (i gt 0) then begin
                h_a=headfits(inpfilelist[i-1],ext=extnum[e])
                sxaddpar,h_a,'BZERO',0
                exp_cur=sxpar(h_a,'EXPTIME',count=cexp_cur)
                if(cexp_cur eq 0) then exp_cur=sxpar(h_a,'EXPOSURE',count=cexp_cur)
                if(cexp_cur eq 0) then exp_cur=1.0
                sxaddpar,h_a,'EXPTIME',exp_cur
                if(extnum[e] eq 0) then begin
                    for j=1,7 do sxdelpar,h_a,'NAXIS'+string(j,format='(i1)')
                    h_a[0]=hdrtmp[0] ;; defined above
                    sxaddpar,h_a,'EXTNAME','SPECTRUM01'
                endif
            endif
            sxdelpar,h_a,'SIMPLE'
            mwrfits,float((i eq 0)? rotate(comb_a,rotdir) : rotate(clean_c_a[*,*,i-1],rotdir)),outfile_all[i],h_a

            if(e eq n_elements(extnum)-1 and n_elements(xtraext) gt 0) then begin
                fits_open, inpfilelist[0], fcb
                n_ext=fcb.nextend
                fits_close, fcb
                for x=0,n_elements(xtraext)-1 do begin
                    if n_ext ge xtraext[x] then begin
                        tab_a=mrdfits(inpfilelist[0],xtraext[x],h_tab_a,/silent)
                        mwrfits,tab_a,outfile_all[i],h_tab_a
                    endif
                endfor
            endif
        endfor
    endfor

end
