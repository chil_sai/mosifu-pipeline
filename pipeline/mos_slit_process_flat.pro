function mos_slit_process_flat,flat,bkspace=bkspace,npoly=npoly,$
        nopixvar=nopixvar,pixvar=pixvar
    s_ord=size(flat)
    x_crd=dindgen(s_ord[1]) # (dblarr(s_ord[2])+1d)
    y_crd=(dblarr(s_ord[1])+1d) # dindgen(s_ord[2])

    if(n_elements(bkspace) ne 1) then bkspace=s_ord[1]/50. ;; s_ord[1]/20.
    if(n_elements(npoly) ne 1) then npoly=(4< (fix(s_ord[2]/35)) > 0)

    w_cnt=300
    norm_prof=median(flat[s_ord[1]/2-w_cnt:s_ord[1]/2+w_cnt,*],dim=1)
    norm_prof=norm_prof/max(norm_prof,/nan)
    norm_arr=congrid(transpose(norm_prof),s_ord[1],s_ord[2])

    g_fl=where(finite(flat) eq 1, cg_fl)
    if(cg_fl lt 1000) then return,flat
    if(n_elements(pixvar) ne n_elements(flat)) then pixvar=flat
    
    s_x = sort(x_crd[g_fl])
    xx = x_crd[g_fl[s_x]]
    yy = y_crd[g_fl[s_x]]
    ff = (flat/norm_arr)[g_fl[s_x]]

    sset = (npoly eq 0)? $
        bspline_iterfit(xx, ff, ivar=(dblarr(cg_fl)+1d), bkspace=bkspace) :$
        bspline_iterfit(xx, x2=yy, ff, ivar=(dblarr(cg_fl)+1d), npoly=npoly, bkspace=bkspace)
    flat_model = (npoly eq 0)? $
        reform(bspline_valu(reform(x_crd,s_ord[1]*s_ord[2]),sset),s_ord[1],s_ord[2]) :$
        reform(bspline_valu(reform(x_crd,s_ord[1]*s_ord[2]),sset,x2=reform(y_crd,s_ord[1]*s_ord[2])),s_ord[1],s_ord[2])
    flat_proc=flat_model*norm_arr
    smw=5.0
    if(~keyword_set(nopixvar)) then $
        for y=0,s_ord[2]-1 do flat_proc[*,y]+=(pixvar[*,y]-smooth(pixvar[*,y],smw,/nan))

    return, flat_proc
end
