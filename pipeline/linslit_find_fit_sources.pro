function linslit_find_fit_sources,image,error_inp,moffat=moffat,nsig=nsig,xmin=xmin,xmax=xmax,fitprof=fitprof,prof=prof,verbose=verbose

    s_im=size(image)
    if(s_im[0] ne 2) then message,'Only two-dimensional input is supported'
    if(n_elements(xmin) ne 1) then xmin=0L
    if(n_elements(xmax) ne 1) then xmax=s_im[2]-1L

    ;;; no error frame is used as of now
    ;s_er=size(error_inp) 
    ;error=(array_equal(s_im[0:2],s_er[0:2]) eq 1)? error_inp : sqrt(abs(image))

    resistant_mean,image[(0>xmin<(s_im[1]-1)):(0>xmax<(s_im[1]-1)),*],3.0,prof,dim=1

    std_prof=robust_sigma(prof)
    if(n_elements(nsig) ne 1) then nsig=3.0

    pixvec=findgen(s_im[2])
    find_peak,pixvec,prof,nsig*std_prof,pix_peak,x_peak,y_peak,bgr_peak,n_peak

    if(n_peak le 0) then begin
        message,/inf,'No sources found on the slit'
        return,-1
    endif

    prof_arr=dblarr(s_im[2],n_peak)
    prof_fit_arr=dblarr(s_im[2],n_peak)
    x_win=lonarr(2,n_peak)-1L
    s_flux=sort(y_peak)

    prof_fit=prof

    res_arr=replicate({$
        x:-1.0,$
        peak:-1.0,$
        window:[0,0],$
        profile:dblarr(s_im[2]),$
        norm_profile:0d,$
        bestfit:dblarr(s_im[2]),$
        norm_bestfit:0d,$
        fit_params:dblarr(4+keyword_set(moffat)),$
        prof_type:(keyword_set(moffat)?'moffat':'gaussian')},n_peak)

    for i=0,n_peak-1 do begin
        p_idx=s_flux[i]
        if(keyword_set(verbose)) then print,'Processing object #'+string(p_idx+1,format='(i)')+string(i+1,format='(i)')+'/'+string(n_peak,format='(i)')
        x_peak_cur=x_peak[p_idx]
        x_win[0,i]=(p_idx gt 0)? fix((x_peak[p_idx-1]+x_peak[p_idx])/2) : 0L
        x_win[1,i]=(p_idx lt n_peak-1L)? fix((x_peak[p_idx]+x_peak[p_idx+1])/2) : s_im[2]-1L
        prof_arr[x_win[0,i]:x_win[1,i],i]=prof[x_win[0,i]:x_win[1,i]]
        pixtmp=pixvec[x_win[0,i]:x_win[1,i]]
        g_prof=where(finite(prof[x_win[0,i]:x_win[1,i]]) eq 1, cg_prof)
        if(cg_prof lt 6) then begin
            x_win[*,i]=-1L
            continue
        endif
        prof_fit_seg=mpfitpeak(pixtmp,prof[x_win[0,i]:x_win[1,i]],par_prof,moffat=moffat,nterms=4+keyword_set(moffat),/nan,/positive)
;        prof_fit_arr[x_win[0,i]:x_win[1,i]]=prof_fit_seg
;        prof_fit[x_win[0,i]:x_win[1,i]]-=prof_fit_seg
        prof_fit_arr[*,i]=(keyword_set(moffat))? $
            par_prof[4]+par_prof[0]/(((pixvec-par_prof[1])/par_prof[2])^2+1.0)^par_prof[3] : $
            par_prof[3]+par_prof[0]*exp(-0.5d*((pixvec-par_prof[1])/par_prof[2])^2)
;            par_prof[4]+par_prof[5]*pixvec+par_prof[0]/(((pixvec-par_prof[1])/par_prof[2])^2+1.0)^par_prof[3] : $
;            par_prof[3]+par_prof[4]*pixvec+par_prof[0]*exp(-0.5d*((pixvec-par_prof[1])/par_prof[2])^2)
        prof_fit-=prof_fit_arr[*,i]
        res_arr[p_idx].x=x_peak[p_idx]
        res_arr[p_idx].peak=y_peak[p_idx]
        res_arr[p_idx].window=x_win[*,i]
        res_arr[p_idx].norm_profile=total(abs(prof_arr[*,i]),/nan)
        res_arr[p_idx].profile=prof_arr[*,i]/res_arr[p_idx].norm_profile
        res_arr[p_idx].norm_bestfit=total(abs(prof_fit_arr[*,i]-par_prof[n_elements(par_prof)-1]))
        res_arr[p_idx].bestfit=(prof_fit_arr[*,i]-par_prof[n_elements(par_prof)-1])/res_arr[p_idx].norm_bestfit
        res_arr[p_idx].fit_params=par_prof
    endfor

    fitprof=prof-prof_fit
    return,res_arr
end
