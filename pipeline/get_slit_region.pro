function get_slit_region,maskentry,Nx=Nx,Ny=Ny,ratio=ratio,pady=pady,dist_map=dist_map,slit_geom=slit_geom,slit_trace=slit_trace,dy0=dy0,y_scl=y_scl
    n_slits=n_elements(maskentry)

    res=dblarr(2,n_slits)
    if(n_elements(pady) ne 1) then pady=0
    if(n_elements(Ny) ne 1) then Ny=2048
    if(n_elements(Nx) ne 1) then Nx=2048
    if(n_elements(ratio) ne 1) then ratio=1.0
    slit_geom=replicate({y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
                         w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1},Ny)

    slit_trace=replicate({x_trace:dindgen(Nx),y_trace:dblarr(Nx)+!values.d_nan,y_edge:dblarr(Nx,2)+!values.d_nan,slit_h:!values.d_nan},n_slits)
    x_slits=double(maskentry.x)
    y_slits=double(maskentry.y)
    dx_slits=double(maskentry.width)
    dy_slits=double(maskentry.height)

    if(n_elements(dy0) ne 1) then dy0=0.0
    if(n_elements(y_scl) ne 1) then y_scl=1d

    if(n_elements(dist_map) eq 1) then begin
        if(tag_exist(dist_map,'mask_dy0')) then dy0=dist_map.mask_dy0
        if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl
    endif

    x_slits_pix=(x_slits)*y_scl+Nx/2.0
    y_slits_pix=Ny-1-((y_slits-maskentry[0].corners[1])*y_scl)+dy0

    if(n_elements(dist_map) eq 1) then begin
        dy=poly2d(x_slits_pix,y_slits_pix,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)
    endif else dy=dblarr(n_slits)
    dx_slits_pix=dx_slits*y_scl*ratio
    dy_slits_pix=dy_slits*y_scl*ratio

    for i=0,n_slits-1 do begin
;        res[0,i]=0 >  (fix(y_slits_pix[i]+dy[i]-dy_slits_pix[i]/2.0-pady)) < (Ny-1)
        res[0,i]=0 > (round(y_slits_pix[i]+dy[i]-dy_slits_pix[i]/2.0-pady)) < (Ny-1)
;        res[1,i]=0 >  (fix(y_slits_pix[i]+dy[i]+dy_slits_pix[i]/2.0-1+pady)) < (Ny-1)
        res[1,i]=0 > (round(y_slits_pix[i]+dy[i]+dy_slits_pix[i]/2.0-1+pady)) < (Ny-1)
        slit_trace[i].y_trace = (n_elements(dist_map) eq 1)? $
              poly2d(Nx-1d -dindgen(Nx),dindgen(Nx)*0+y_slits_pix[i],dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_slits_pix[i]+dy[i] : $
              dindgen(Nx)*0.0+y_slits_pix[i]
        slit_trace[i].slit_h=dy_slits_pix[i]
        slit_trace[i].y_edge[*,0] = (n_elements(dist_map) eq 1)? $
              poly2d(Nx-1d -dindgen(Nx),dindgen(Nx)*0+y_slits_pix[i]+dy_slits_pix[i]/2.0,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_slits_pix[i]+dy_slits_pix[i]/2.0+dy[i] : $
              dindgen(Nx)*0.0+y_slits_pix[i]+dy_slits_pix[i]/2.0
        slit_trace[i].y_edge[*,1] = (n_elements(dist_map) eq 1)? $
              poly2d(Nx-1d -dindgen(Nx),dindgen(Nx)*0+y_slits_pix[i]-dy_slits_pix[i]/2.0,dist_map.kx_2dfit,deg1=dist_map.deg1,deg2=dist_map.deg2,/irreg)+y_slits_pix[i]-dy_slits_pix[i]/2.0+dy[i] : $
              dindgen(Nx)*0.0+y_slits_pix[i]-dy_slits_pix[i]/2.0
        ;;;;;print,'Slit,dY=',i,res[1,i]+1-res[0,i]
        for y=((res[0,i]-max(abs(dy))) > 0),((res[1,i]+max(abs(dy[i]))) < (Ny-1)) do begin
            slit_geom[y].y_pix=y
            slit_geom[y].x_mask=maskentry[i].x
            slit_geom[y].y_mask=maskentry[i].y+(y_slits_pix[i]-y)/y_scl
            slit_geom[y].w_pix=maskentry[i].width
            slit_geom[y].h_pix=maskentry[i].height/2.0+(y_slits_pix[i]-y)/y_scl
            slit_geom[y].slit=maskentry[i].slit
        endfor
    endfor
    return,res
end


