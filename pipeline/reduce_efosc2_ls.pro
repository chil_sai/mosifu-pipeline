function headpar, h, par
	par_id = where(strpos(h, par) eq 0)
	val_id0 = strpos(h[par_id], "'")
	val_id1 = strpos(h[par_id], "'", /reverse_search)
	if ((val_id0 eq -1) and (val_id1 eq -1)) then begin
		val_id0 = strpos(h[par_id], "=")
    	val_id1 = strpos(h[par_id], "/", /reverse_search)
		val=strmid(h[par_id], val_id0+1, val_id1-val_id0-1)
	endif else val = strmid(h[par_id], val_id0+1, val_id1-val_id0-1)
	return, val[0]
end


pro reduce_efosc2_ls,rawfilename,bias=rawbias,flat=rawflat,arc=rawarc,tmpdir=tmpdir,$
    stdstar=rawstdstar,std_flat=std_rawflat,std_arc=std_rawarc,stddata=stddata,oh=oh,skipsteps=skipsteps,$
    n_stack=n_stack,debug=debug,skysubalgorithm=skysubalgorithm,ext=ext,abscal=abscal,disp_conf_str=disp_conf_str,std_disp_conf_str=std_disp_conf_str,$
	skylinecorr=skylinecorr,wl_line=wl_line,dist_map_flat=dist_map_flat,dist_map_std=dist_map_std

    if(n_elements(n_stack) eq 0) then n_stack=1
	if(n_elements(ext) eq 0) then ext=0
	if(n_elements(wl_line) eq 0) then wl_line=5577.34 ;A

	if (n_elements(std_rawflat) eq 0) then std_rawflat=rawflat
    if (n_elements(std_rawarc) eq 0) then std_rawarc=rawarc

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'pri_obj',skippri_obj)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_bias=n_elements(rawbias)
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
	n_std_flat=n_elements(std_rawflat)
	n_std_arc=n_elements(std_rawarc)
    n_stdstar=n_elements(rawstdstar)
	if (n_elements(disp_conf_str) eq 0) then $
		disp_conf_str={smy:7, y_ndeg:2, ndeg:5, fwhm:4}
    if (n_elements(std_disp_conf_str) eq 0) then $
        std_disp_conf_str={smy:7, y_ndeg:2, ndeg:5, fwhm:4}
	disp_conf_str=[disp_conf_str, std_disp_conf_str]

	file_mkdir, tmpdir+'/object/'
	file_mkdir, tmpdir+'/stdstar/'
	exp_calib=['object', 'stdstar']

    rotdir=3
    trimx1=16
    trimx2=0
	
    if(~keyword_set(skippri)) then begin
        if(n_elements(nsig_clip) ne 1) then nsig_clip=5.
        if(n_bias gt 0) then begin
            if(keyword_set(verbose)) then print,'Processing bias frames'
            bias_0=float(readfits(rawbias[0],h_bias,/silent,exten_no=ext))
            bias_cube=fltarr(sxpar(h_bias,'NAXIS1'),sxpar(h_bias,'NAXIS2'),n_bias)
            gain=float(headpar(h_bias, 'HIERARCH ESO DET OUT1 GAIN'))
            if(gain eq 0.0) then gain=1.0
            bias_cube[*,*,0]=bias_0*gain
            for i=1,n_bias-1 do bias_cube[*,*,i]=float(readfits(rawbias[i],/silent,exten_no=ext))*gain
            if(n_bias gt 1) then begin
                bias_cube = bias_cube[*,trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*]
                bias = median(bias_cube,dim=3) ;should be resistant_mean instead
            endif else bias=bias_cube[*,trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1]
            writefits,tmpdir+'bias_i.fits',bias,h_bias
            bias_cube=0.0
        endif else bias=0.0
        if(n_flat gt 0) then ccd_combine,rawflat,tmpdir+'/object/flat_dark.fits',nsig=10.0,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_arc gt 0) then ccd_combine,rawarc,tmpdir+'/object/arc_dark.fits',nsig=30.0,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_skyflat gt 0) then ccd_combine,rawskyflat,tmpdir+'skyfat_dark.fits',nsig=7.0,/norm,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_std_flat gt 0) then ccd_combine,std_rawflat,tmpdir+'/stdstar/flat_dark.fits',nsig=25.0,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_std_arc gt 0) then ccd_combine,std_rawarc,tmpdir+'/stdstar/arc_dark.fits',nsig=30.0,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_stdstar gt 0) then ccd_combine,rawstdstar,tmpdir+'/stdstar/stdstar_dark.fits',nsig=10.0,readn=readn,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir,/filter_negative
    endif

	if(~keyword_set(skippri_obj)) then begin
		bias = mrdfits(tmpdir+'bias_i.fits', 0, h_bias)
        if(n_obj gt 1) then begin
            print, 'n_obj'
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    ccd_combine,rawfilename[findgen(n_stack)+n_stack*i+1],tmpdir+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=15.0,readn=readn,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
                ccd_combine,tmpdir+'obj_dark_'+string(findgen(n_obj_total),format='(i2.2)')+'.fits',tmpdir+'obj_dark.fits',nsig=10.0,readn=readn,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
            endif else begin
                ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=5.0,series=series,n_img_clean=n_img_clean,readn=readn,bias=bias,trimy1=trimx1,trimx2=trimx2,rot=rotdir,ext=ext
            endelse
        endif else ccd_combine,rawfilename,tmpdir+'/object/obj_dark.fits',nsig=7.0,bias=bias,trimy1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
	endif

for k=0, n_elements(exp_calib)-1 do begin

	if (exp_calib[k] eq 'object') then begin
		if (keyword_set(dist_map_flat)) then begin
			print, 'aaaaaaaaaa'
            dist_map=distortion_map_generic(tmpdir+'/object/flat_dark.fits',extnum=1,/longslit,maskentry=mask)
		endif
		if (keyword_set(dist_map_std)) then begin
			print, 'bbbbbbbbbbbb'
	    	dist_map=distortion_map_generic(tmpdir+'/stdstar/stdstar_dark.fits',extnum=1,/longslit,maskentry=mask)
		endif
		if (~keyword_set(dist_map_flat) and ~keyword_set(dist_map_std)) then begin
			print, 'cccccccccccc'
            dist_map=distortion_map_generic(tmpdir+'/object/obj_dark.fits',extnum=1,/longslit,maskentry=mask)
		endif
	endif
    if (exp_calib[k] eq 'stdstar') then $
        dist_map=distortion_map_generic(tmpdir+'/stdstar/stdstar_dark.fits',extnum=1,/longslit,maskentry=mask)
	
    flat = mrdfits(tmpdir+exp_calib[k]+'/'+'/flat_dark.fits', 1, hff)
    nx_f = sxpar(hff, 'NAXIS1')
    ny_f = sxpar(hff, 'NAXIS2')

	mask={slit:0,ra:0d,dec:0d,x:0d,y:5d,$
        target:0ll,object:'',type:'TARGET',$
        wstart:0.0,wend:0.0,height:float(ny_f)-5.,width:1.0,offset:0.0,theta:0.0,$
        bbox:dblarr(8),$
        mask_id:-1l,mask_name:'',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
        mask_pa:0d,corners:[-0.5,-ny_f/2,0.5,ny_f/2]}

	if (exp_calib[k] eq 'object') then $
		exp_flat=['obj','arc','flat'] $
	else $
		exp_flat=['stdstar','arc','flat']
;    if(n_skyflat gt 0) then exp_flat=[exp_flat,'skyflat']
;    if(n_stdstar gt 0) then exp_flat=[exp_flat,'stdstar']
    flat_fielding_mosifu, exp_flat, wdir=tmpdir+exp_calib[k]+'/', dist_map=dist_map, norm_slit=-1, mask=mask, sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat, flat_thr=0.01,/slit_edg_mask

	h_arc = headfits(tmpdir+exp_calib[k]+'/'+'arc_dark.fits', exten=1)
	slit = strtrim(headpar(h_arc, 'HIERARCH ESO INS SLIT1 NAME'), 2)
	grism = strtrim(headpar(h_arc, 'HIERARCH ESO INS GRIS1 NAME'), 2)

    if (grism eq 'Gr#4' and slit eq 'slit#1.0') then begin
;		px = [34, 135, 305, 561, 793, 875, 903, 994]
;		wl = [4158.6, 4471.5, 5015.7, 5875.6, 6678.2, 6965.4, 7065.2, 7384.0]
		wl_ini = [4055.3586,       3.0264373,   0.00038077857,   1.8618830e-07,  -3.9664716e-10,   1.5328147e-13]
;		wl_ini = [4052.4871,       3.0297301,   0.00042842188,   9.0941521e-08,  -3.6387560e-10,   1.6714105e-13]; from disper.fits
		wl0_lin=4070.0
		dwl_lin=3.34
	endif		
	if (grism eq 'Gr#19' and slit eq 'slit#1.0_red') then begin
;		px = [97, 319, 409, 544, 788, 983]
;		wl = [4713.15, 4861.34, 4921.93, 5015.7, 5187.75, 5335.18]
		wl_ini=[4651.5278,      0.62130809,   0.00016532278,  -2.0694905e-07,   1.1750492e-10]
;		wl_ini=[4650.88000977242, 0.6327113826284365, 1.2189782701239296E-4, -1.9859192071902531E-7, 2.689996420699577E-10, -1.4293094037230997E-13]
		wl0_lin=4660.0
		dwl_lin=0.67
	endif
    lin_param={grism:grism, filter:'', wl0:wl0_lin, dwl:dwl_lin, npix:nx_f, wl_min:wl0_lin, wl_max:wl0_lin+nx_f*dwl_lin}

    if(~keyword_set(skipwl)) then begin
        if (keyword_set(oh) and exp_calib[k] eq 'object') then begin
                arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesOI_O2.tab','linesOH_R2k_1050nm_corr.tab','lines_sky_misc.tab'],label:['OI','OH','other'],weight:[1.,1.,1.]}
        endif else begin
            arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesHe_NIST.tab', 'linesAr_NIST.tab', 'linesArII_EFOSC.tab'],$
                weight:[1.0, 1.0, 0.1],label:['He','Ar', 'ArII']}
        endelse

		if (exp_calib[k] eq 'object') then $
			exp_disp = 'obj_ff.fits' $
		else $
			exp_disp = 'arc_ff.fits'
        status=crea_disper_generic_slit(tmpdir+exp_calib[k]+'/'+(keyword_set(oh)? exp_disp : 'arc_ff.fits'),0,$
            wl_ini,disp_conf_str[k].fwhm,disp_conf_str[k].ndeg,arc_list_config=arc_list_config,$
            smy=disp_conf_str[k].smy,ndegy=disp_conf_str[k].y_ndeg,dist_map=dist_map,use_oh=oh,debug=debug,plot=plot,fast=1,disper_table=disper_table,/fullslit,/high_curvature)

        writefits,tmpdir+exp_calib[k]+'/'+'disper_table.fits',0
        mwrfits,disper_table,tmpdir+exp_calib[k]+'/'+'disper_table.fits'
    endif else begin
        disper_table=mrdfits(tmpdir+exp_calib[k]+'/'+'disper_table.fits',1,/silent)
    endelse


    if (exp_calib[k] eq 'object') then $
        obs_type_arr=['obj','arc','flat'] $
    else $
        obs_type_arr=['stdstar','arc']
;	obs_type_arr=['obj','arc']
;    if(n_skyflat gt 0) then obs_type_arr=[obs_type_arr,'skyflat']
;    if(n_stdstar gt 0) then obs_type_arr=[obs_type_arr,'stdstar']
	for i=0, n_elements(obs_type_arr)-1 do begin
        hh_pri=headfits(tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_ff.fits')
        im=mrdfits(tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_ff.fits',1,hh)
		im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param)
        writefits,tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_lin.fits',0,hh_pri
        mwrfits,im_lin,tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_lin.fits',hh_out

		if (keyword_set(skylinecorr) and obs_type_arr[i] eq 'obj') then begin 
			line_par=mos_measure_emline('obj',wl_line,win_line=7*lin_param.dwl,wdir=tmpdir+'/object/',mask=mask,slit_poly=-1,slit_medw=20,/slit_bspl,slit_nbkpts=20,nowlshift=oh)
;			skyline_flux_data_arr={slits:line_par}
			print, 'MEAN WL OFFSET: ', median(line_par.slits[100:900].wl-line_par.slits[100:900].wl_line), ', measured at ', wl_line, 'A'
	        im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param,skyline_flux_data_arr=line_par)
			sxaddpar, hh_out, median(line_par.slits[100:900].wl-line_par.slits[100:900].wl_line), 'MED_WL_OFF'
	        writefits,tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_lin.fits',0,hh_pri
    	    mwrfits,im_lin,tmpdir+exp_calib[k]+'/'+obs_type_arr[i]+'_lin.fits',hh_out
		endif
	endfor

    if(skysubalgorithm eq 2) then begin ;; simple sky subtraction using skyreg
        ;; simple sky subtraction
		if (exp_calib[k] eq 'object') then begin
			dirin = 'object/obj_lin.fits'
			dirout = 'object/obj-sky_lin.fits'
		endif else begin
            dirin = 'stdstar/stdstar_lin.fits'
            dirout = 'stdstar/stdstar-sky_lin.fits'
		endelse
        hh_pri=headfits(tmpdir+dirin)
        obj_lin=mrdfits(tmpdir+dirin,1,hlin)
        nx_lin=sxpar(hlin,'NAXIS1')
        parse_spechdr,hlin,wl=wl
        if(n_elements(skyreg) eq 0) then begin
            p_obj=median(obj_lin[Nx_lin*0.4:Nx_lin*0.6,*],dim=1)
            p_zero=where(p_obj eq 0,cp_zero)
            if(cp_zero gt 0) then p_obj[p_zero]=!values.f_nan
            outer_p_obj=median([p_obj[0:150],p_obj[970:*]])
            min_p_obj=min(p_obj,/nan)
            nbin_p=11
            h_prof=histogram(p_obj,min=min_p_obj,max=2*outer_p_obj-min_p_obj,nbin=nbin_p)
            p_fit=mpfitpeak(min_p_obj+dindgen(nbin_p)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.0),h_prof,g_coeff)

            skyreg=where(p_obj gt min_p_obj and $
                          p_obj le min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.), csky)
        endif
        resistant_mean,obj_lin[*,skyreg],3.0,skyvec,dim=2
        writefits,tmpdir+dirout,0,hh_pri
        mwrfits,obj_lin-rebin(skyvec,nx_lin,sxpar(hlin,'NAXIS2')),tmpdir+dirout,hlin
    endif
endfor

	if (n_stdstar gt 0 and n_elements(stddata) eq 0) then message, 'No flux data for standard star. Aborting flux calibraion.'

    flat_norm=mrdfits(tmpdir+'/stdstar/flat_norm.fits',1,hh,/silent)
    flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param)
    flatflux=median(flat_lin[*,sxpar(hh,'NAXIS2')/2-20:sxpar(hh,'NAXIS2')/2+20],dim=2)
    gfl=where(finite(flatflux) eq 1,cgfl,compl=bfl,ncompl=cbfl)
    if(cbfl gt 0 and cgfl gt 10) then flatflux=interpol(flatflux[gfl],float(gfl),findgen(cbfl+cgfl))
    flatflux[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]=(poly_smooth(flatflux,(cbfl+cgfl)/20.0,deg=2))[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]

    hh_pri=headfits(tmpdir+'object/obj_lin.fits')
    obj_lin=mrdfits(tmpdir+'object/obj_lin.fits',1,hlin,/silent)
    obj_flat_norm=mrdfits(tmpdir+'/object/flat_norm.fits',1,hh,/silent)
    obj_flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param)
    err_obj_lin=sqrt((sxpar(hlin,'RDNOISE')+obj_lin*obj_flat_lin)>0)/obj_flat_lin
    writefits,tmpdir+'object/err_obj_lin.fits',0,hh_pri
    mwrfits,err_obj_lin,tmpdir+'object/err_obj_lin.fits',hlin

	stdstar_lin=mrdfits(tmpdir+'stdstar/stdstar_lin.fits',1,hlin_s,/silent)
    err_stdstar_lin=sqrt((sxpar(hlin_s,'RDNOISE')+stdstar_lin*obj_flat_lin)>0)/obj_flat_lin
    writefits,tmpdir+'stdstar/err_stdstar_lin.fits',0
    mwrfits,err_obj_lin,tmpdir+'stdstar/err_sdstar_lin.fits',hlin_s

	if(n_stdstar gt 0 and n_elements(stddata) eq 1) then begin
        print,'Computing a throughput curve using a standard star spectrum'
        alt_start_s = float(headpar(hlin_s, 'HIERARCH ESO TEL ALT'))
        alt_start = float(headpar(hlin, 'HIERARCH ESO TEL ALT'))
		fluxcal_data=mos_measure_throughput(tmpdir+'stdstar/stdstar_lin.fits',stddata,$
            mirr=!pi*((358.0/2)^2-(87.5/2)^2),/hdrext,n_wl_seg=5,$
            flatflux=flatflux,expcorr=1.0/n_stdstar,/lintails,el=alt_start,plot=1)

        if(keyword_set(abscal)) then begin
            parse_spechdr,hlin,wl=wllin
            ext_vec=calc_extinction(wllin,90.0-alt_start)
            ext_vec_s=calc_extinction(wllin,90.0-alt_start_s)

            sxaddpar,hlin,'BUNIT','1.0E-17 erg/cm^2/s/A'
			exptime = sxpar(hlin,'exptime')
            obj_lin_abs=obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/exptime
            writefits,tmpdir+'object/obj_lin_abs.fits',0
            mwrfits,obj_lin_abs,tmpdir+'object/obj_lin_abs.fits',hlin

            err_obj_abs=err_obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/exptime
            writefits,tmpdir+'object/err_obj_abs.fits',0
            mwrfits,err_obj_abs,tmpdir+'object/err_obj_abs.fits',hlin

            if(file_test(tmpdir+'object/obj-sky_lin.fits')) then begin
                obj_sky=mrdfits(tmpdir+'object/obj-sky_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
	            exptime = sxpar(hsky,'exptime')
                obj_abs=obj_sky*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/exptime
                writefits,tmpdir+'object/obj_abs.fits',0
                mwrfits,obj_abs,tmpdir+'object/obj_abs.fits',hsky
            endif

            if(file_test(tmpdir+'stdstar/stdstar-sky_lin.fits')) then begin
                stdstar_lin=mrdfits(tmpdir+'stdstar/stdstar-sky_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
                exptime = sxpar(hsky,'exptime')
                stdstar_abs=stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_s,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/exptime
                err_stdstar_abs=err_stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_s,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/exptime

                writefits,tmpdir+'stdstar/stdstar_abs.fits',0
                mwrfits,stdstar_abs,tmpdir+'stdstar/stdstar_abs.fits',hsky

                writefits,tmpdir+'stdstar/err_stdstar_abs.fits',0
                mwrfits,err_stdstar_abs,tmpdir+'stdstar/err_stdstar_abs.fits',hsky
			endif

            if(file_test(tmpdir+'object/arc_lin.fits')) then begin
                arc_lin=mrdfits(tmpdir+'stdstar/arc_lin.fits',1,harc,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/A'
                arc_abs=arc_lin*rebin(fluxcal_data.c_flux_erg,sxpar(harc,'NAXIS1'),sxpar(harc,'NAXIS2'))*1e17
                writefits,tmpdir+'stdstar/arc_abs.fits',0
                mwrfits,arc_abs,tmpdir+'stdstar/arc_abs.fits',hsky
            endif
        endif
    endif
end
