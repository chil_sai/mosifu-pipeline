pro reduce_cmotds_ls,rawfilename,bias=rawbias,dark=rawdark,flat=rawflat,arc=rawarc,skyflat=rawskyflat,tmpdir=tmpdir,$
    dist_map_frame=dist_map_frame,stdstar=stdstar,stddata=stddata,telluric=telluric,Vmag_tell=Vmag_tell,crhfill=crhfill,$
    cmotds_setup=cmotds_setup,$ ; CMO TDS Setup: 0=red (default), 1=blue grating, 2=blue grism -- will attempt automatic determination if not specified
    nsig_clip=nsig_clip,$ ; N(sigma) for sigma clipping when cleaning cosmic ray hits
    sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat,split1d=split1d,noadjust=noadjust, $
    ndeg_wl=ndeg_wl,skylinecorr=skylinecorr,wl_skyline=wl_skyline,lsfcorr=lsfcorr,barycorr=barycorr,$
    skysubtarget=skysubtarget,bright=bright,extapw=extapw,skysubalgorithm=skysubalgorithm,oh=oh,$
    extract=extract,extr_optimal=extr_optimal,extr_detect=extr_detect,extr_estimate=extr_estimate,$
    abscal=abscal,series=series,n_stack=n_stack,n_img_clean=n_img_clean,n_apwmax=n_apwmax,$
    mask_slit_individual=mask_slit_individual,skipsteps=skipsteps,smooth_overscan=smooth_overscan,debug=debug, plot=plot, dithpos=dithpos, verbose=verbose

    if(n_elements(tmpdir) ne 1) then tmpdir='../test_data/test_dir/'
    if(NOT file_test(tmpdir,/directory)) then file_mkdir, tmpdir
    if(n_elements(extapw) ne 1) then extapw=2
    if(n_elements(skysubalgorithm) ne 1) then skysubalgorithm=1 ;; non-local Kelson
    if(n_elements(wl_skyline) lt 1) then skylinecorr=0
    if(n_elements(skipsteps) eq 0) then skipsteps=['none']
    if(n_elements(n_stack) eq 0) then n_stack=1
    if(keyword_set(debug)) then verbose=1

    trimx1=0 ; 8
    trimx2=0 ; 15

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_bias=n_elements(rawbias)
    n_dark=n_elements(rawdark)
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
    n_dist_map=n_elements(dist_map_frame)
    n_stdstar=n_elements(stdstar)

    h_rawobj=headfits(rawfilename[0])
    if(n_elements(cmotds_setup) ne 1) then begin
		if strcmp(rawfilename[0], 'R', 1) then begin 
			cmotds_setup = 0
		endif else begin
       		disp_hdr=strcompress(sxpar(h_rawobj,'DISPELEM',count=ndisp),/remove_all)
			disp = strcompress(sxpar(h_rawobj, 'DISP'), /remove_all)
        	if(ndisp eq 0) then disp_hdr='VPH650-1200lpmm'
        	if(disp_hdr eq 'NA') then disp_hdr=strcompress(sxpar(h_rawobj,'BDISP'),/remove_all)
        	cmotds_setup = (disp eq 'B')? 1 : (disp eq 'G' ? 2 : (disp_hdr eq 'VPH656-1200lpmm' or disp_hdr eq 'VPH650-1200lpmm')? 0 : (disp_hdr eq 'B' ? 1 : 2))
		endelse
    endif

    readn=(cmotds_setup eq 0)? 3.0 : 3.0

    if(~keyword_set(skippri)) then begin
        rotdir=(cmotds_setup eq 0)? 5 : 0
        if(n_elements(nsig_clip) ne 1) then nsig_clip=((cmotds_setup eq 0)? 5. : 6.)
        if(n_bias gt 0) then begin
            if(keyword_set(verbose)) then print,'Processing bias frames'
            bias_0=float(readfits(rawbias[0],h_bias,/silent))
            
            bias_cube=fltarr(sxpar(h_bias,'NAXIS1'),sxpar(h_bias,'NAXIS2'),n_bias)
            gain=1.0 ;;;sxpar(h_bias,'GAIN')
            if(gain eq 0.0) then gain=1.0
            bias_cube[*,*,0]=bias_0*gain
            for i=1,n_bias-1 do bias_cube[*,*,i]=float(readfits(rawbias[i],/silent))*gain
            if(n_bias gt 1) then begin
                resistant_mean,bias_cube,3.0,bias,dim=3
                bias=bias[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*] 
            endif else bias=bias_cube[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*]
            ;bias=rotate(bias,rotdir)
            writefits,tmpdir+'bias_i.fits',bias,h_bias
            bias_cube=0.0
        endif else bias=0.0
        readn_bias=(n_elements(bias) eq 1)? 0.0 : robust_sigma(bias)
        if(n_dark gt 0) then begin
            gain=1.0 ;;;sxpar(h_dark,'GAIN')
            for i=0,n_dark-1 do begin
                if(keyword_set(verbose)) then print,'Processing dark frame #'+string(i+1,format='(i)')+' / '+string(n_dark,format='(i)')
                dark_cur=float(readfits(rawdark[i],h_dark,/silent))

                if(i eq 0) then dark_cube=fltarr(sxpar(h_dark,'NAXIS1'),sxpar(h_dark,'NAXIS2'),n_dark)
                dark_cur=(dark_cur-bias/gain)*gain
                med_dark=median(dark_cur)
                med_dark_15=smooth(median(dark_cur,7),25,/nan,/edge_trunc)
                g_dark=where(abs(dark_cur-med_dark) gt 5.0*readn_bias, cg_dark, ncompl=cb_dark, compl=b_dark)
                if(cb_dark gt 0) then dark_cur[b_dark]=med_dark_15[b_dark]
                dark_cube[*,*,i]=dark_cur/sxpar(h_dark,'EXPOSURE')
            endfor
            if(n_dark gt 1) then begin
                resistant_mean,dark_cube,3.0,dark_expnorm,dim=3
                dark_expnorm=dark_expnorm[trimx1:sxpar(h_dark,'NAXIS1')-trimx2-1,*]
            endif else dark_expnorm=dark_cube[trimx1:sxpar(h_dark,'NAXIS1')-trimx2-1,*] ;;; should do resistant_mean instead but takes too long
            ;dark_expnorm=rotate(dark_expnorm,rotdir)
            sxaddpar,h_dark,'EXPOSURE',1.0,' effective exposure time [sec]'
            sxaddpar,h_dark,'EXPOSURE',1.0,' effective exposure time [sec]'
            writefits,tmpdir+'dark_expnorm.fits',dark_expnorm,h_dark
            dark_cube=0.0
        endif else dark_expnorm=0.0
        if(n_obj gt 1) then begin
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    ccd_combine,rawfilename[findgen(n_stack)+n_stack*i+1],tmpdir+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=15.0,readn=readn,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir,filter_negative=(cmotds_setup eq 0)*0
                ccd_combine,tmpdir+'obj_dark_'+string(findgen(n_obj_total),format='(i2.2)')+'.fits',tmpdir+'obj_dark.fits',nsig=5.0,readn=readn,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir,filter_negative=(cmotds_setup eq 0)*0
            endif else begin
                ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=nsig_clip,series=series,n_img_clean=n_img_clean,readn=readn,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir,filter_negative=(cmotds_setup eq 0)*0
            endelse
        endif else ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=5.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir
        if(n_flat gt 0) then ccd_combine,rawflat,tmpdir+'flat_dark.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir,filter_negative=(cmotds_setup eq 0)*0
        if(n_arc gt 0) then ccd_combine,rawarc,tmpdir+'arc_dark.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir
        if(n_skyflat gt 0) then ccd_combine,rawskyflat,tmpdir+'skyflat_dark.fits',nsig=7.0,/norm,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir
        if(n_dist_map gt 0) then ccd_combine,dist_map_frame,tmpdir+'distortion_frame.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir
        if(n_stdstar gt 0) then ccd_combine,stdstar,tmpdir+'stdstar_dark.fits',nsig=10.0,readn=readn,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,rot=rotdir,filter_negative=(cmotds_setup eq 0)*0
    endif

    h_a=headfits(tmpdir+'obj_dark.fits',ext=1)

    mjd_avg=sxpar(h_a,'MJD-AVG')
    new_lamp_grism = (mjd_avg gt 59140.0)
    new_red_wl_ini = (mjd_avg gt 59789.0)

;;    grating=strcompress(sxpar(h_a,'GRATING'),/remove_all)
    grating=(cmotds_setup eq 2)? 'B_Grism' : (cmotds_setup eq 1? 'B_grating' : 'R_grating')
    if not keyword_set(dithpos) then dithpos=strcompress(sxpar(h_a,'IELOFF'),/remove_all) else dithpos=strcompress(string(dithpos),/remove);get telescope instrument offset from header in arcsec

    logfile=tmpdir+'test_logfile.txt'
    write_tmp_logfile,logfile,tmpdir,tmpdir,instrument='CMO-TDS',$
        slit='1.0',grating=grating,filter='',dithpos=dithpos,$
        bright=bright,extapw=extapw,rawext='.gz'

    ndeg=(n_elements(ndeg_wl) eq 1)? ndeg_wl : ((grating eq 'R_grating')? 4 : 4)

    h_a=headfits(tmpdir+'obj_dark.fits',ext=1)
    nx=sxpar(h_a,'NAXIS1')
    ny=sxpar(h_a,'NAXIS2')

    sky_slit_length_a=ny
    y_ndeg=-1

    ystretch=(cmotds_setup eq 2)? 1.0 : (cmotds_setup eq 1? 0.35/0.354 : 0.37/0.354)
    yshift=(cmotds_setup eq 2)? 0.0 : (cmotds_setup eq 1? 0.0 : 9.5)
    if(n_dist_map gt 0) then dist_map=distortion_map_generic(tmpdir+'distortion_frame.fits',extnum=1,cuty1=3,cuty2=15,/oned,yshift=yshift,ystretch=ystretch)
    ;;; blue grating wl ini (NGC3516 2020/02/22):
    ;;; [[3727.*(1.0+2600./3e5),4046.57,4358.335,4861.*(1.0+2600./3e5),5460.75,5577.34], [337.,569.,825.,1284.,1763.,1864.]] -- poly_fit rms 0.2A
    ;;; green grism wl ini (1107p1347 2020/03/27):
    ;;; [[4355.48,4386.54,4577.21,4739.00,4832.08,5005.16,5330.78,5400.56],[46.,101.,445.,738.,908.,1223.,1809,1934.]] -- poly_fit rms 0.2A
    ;;; red grating wl ini (NGC3516 2020/02/22):
    ;;; [[5889.953,6300.304,6363.776,6562.78*(1.0+2600./3e5),6863.951,7276.401,7340.881],[1811.,1355.,1283.,992.,708.,221.,143.]] -- poly_fit rms 0.2A
    wl_ini=(cmotds_setup eq 2)? (new_lamp_grism ? [4301.80d,0.54623d,1.125977d-05,-9.519489d-09,2.749414d-12] : [4329.83d,0.560951,-1.27749d-05,4.67517d-09]) : $
          ((cmotds_setup eq 1)? [3330.19d,1.29499d,-6.86938d-05,1.11175d-08] : $
                                (new_lamp_grism ? [5672.39d,0.92343d,-2.499245d-05,-9.006456d-10,4.921919d-13] : [5672.08d,0.92361d,-2.48847d-05,-1.10118d-09,5.6730157d-13]))

    if (cmotds_setup eq 0 and new_red_wl_ini) then wl_ini = [ 5.59270898e+03, 1.17689916e+00, -6.65013688e-04,  7.31271668e-07, -3.82972719e-10,  7.46261157e-14]

    exp_flat=['obj','arc','flat']
    if(n_skyflat gt 0) then exp_flat=[exp_flat,'skyflat']
    if(n_stdstar gt 0) then exp_flat=[exp_flat,'stdstar']

    if(keyword_set(series)) then exp_flat=[exp_flat,'obj_clean'+string(lindgen(n_obj),format='(i03)')]

    mask={slit:0,ra:0d,dec:0d,x:0d,y:0d,$
            target:0ll,object:sxpar(h_a,'OBJECT'),type:'TARGET',$
            wstart:0.0,wend:0.0,height:float(Ny),width:1.0,offset:0.0,theta:0.0,$
            bbox:dblarr(8),$
            mask_id:-1l,mask_name:'long_1.0arcsec',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
            mask_pa:0d,corners:[-0.5,-Ny/2,0.5,Ny/2]}
    if(cmotds_setup ge 1) then mask.height=float(Ny)-20

    illum_corr_str=(cmotds_setup eq 2)? {xpix:850,illum_corr_coeff:[1.0567099d,-0.00033793756d,9.4694962d-07,-2.7120715d-09,2.8070838d-12]} : $
          ((cmotds_setup eq 1)? {xpix:850, illum_corr_coeff:[1.0567099d,-0.00033793756d,9.4694962d-07,-2.7120715d-09,2.8070838d-12]} : $ ;; blue
                                {xpix:720, illum_corr_coeff:[1.1293962d,-0.0016867546d,8.4553815d-06,-1.8608247d-08,1.4114827d-11]}) ;; red
;;                                {xpix:1085, illum_corr_coeff:[1.0441939d,-0.00056648441d,4.3238406d-06,-1.7856778d-08,3.2776352d-11,-2.2189190d-14]}) ;; red

    flat_fielding_mosifu,exp_flat,wdir=tmpdir,dist_map=dist_map,norm_slit=-1,$
        sub_sc_sci=sub_sc_sci,sub_sc_flat=sub_sc_flat,flat_thr=0.01,mask=mask,fringing=(cmotds_setup eq 0),illum_corr_str=illum_corr_str

    arc_list_config=(new_lamp_grism)? $
        {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesNe_NIST.tab','linesNeII_NIST.tab', 'linesPb_NIST.tab', 'linesPbII_NIST.tab', 'linesCd_NIST.tab'],$
            weight:[1.0,1.0, 1.0, 1.0, 1.0],label:['NeI','NeII', 'Pb', 'PbII', 'Cd']}: $
        {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesNe_NIST.tab','linesKr_NIST.tab','linesKrII_NIST.tab','linesPb_NIST.tab','linesNa_NIST.tab'],$
            weight:[1.0,1.0,0.1,1.0,1.0],label:['Ne','Kr','KrII','Pb','Na']}

    if(~keyword_set(skipwl)) then begin
        status=crea_disper_generic_slit(tmpdir+'arc_dark.fits',0,$
            wl_ini,3.0+1,ndeg,arc_list_config=arc_list_config,$
            smy=3,ndegy=y_ndeg,dist_map=dist_map,cd2d=cd2d,use_oh=oh,debug=debug,plot=plot,fast=1,disper_table=disper_table)

        writefits,tmpdir+'disper_table.fits',cd2d
        mwrfits,disper_table,tmpdir+'disper_table.fits'
    endif else begin
        cd2d=readfits(tmpdir+'disper_table.fits')
        s_cd2d=size(cd2d)
        ndeg=s_cd2d[1]
        y_ndeg=s_cd2d[2]
        disper_table=mrdfits(tmpdir+'disper_table.fits',1,/silent)
    endelse

    obs_type_arr=['obj','arc']
    if(keyword_set(series)) then obs_type_arr=[obs_type_arr,'obj_clean'+string(lindgen(n_obj),format='(i03)')]
    if(n_skyflat gt 0) then obs_type_arr=[obs_type_arr,'skyflat']
    if(n_stdstar gt 0) then obs_type_arr=[obs_type_arr,'stdstar']
    for i=0,n_elements(obs_type_arr)-1 do begin
        im=mrdfits(tmpdir+obs_type_arr[i]+'_ff.fits',1,hh)
        if(keyword_set(crhfill)) then begin
           im_m3=median(im,3)
           im_m5=median(im,5)
           b_m3=where(finite(im) ne 1, cb_m3)
           if(cb_m3 gt 0) then im[b_m3]=im_m3[b_m3]
           b_m5=where(finite(im) ne 1, cb_m5)
           if(cb_m5 gt 0) then im[b_m5]=im_m5[b_m5]
        endif
        im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map)
        im_lin[*,490:*]=!values.f_nan
        writefits,tmpdir+obs_type_arr[i]+'_lin.fits',0
        mwrfits,float(im_lin),tmpdir+obs_type_arr[i]+'_lin.fits',hh_out
    endfor

    if(skysubalgorithm gt 0) then begin
        for i=0,n_elements(obs_type_arr)-1 do begin
            ;; simple sky subtraction
            obj_lin=mrdfits(tmpdir+obs_type_arr[i]+'_lin.fits',1,hlin)
            nx_lin=sxpar(hlin,'NAXIS1')
            parse_spechdr,hlin,wl=wl
            p_obj=median(obj_lin[Nx_lin*0.4:Nx_lin*0.6,*],dim=1)
            p_zero=where(p_obj eq 0,cp_zero)
            if(cp_zero gt 0) then p_obj[p_zero]=!values.f_nan
            p_obj[0:10]=!values.f_nan
            p_obj[490:*]=!values.f_nan
            outer_p_obj=median([p_obj[0:100],p_obj[400:*]])
            min_p_obj=min(p_obj,/nan)
            nbin_p=11
            h_prof=histogram(p_obj,min=min_p_obj,max=2*outer_p_obj-min_p_obj,nbin=nbin_p)
            p_fit=mpfitpeak(min_p_obj+dindgen(nbin_p)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.0),h_prof,g_coeff)
    
;    sky_idx=where(p_obj gt min_p_obj and $
;                  p_obj le ((min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.)) < outer_p_obj), csky)
            sky_idx=where(p_obj gt min_p_obj and $
                          p_obj le min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.), csky)
            resistant_mean,obj_lin[*,sky_idx],3.0,skyvec,dim=2
            writefits,tmpdir+obs_type_arr[i]+'-sky_lin.fits',0
            mwrfits,float(obj_lin-rebin(skyvec,nx_lin,sxpar(hlin,'NAXIS2'))),tmpdir+obs_type_arr[i]+'-sky_lin.fits',hlin
        endfor
    endif

    if(n_stdstar gt 0 and ((n_elements(stddata) eq 1) or keyword_set(telluric))) then begin
        mirror_area=!pi*((250.0/2)^2-(110.0/2)^2)
        for i=0,n_elements(obs_type_arr)-1 do begin
            n_exp_obj=(keyword_set(series) and i ge n_elements(obs_type_arr)-1-n_obj)? 1: n_obj
            if(obs_type_arr[i] eq 'stdstar') then n_exp_obj=n_stdstar
            mos_abscal,tmpdir,obs_type_arr[i],fluxcal_data=fluxcal_data,n_exp=n_exp_obj,$
                disper_table=disper_table,dist_map=dist_map,$
                mirror_area=mirror_area,stddata=stddata,telluric=telluric,$
                blue_thr=3600.0,Vmag_tell=Vmag_tell,n_star_exp=n_stdstar,$
                verbose=verbose
            if(file_test(tmpdir+obs_type_arr[i]+'-sky_lin.fits')) then $
                mos_abscal,tmpdir,obs_type_arr[i]+'-sky',fluxcal_data=fluxcal_data,$
                    disper_table=disper_table,dist_map=dist_map,$
                    noskytype=obs_type_arr[i],n_exp=n_exp_obj,$
                    mirror_area=mirror_area,stddata=stddata,telluric=telluric,$
                    blue_thr=3600.0,Vmag_tell=Vmag_tell,n_star_exp=n_stdstar,$
                    verbose=verbose
        endfor
        if(n_skyflat gt 0) then $
            mos_abscal,tmpdir,'skyflat',fluxcal_data=fluxcal_data,n_exp=1,$
                disper_table=disper_table,dist_map=dist_map,$
                mirror_area=mirror_area,stddata=stddata,telluric=telluric,$
                blue_thr=3600.0,Vmag_tell=Vmag_tell,n_star_exp=n_stdstar,$
                verbose=verbose
    endif
end
