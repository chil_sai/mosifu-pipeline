pro mos_abscal,tmpdir,obstype,fluxcal_data=fluxcal_data,$
    noskytype=noskytype,n_exp=n_exp,$
    disper_table=disper_table,dist_map=dist_map,l_par=l_par,$
    mirror_area=mirror_area,stddata=stddata,telluric=telluric,$
    blue_thr=blue_thr,Vmag_tell=Vmag_tell,n_star_exp=n_star_exp,$
    recompute_throughput=recompute_throughput,verbose=verbose

    if(n_elements(n_exp) ne 1) then n_exp=1
    if(n_elements(n_star_exp) ne 1) then n_star_exp=1
    if(n_elements(noskytype) ne 1) then noskytype=obstype

    flat_norm=mrdfits(tmpdir+'flat_norm.fits',1,hh,/silent)
    flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,lin_param=l_par)
    flatflux=median(flat_lin[*,sxpar(hh,'NAXIS2')/2-20:sxpar(hh,'NAXIS2')/2+20],dim=2)

    obj_lin=mrdfits(tmpdir+obstype+'_lin.fits',1,hlin,/silent)
    obj_lin_nosky=(obstype eq noskytype)? obj_lin : mrdfits(tmpdir+noskytype+'_lin.fits',1,/silent)
    err_obj_lin=sqrt((sxpar(hlin,'RDNOISE')+obj_lin_nosky*flat_lin)>0)/flat_lin

    fileout=tmpdir+obstype+'_abs.fits'
    errfileout=tmpdir+'err_'+obstype+'_abs.fits'

    if(keyword_set(verbose)) then message,/inf,'Computing a throughput curve using a standard star spectrum'
    s_fcd=size(fluxcal_data)
    if(not (s_fcd[0] eq 1 and s_fcd[2] eq 8 and ~keyword_set(recompute_throughput))) then $
        fluxcal_data=mos_measure_throughput(tmpdir+'stdstar_lin.fits',stddata,$
            mirror_area=mirror_area,/hdrext,flatflux=flatflux,blue_thr=blue_thr,$
            expcorr=1.0/n_star_exp,telluric=telluric,Vmag_tell=Vmag_tell)

    parse_spechdr,hlin,wl=wllin
    el=float(sxpar(hlin,'MOUNT_EL',count=n_el))
    if(n_el eq 0) then el=90.0
    ext_vec=calc_extinction(wllin,90.0-el)
    sxaddpar,hlin,'BUNIT','1.0E-17 erg/cm^2/s/A'
    obj_lin_abs=obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/sxpar(hlin,'EXPTIME')*n_exp
    writefits,fileout,0
    mwrfits,float(obj_lin_abs),fileout,hlin
    fxbhmake,hdrfluxcal,n_elements(fluxcal_data),'FLUXCAL'
    fluxcal_corr=fluxcal_data
    fluxcal_corr.throughput*=flatflux
    fluxcal_corr.c_flux_erg/=flatflux
    fluxcal_corr.c_flux_mjy/=flatflux
    mwrfits,fluxcal_corr,fileout,hdrfluxcal
    hflat=hlin
    hflat[0]='XTENSION=''IMAGE'''
    sxaddpar,hflat,'EXTNAME','FLATPROF',after='XTENSION'
    sxdelpar,hflat,'NAXIS2'
    mwrfits,float(flatflux),fileout,hflat

    err_obj_abs=err_obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17/sxpar(hlin,'EXPTIME')*n_exp
    writefits,errfileout,0
    mwrfits,float(err_obj_abs),errfileout,hlin

end
