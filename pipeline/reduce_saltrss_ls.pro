pro reduce_saltrss_ls,rawfilename,bias=rawbias,flat=rawflat,arc=rawarc,skyflat=rawskyflat,tmpdir=tmpdir,$
    dist_map_frame=dist_map_frame,stdstar=stdstar,stddata=stddata,crhfill=crhfill,dy_ext=dy_ext,$
    sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat,split1d=split1d,noadjust=noadjust,oh=oh,$
    ndeg_wl=ndeg_wl,hgar=hgar,skylinecorr=skylinecorr,wl_skyline=wl_skyline,skyline_group=skyline_group,lsfcorr=lsfcorr,barycorr=barycorr,$
    skysubtarget=skysubtarget,bright=bright,extapw=extapw,skysubalgorithm=skysubalgorithm,skyreg=skyreg,subskydimension=subskydimension,skydegy=skydegy,$
    extract=extract,extr_optimal=extr_optimal,extr_detect=extr_detect,extr_estimate=extr_estimate,$
    abscal=abscal,series=series,n_stack=n_stack,n_img_clean=n_img_clean,n_apwmax=n_apwmax,$
    mask_slit_individual=mask_slit_individual,skipsteps=skipsteps,smooth_overscan=smooth_overscan,debug=debug, dithpos=dithpos

    if(n_elements(tmpdir) ne 1) then tmpdir='../test_data/test_dir/'
    if(NOT file_test(tmpdir,/directory)) then file_mkdir, tmpdir
    if(n_elements(extapw) ne 1) then extapw=2
    if(n_elements(skysubalgorithm) ne 1) then skysubalgorithm=1 ;; non-local Kelson
    if(n_elements(wl_skyline) lt 1) then skylinecorr=0
    if(n_elements(skipsteps) eq 0) then skipsteps=['none']
    if(n_elements(n_stack) eq 0) then n_stack=1

    trimx1=0
    trimx2=0

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_bias=n_elements(rawbias)
    n_arc=n_elements(rawarc)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
    n_dist_map=n_elements(dist_map_frame)
    n_stdstar=n_elements(stdstar)

    if(~keyword_set(skippri)) then begin
        if(n_bias gt 0) then begin
            h_bias_pri=headfits(rawbias[0])
            bias_0=float(mrdfits(rawbias[0],1,h_bias,/silent))
            
            bias_cube=fltarr(sxpar(h_bias,'NAXIS1'),sxpar(h_bias,'NAXIS2'),n_bias)
            gain=sxpar(h_bias,'GAIN')
            if(gain eq 0.0) then gain=1.0
            bias_cube[*,*,0]=bias_0*gain
            for i=1,n_bias-1 do bias_cube[*,*,i]=float(mrdfits(rawbias[i],1,/silent))*gain
            bias=(median(bias_cube,dim=3))[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*] ;;; should do resistant_mean instead but takes too long
            writefits,tmpdir+'bias_i.fits',bias,h_bias_pri
            bias_cube=0.0
        endif else bias=0.0
        if(n_obj gt 1) then begin
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    ccd_combine,rawfilename[findgen(n_stack)+n_stack*i+1],tmpdir+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=15.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
                ccd_combine,tmpdir+'obj_dark_'+string(findgen(n_obj_total),format='(i2.2)')+'.fits',tmpdir+'obj_dark.fits',nsig=10.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
            endif else begin
                ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=10.0/2.0,series=series,n_img_clean=n_img_clean,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
            endelse
        endif else ccd_combine,rawfilename,tmpdir+'obj_dark.fits',nsig=5.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
        if(n_flat gt 0) then ccd_combine,rawflat,tmpdir+'flat_dark.fits',nsig=10.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
        if(n_arc gt 0) then ccd_combine,rawarc,tmpdir+'arc_dark.fits',nsig=10.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
        if(n_skyflat gt 0) then ccd_combine,rawskyflat,tmpdir+'skyflat_dark.fits',nsig=7.0,/norm,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
        if(n_dist_map gt 0) then ccd_combine,dist_map_frame,tmpdir+'distortion_frame.fits',nsig=7.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
        if(n_stdstar gt 0) then ccd_combine,stdstar,tmpdir+'stdstar_dark.fits',nsig=7.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=1
    endif

    h1=headfits(tmpdir+'obj_dark.fits',ext=0)
    grating=strcompress(sxpar(h1,'GRATING'),/remove_all)
    if not keyword_set(dithpos) then dithpos=strcompress(sxpar(h1,'IELOFF'),/remove_all) else dithpos=strcompress(string(dithpos),/remove);get telescope instrument offset from header in arcsec
    logfile=tmpdir+'test_logfile.txt'
    openw,u,logfile,/get_lun
    printf,u,'SIMPLE  =                                 T / FITS-like header'
    printf,u,'LONGSTRN= ''OGIP 1.0''           / The OGIP long string convention may be used.'
    printf,u,'RAW_DIR = ''/tmp/'''
    printf,u,'R_DIR   = '''+tmpdir+''''
    printf,u,'W_DIR   = '''+tmpdir+''''
    printf,u,'RAWEXT  = ''.gz'''
    printf,u,'INSTRUME= ''SALT-RSS'' / spectrograph name'
    printf,u,'SLIT    = ''1.0'' / slit name or MOS'
    printf,u,'GRATING = '''+grating+''' / grating'
    printf,u,'FILTER  = '''' / filter'
    printf,u,'DITHPOS = '+dithpos+' / current dithering position in arcsec'
    printf,u,'BRIGHT  = '+string(keyword_set(bright),format='(i1)')
    printf,u,'EXTAPW  = '+string(extapw,format='(i2)')
    printf,u,'END'
    close,u
    free_lun,u

    ndeg=(n_elements(ndeg_wl) eq 1)? ndeg_wl : 5

    h_a=headfits(tmpdir+'obj_dark.fits',ext=1)
    nx=sxpar(h_a,'NAXIS1')
    ny=sxpar(h_a,'NAXIS2')
    get_coords,radec_deg,instring=sxpar(h1,'RA')+' '+sxpar(h1,'DEC'),/quiet
    radec_deg[0]*=15d
    mjd=sxpar(h1,'JD')-2400000.5d
    baryconf={mjd:mjd,ra:radec_deg[0],dec:radec_deg[1]}

    if(n_arc gt 0) then begin
        h_arc0=headfits(tmpdir+'arc_dark.fits')
        lamp_id=strcompress(sxpar(h_arc0,'LAMPID'),/remove_all)
    endif else begin
        oh=1
        lamp_id='OH'
    endelse

    sky_slit_length_a=ny
    y_ndeg=7

    dist_map=distortion_map_generic(tmpdir+'flat_dark.fits',extnum=1,/detect_slits,maskentry=mask,/longslit,dy_ext=dy_ext)
    ;; GRATING TILT / CAMERA ANGLE MUST BE ADDED TO THIS COMPUTATION
    wl_ini=(grating eq 'PG1800')? [5799.93,0.44839,-1.275088E-05,1.666908E-09,-9.792175E-13,1.419820E-16] : $
          ((grating eq 'PG0900')? [3634.4956,0.99314747, 6.8748804e-06,-5.3224569e-09,3.8168647e-13] : [3601.3712,1.3178084,3.4097229e-06,-3.8326294e-09])

    wl0_lin=(grating eq 'PG1800'? 5800.0 : (grating eq 'PG0900'? 3635.0 : 5800.0))
    dwl_lin=(grating eq 'PG1800'? 0.42   : (grating eq 'PG0900'? 1.0 : 0.42))
    lin_param={grism:grating, filter:'', wl0:wl0_lin, dwl:dwl_lin, npix:nx, wl_min:wl0_lin, wl_max:wl0_lin+nx*dwl_lin}

    exp_flat=['obj','arc','flat']
    if(n_skyflat gt 0) then exp_flat=[exp_flat,'skyflat']
    if(n_stdstar gt 0) then exp_flat=[exp_flat,'stdstar']

    if(keyword_set(series)) then exp_flat=[exp_flat,'obj_clean'+string(lindgen(n_obj),format='(i03)')]

    flat_fielding_mosifu, exp_flat, wdir=tmpdir, dist_map=dist_map, norm_slit=-1, mask=mask, sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat, flat_thr=0.01

    if(~keyword_set(skipwl)) then begin
        arc_list_config=(keyword_set(oh))? $
            {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesOI_O2.tab','linesOH_R2k_1050nm_corr.tab','lines_sky_misc.tab'],label:['OI','OH','other'],weight:[1.,1.,1.]} : $
            ((lamp_id eq 'Ne')? $
            {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesNe_NIST.tab'],weight:[1.0],label:['NeI']} : $
            {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesAr_SALTRSS_lowres.tab'],weight:[1.0],label:['Ar']})

        status=crea_disper_generic_slit(tmpdir+(keyword_set(oh)? 'obj_ff.fits' : 'arc_ff.fits'),0,arc_list_config=arc_list_config,$
            wl_ini,6.0,ndeg,smy=5,ndegy=y_ndeg,dist_map=dist_map,use_oh=oh,debug=debug,/fast,disper_table=disper_table,$
            skip_lines=(keyword_set(oh)? [-1.0] : [-1.0]),/high_curvature,/fullslit)
        writefits,tmpdir+'disper_table.fits',0
        mwrfits,disper_table,tmpdir+'disper_table.fits'
    endif else disper_table=mrdfits(tmpdir+'disper_table.fits',1)

    obs_type_arr=['obj','arc']
    if(n_skyflat gt 0) then obs_type_arr=[obs_type_arr,'skyflat']
    if(n_stdstar gt 0) then obs_type_arr=[obs_type_arr,'stdstar']
    for i=0,n_elements(obs_type_arr)-1 do begin
        hh_pri=headfits(tmpdir+obs_type_arr[i]+'_ff.fits')
        im=mrdfits(tmpdir+obs_type_arr[i]+'_ff.fits',1,hh)
        if(keyword_set(crhfill)) then begin
           im_m3=median(im,3)
           im_m5=median(im,5)
           b_m3=where(finite(im) ne 1, cb_m3)
           if(cb_m3 gt 0) then im[b_m3]=im_m3[b_m3]
           b_m5=where(finite(im) ne 1, cb_m5)
           if(cb_m5 gt 0) then im[b_m5]=im_m5[b_m5]
        endif

        im_lin=(keyword_set(skylinecorr))? $
            linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param) : $
            linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,lin_param=lin_param,barycorr=barycorr,baryv=baryv,baryconf=baryconf)

        writefits,tmpdir+obs_type_arr[i]+'_lin.fits',0,hh_pri
        mwrfits,im_lin,tmpdir+obs_type_arr[i]+'_lin.fits',hh_out
        if(keyword_set(skylinecorr)) then begin
            if((obs_type_arr[i] eq 'obj') or (obs_type_arr[i] eq 'stdstar')) then begin
                file_delete,tmpdir+'flat_emline*_slits.fits',/allow_nonexistent,/quiet
                skyline_flux_data_arr=mos_create_emline_flat(wl_skyline,win_line_arr=12.0*dwl_lin,wdir=tmpdir,mask=mask,dist_map=dist_map,image_type=obs_type_arr[i],slit_poly=-1,/slit_bspl,slit_medw=25,nowlshift=oh,slit_nbkpts=30,line_group=skyline_group,/model2d)
                flatcorr_emline=mrdfits(tmpdir+'flat_emline.fits',1)
            endif else flatcorr_emline=1.0
            im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,skyline_flux_data_arr=skyline_flux_data_arr,lin_param=lin_param,flatcorr_emline=flatcorr_emline,barycorr=barycorr,baryv=baryv,baryconf=baryconf)

            writefits,tmpdir+obs_type_arr[i]+'_lin.fits',0,hh_pri
            mwrfits,im_lin,tmpdir+obs_type_arr[i]+'_lin.fits',hh_out
        endif
    endfor

    if(skysubalgorithm eq 1) then begin ;; Kelson-like sky subtraction using mask_slit_individual and ignoring skyreg
        if(n_elements(subskydimension) ne 1) then subskydimension=2
        if(n_elements(skydegy) ne 1) then skydegy=(subskydimension eq 1)? 0 : 1
        if(skipskymod ne 1) then $
            mos_create_sky_ms,'obj',wdir=tmpdir,dist_map=dist_map,$
                ccdscl=1.0,maskentry=mask,lin_param=lin_param,$
                dimen=subskydimension,skydegy=skydegy,mask_slit_individual=mask_slit_individual,$
                rdnoise=12.0,skylinecorr=skylinecorr,skyline_flux_data_arr=skyline_flux_data_arr

        hh_pri=headfits(tmpdir+'obj_ff.fits')
        im=mrdfits(tmpdir+'obj_ff.fits',1,hh,/silent)
        skymod=mrdfits(tmpdir+'sky_obj_2d_bspl_slits.fits',1,/silent)
        if(keyword_set(skylinecorr)) then flatcorr_emline=mrdfits(tmpdir+'flat_emline.fits',1)
        im_lin=linearisation_simple(im-skymod*flatcorr_emline,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,skyline_flux_data_arr=skyline_flux_data_arr,lin_param=lin_param,flatcorr_emline=flatcorr_emline,barycorr=barycorr,baryv=baryv,baryconf=baryconf)

        writefits,tmpdir+'obj-sky_lin.fits',0,hh_pri
        mwrfits,im_lin,tmpdir+'obj-sky_lin.fits',hh_out
    endif else $
    if(skysubalgorithm eq 2) then begin ;; simple sky subtraction using skyreg
        ;; simple sky subtraction
        hh_pri=headfits(tmpdir+'obj_lin.fits')
        obj_lin=mrdfits(tmpdir+'obj_lin.fits',1,hlin)
        nx_lin=sxpar(hlin,'NAXIS1')
        parse_spechdr,hlin,wl=wl
        if(n_elements(skyreg) eq 0) then begin
            p_obj=median(obj_lin[Nx_lin*0.4:Nx_lin*0.6,*],dim=1)
            p_zero=where(p_obj eq 0,cp_zero)
            if(cp_zero gt 0) then p_obj[p_zero]=!values.f_nan
            outer_p_obj=median([p_obj[0:400],p_obj[1600:*]])
            min_p_obj=min(p_obj,/nan)
            nbin_p=11
            h_prof=histogram(p_obj,min=min_p_obj,max=2*outer_p_obj-min_p_obj,nbin=nbin_p)
            p_fit=mpfitpeak(min_p_obj+dindgen(nbin_p)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.0),h_prof,g_coeff)

            skyreg=where(p_obj gt min_p_obj and $
                          p_obj le min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.), csky)
        endif
        resistant_mean,obj_lin[*,skyreg],3.0,skyvec,dim=2
        writefits,tmpdir+'obj-sky_lin.fits',0,hh_pri
        mwrfits,obj_lin-rebin(skyvec,nx_lin,sxpar(hlin,'NAXIS2')),tmpdir+'obj-sky_lin.fits',hlin
    endif

    flat_norm=mrdfits(tmpdir+'flat_norm.fits',1,hh,/silent)
    flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir,skyline_flux_data_arr=skyline_flux_data_arr,lin_param=lin_param,flatcorr_emline=flatcorr_emline)
    flatflux=median(flat_lin[*,sxpar(hh,'NAXIS2')/2-20:sxpar(hh,'NAXIS2')/2+20],dim=2)
    gfl=where(finite(flatflux) eq 1,cgfl,compl=bfl,ncompl=cbfl)
    if(cbfl gt 0 and cgfl gt 10) then flatflux=interpol(flatflux[gfl],float(gfl),findgen(cbfl+cgfl))
    flatflux[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]=(poly_smooth(flatflux,(cbfl+cgfl)/20.0,deg=2))[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]

    hh_pri=headfits(tmpdir+'obj_lin.fits')
    obj_lin=mrdfits(tmpdir+'obj_lin.fits',1,hlin,/silent)
    err_obj_lin=sqrt((sxpar(hlin,'RDNOISE')+obj_lin*flat_lin)>0)/flat_lin
    writefits,tmpdir+'err_obj_lin.fits',0,hh_pri
    mwrfits,err_obj_lin,tmpdir+'err_obj_lin.fits',hlin

    if(n_stdstar gt 0 and n_elements(stddata) eq 1) then begin
        print,'Computing a throughput curve using a standard star spectrum'
        fluxcal_data=mos_measure_throughput(tmpdir+'stdstar_lin.fits',stddata,$
            mirr=!pi*((410.0/2)^2-(130.0/2)^2),/hdrext,n_wl_seg=10,$
            flatflux=flatflux,blue_thr=wl0_lin,expcorr=1.0/n_stdstar,/lintails)

        if(keyword_set(abscal)) then begin
            parse_spechdr,hlin,wl=wllin
            ext_vec=calc_extinction(wllin,90.0-float(sxpar(hlin,'MOUNT_EL',count=n_el)))
            sxaddpar,hlin,'BUNIT','1.0E-17 erg/cm^2/s/A'
            obj_lin_abs=obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
            writefits,tmpdir+'obj_lin_abs.fits',0
            mwrfits,obj_lin_abs,tmpdir+'obj_lin_abs.fits',hlin

            err_obj_abs=err_obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
            writefits,tmpdir+'err_obj_abs.fits',0
            mwrfits,err_obj_abs,tmpdir+'err_obj_abs.fits',hlin

            if(file_test(tmpdir+'obj-sky_lin.fits')) then begin
                obj_sky=mrdfits(tmpdir+'obj-sky_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
                obj_abs=obj_sky*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
                writefits,tmpdir+'obj_abs.fits',0
                mwrfits,obj_abs,tmpdir+'obj_abs.fits',hsky
            endif

            if(file_test(tmpdir+'stdstar_lin.fits')) then begin
                stdstar_lin=mrdfits(tmpdir+'stdstar_lin.fits',1,hsky,/silent)
                sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
                stdstar_abs=stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec,sxpar(hlin,'NAXIS1'),sxpar(hlin,'NAXIS2'))*1e17
                writefits,tmpdir+'stdstar_lin_abs.fits',0
                mwrfits,stdstar_abs,tmpdir+'stdstar_lin_abs.fits',hsky
            endif
        endif
    endif
end
