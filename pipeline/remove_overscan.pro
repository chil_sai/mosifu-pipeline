function clean_overscan_vector,overscan,w,nsig=nsig,rdnoise=rdnoise
    if(n_params() eq 1) then w=5
    if(w lt 3) then w=3
    if(n_elements(rdnoise) ne 1) then rdnoise=4.0
    if(n_elements(nsig) ne 1) then nsig=3.0

    m_overscan=median(overscan,w)
    bvec=where(abs(overscan-m_overscan) gt rdnoise*nsig, cbvec, compl=gvec, ncompl=cgvec)
    clean_overscan=overscan
    if(cbvec gt 0 and cgvec gt 0) then clean_overscan[bvec]=interpol(overscan[gvec],gvec,bvec)
    return, clean_overscan
end

function remove_overscan,image,datasec,subtract=subtract,median=median,overscan_vec=overscan_vec,sm_w=sm_w,noclean=noclean,directions=directions
;;; subtracts overscan given an image and a datasec array (4 numbers)
    if(n_elements(sm_w) ne 1) then sm_w=0
    s_im=size(image)
    if(n_elements(directions) lt 1 or n_elements(directions) gt 2) then directions=[0,1]
    t0=where(directions eq 0, dir0)
    t1=where(directions eq 1, dir1)

    overscan_x=[-1]
    if(datasec[0] gt 1) then overscan_x=[overscan_x,lindgen(datasec[0]-1l)]
    if(datasec[1] lt s_im[1]) then $
        overscan_x=[overscan_x,datasec[1]+lindgen(s_im[1]-datasec[1]-1l)]
    if(n_elements(overscan_x) gt 1 and keyword_set(subtract) and keyword_set(dir0)) then begin
        overscan_x=overscan_x[1:*]
        if(keyword_set(median)) then $
            overscan_x_vec=median(image[overscan_x,*],dim=1) $
        else begin
            resistant_mean,image[overscan_x,*],3.0,overscan_x_vec,dim=1
            overscan_x_vec=reform(overscan_x_vec,n_elements(overscan_x_vec))
        endelse
        if(~keyword_set(noclean)) then overscan_x_vec=clean_overscan_vector(overscan_x_vec,5)
        if(sm_w gt 0) then overscan_x_vec=smooth(overscan_x_vec,sm_w,/nan)
        image_sub=image-((fltarr(s_im[1])+1) # overscan_x_vec)
    endif else image_sub=image

    overscan_y=[-1]
    if(datasec[2] gt 1) then overscan_y=[overscan_y,lindgen(datasec[2]-1l)]
    if(datasec[3] lt s_im[2]) then $
        overscan_y=[overscan_y,datasec[3]+lindgen(s_im[2]-datasec[3]-1l)]
    if(n_elements(overscan_y) gt 1 and keyword_set(subtract) and keyword_set(dir1)) then begin
        overscan_y=overscan_y[1:*]
        if(keyword_set(median)) then $
            overscan_vec=median(image_sub[*,overscan_y],dim=2) $
        else begin
            resistant_mean,image_sub[*,overscan_y],3.0,overscan_vec,dim=2
            overscan_vec=reform(transpose(overscan_vec))
        endelse
        if(~keyword_set(noclean)) then overscan_vec=clean_overscan_vector(overscan_vec,5)
        if(sm_w gt 0) then overscan_vec=smooth(overscan_vec,sm_w,/nan)
        image_sub=image_sub-(overscan_vec # (fltarr(s_im[2])+1))
    endif ;;; else image_sub=image
    img_sub=image_sub[datasec[0]-1:datasec[1]-1,datasec[2]-1:datasec[3]-1]
    return,img_sub
end

