function linearisation_simple,obs_in,wl_data,hdrin=hdrin,hdrout=hdrout,$
    lin_param=l_par,dist_map=dist_map,skyline_flux_data_arr=skyline_flux_data_arr,wdir=wdir,$
    barycorr=barycorr,baryv=baryv,baryconf=baryconf,$
    flatcorr_emline=flatcorr_emline,debug=debug

obs=obs_in
s_obs=size(obs)
nx_cur=s_obs[1]
ny_cur=s_obs[2]

ny_wl=n_elements(wl_data)
gg=where(finite(wl_data.y_mask) eq 1)

if(keyword_set(barycorr)) then begin
    flagbary=0
    if(n_elements(baryconf) eq 1) then begin
        mjd=baryconf.mjd
        jd=mjd+2400000.5d
        ra=baryconf.ra
        dec=baryconf.dec
        flagbary=1
    endif else begin
        mjd=sxpar(hdrin,'MJD',count=cntmjd)
        jd=mjd+2400000.5d
        ra=sxpar(hdrin,'RA',count=cntra)/!radeg
        dec=sxpar(hdrin,'DEC',count=cntdec)/!radeg
        flagbary=(cntra eq 0 or cntdec eq 0 or cntmjd eq 0)? 0 : 1
    endelse
    if(flagbary eq 1) then begin
        baryvel,jd,2000.0,vh,vb
        vel=vb[0]*cos(dec)*cos(ra)+vb[1]*cos(dec)*sin(ra)+vb[2]*sin(dec)
    endif else begin
        vel=0d
        barycorr=0
        message,/inf,'The barycentric correction could not be computed'
    endelse
    if(n_elements(baryv) ne 1) then baryv=vel
endif else baryv=0d


disp_rel=wl_data
if(n_elements(skyline_flux_data_arr) gt 0) then begin
    fit_skyline=0
    sl_flux=0d
    for f_s=0,n_elements(skyline_flux_data_arr)-1 do begin
        skyline_flux_data_tmp=skyline_flux_data_arr[f_s].slits
        sl_flux_cur=median(skyline_flux_data_tmp.flux)
        if(sl_flux_cur gt sl_flux) then begin
            skyline_flux_data=skyline_flux_data_tmp
            g_skyline=where((finite(skyline_flux_data.wl) eq 1) and (finite(skyline_flux_data.x_mask) eq 1),cg_skyline)
            if((cg_skyline gt 10) and keyword_set(fit_skyline)) then begin
                t_wl=robust_poly_fit(double(g_skyline),skyline_flux_data[g_skyline].wl,5)
                skyline_flux_data[g_skyline].wl=poly(double(g_skyline),t_wl)
            endif
            mean_wl_offset=median(skyline_flux_data.wl-skyline_flux_data.wl_line)
            sl_flux=sl_flux_cur
         endif else cg_skyline=0
    endfor
    for s=0,cg_skyline-1 do begin
        disp_rel[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line) -mean_wl_offset/double(cg_skyline)
    endfor
endif

gg=where(finite(disp_rel.y_mask) eq 1)
wl_first=poly(dindgen(nx_cur),disp_rel[gg[0]].wl_sol)

s_dm=size(dist_map)

if(n_elements(l_par) ne 1) then $
    l_par={grism:'', filter:'', $ 
           wl0:wl_first[0], dwl:abs(wl_first[nx_cur-1]-wl_first[0])/(nx_cur-1d), npix:nx_cur, $
           wl_min:wl_first[0], wl_max:wl_first[nx_cur-1]}

wl0=l_par.wl0
dwl=l_par.dwl
npix=l_par.npix
wl_min=l_par.wl_min
wl_max=l_par.wl_max

y_off=sxpar(hdrin,'YOFFSET')

prof_med = median(obs,dim=1)

if(n_elements(flatcorr_emline) ne n_elements(obs)) then flatcorr_emline=1.0
obs_t=(s_dm[0] eq 1 and s_dm[2] eq 8)? mos_rectify_slit(obs/flatcorr_emline,dist_map,y_off,/edge) : obs

obs_lin=fltarr(npix,ny_cur)
wl_sc_lin=wl0+findgen(npix)*dwl
if(baryv ne 0d) then wl_sc_lin/=(1d +baryv/299792.458d)
flag_arr=bytarr(ny_cur)

for i=0,ny_cur-1 do begin
    y_corr=y_off+i
    if(y_corr lt 0 or y_corr gt ny_wl-1) then continue
    wl_sc_cur=poly(findgen(nx_cur),disp_rel[y_corr].wl_sol)
    obs_lin[*,i]=interpol(obs_t[*,i],wl_sc_cur,wl_sc_lin)
    goodpix=where(finite(obs_t[*,i]) eq 1, cgoodpix, ncompl=cbad)
    if(cgoodpix gt 0) then begin
        mingoodwl=min(wl_sc_cur[goodpix],minidx,max=maxgoodwl,subscript_max=maxidx)
        badwl=where((wl_sc_lin lt mingoodwl) or $
                    (wl_sc_lin gt maxgoodwl), cbadwl)
        if(cbadwl gt 1) then obs_lin[badwl,i]=!values.f_nan
    endif
endfor

good_reg=where((wl_sc_lin ge wl_min) and (wl_sc_lin le wl_max))
prof_spec = median(obs_lin[good_reg,*],dim=1)
gprof_spec = where(finite(prof_spec) eq 1, cgprof_spec)
xb=findgen(ny_cur)-(ny_cur-1)/2.0

if(n_elements(hdrin) gt 0) then hdrout=hdrin else mkhdr, hdrout, obs_lin
sxaddpar,hdrout,'CTYPE1','AWAV'
sxaddpar,hdrout,'CUNIT1','Angstrom'
sxaddpar,hdrout,'CRPIX1',1.0
sxaddpar,hdrout,'CRVAL1',wl0
sxaddpar,hdrout,'CDELT1',dwl
sxaddpar,hdrout,'CD1_1',dwl
sxaddpar,hdrout,'CTYPE2',''
sxaddpar,hdrout,'CUNIT2',''
sxaddpar,hdrout,'CRPIX2',1.0
sxaddpar,hdrout,'CRVAL2',1.0
sxaddpar,hdrout,'CDELT2',1.0
sxaddpar,hdrout,'CD2_2',1.0
sxaddpar,hdrout,'BARYCORR',keyword_set(barycorr),' Barycentric correction '+(keyword_set(barycorr)? '' : 'not ')+'applied'
if(keyword_set(barycorr)) then sxaddpar,hdrout,'HELIO_RV',baryv,' km/s barycentric correction applied to wavelengths'

if(n_elements(mask_entry) eq 1) then begin
    sxaddpar,hdrout,'SLITID',mask_entry.slit,' slit id'
    sxaddpar,hdrout,'SLITRA',mask_entry.ra,' ra slit coordinate'
    sxaddpar,hdrout,'SLITDEC',mask_entry.dec,' dec slit coordinate'
    sxaddpar,hdrout,'SLITX',mask_entry.x,' x slit coordinate'
    sxaddpar,hdrout,'SLITY',mask_entry.y,' y slit coordinate'
    sxaddpar,hdrout,'SLITTARG',mask_entry.target,' target'
    sxaddpar,hdrout,'SLITOBJ',mask_entry.object,' object'
    sxaddpar,hdrout,'SLITTYPE',mask_entry.type,' type'
    sxaddpar,hdrout,'SLITHEIG',mask_entry.height,' slit height'
    sxaddpar,hdrout,'SLITWIDT',mask_entry.width,' slit width'
    sxaddpar,hdrout,'SLITOFFS',mask_entry.offset,' slit offset'
    sxaddpar,hdrout,'SLITTHET',mask_entry.theta,' slit tilt (theta)'
endif
sxaddpar,hdrout,'RADECSYS','FK5',' Coordinate System'

return,obs_lin

end
