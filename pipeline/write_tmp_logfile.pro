pro write_tmp_logfile,logfile_name,rdir,wdir,instrument=instrument,$
    slit=slit,grating=grating,filter=filter,dithpos=dithpos,$
    bright=bright,extapw=extapw,rawext=rawext

    if(n_params() eq 2) then wdir=rdir
    if(n_elements(instrument) ne 1) then instrument='CMO-TDS'
    if(n_elements(slit) ne 1) then slit='1.0'
    if(n_elements(rawext) ne 1) then rawext=''
    if(n_elements(grating) ne 1) then grating=''
    if(n_elements(filter) ne 1) then filter=''
    if(n_elements(dithpos) ne 1) then dithpos='0.0'
    if(n_elements(extapw) ne 1) then extapw=3

    openw,u,logfile_name,/get_lun
    printf,u,'SIMPLE  =                                 T / FITS-like header'
    printf,u,'LONGSTRN= ''OGIP 1.0''           / The OGIP long string convention may be used.'
    printf,u,'RAW_DIR = ''/tmp/'''
    printf,u,'R_DIR   = '''+rdir+''''
    printf,u,'W_DIR   = '''+wdir+''''
    printf,u,'RAWEXT  = '''+rawext+''''
    printf,u,'INSTRUME= '''+instrument+''' / spectrograph name'
    printf,u,'SLIT    = '''+slit+''' / slit name or MOS'
    printf,u,'GRATING = '''+grating+''' / grating'
    printf,u,'FILTER  = '''+filter+''' / filter'
    printf,u,'DITHPOS = '+dithpos+' / current dithering position in arcsec'
    printf,u,'BRIGHT  = '+string(keyword_set(bright),format='(i1)')
    printf,u,'EXTAPW  = '+string(extapw,format='(i2)')
    printf,u,'END'
    close,u
    free_lun,u
end