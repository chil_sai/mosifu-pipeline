pro flat_fielding_mosifu,image_type,$
    wdir=wdir,flat_thr=flat_thr,$
    dist_map=dist_map,mask=mask,$
    norm_slit=norm_slit,dymask=dymask,$
    sub_sc_flat=sub_sc_flat,sub_sc_sci=sub_sc_sci,$
    illum_corr_str=illum_corr_str,$
    fringing=fringing,smw_fringes=smw_fringes,debug=debug,$
	slit_edg_mask=slit_edg_mask

message,'multislit flat-field correction ',/cont

if(n_elements(dymask) ne 1) then dymask=1

if(n_elements(wdir) ne 1) then wdir='./'
flat_a=(mrdfits(wdir+'flat_dark.fits',1,ha,/silent))
nx=sxpar(ha,'NAXIS1')
ny=sxpar(ha,'NAXIS2')

if(keyword_set(slit_edg_mask)) then begin
   for i=0, nx-1 do begin
       med_cur = median(flat_a[i, *])
       std_cur = stddev(flat_a[i, *], /nan)
       mask_idx = where(flat_a[i, *] lt med_cur-2*std_cur)
       flat_a[i, mask_idx] = !values.f_nan
    endfor
endif

if(n_elements(smw_fringes) ne 1) then smw_fringes=nx/50.

if(n_elements(mask) eq 0) then mask={slit:0,ra:0d,dec:0d,x:0d,y:52d,$
                    target:0ll,object:sxpar(ha,'OBJECT'),type:'TARGET',$
                    wstart:0.0,wend:0.0,height:float(ny)-112.,width:1.0,offset:0.0,theta:0.0,$
                    bbox:dblarr(8),$
                    mask_id:-1l,mask_name:'',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
                    mask_pa:0d,corners:[-0.5,-ny/2,0.5,ny/2]}

n_slits_a=n_elements(mask)
if(n_slits_a eq 1) then longslit=1

y_slits_a=get_slit_region(mask,nx=nx,ny=ny,ratio=1.0,dist_map=dist_map,slit_trace=slit_trace)

if(keyword_set(sub_sc_flat)) then begin
    flat_a_sc=mos_model_sc_light(flat_a,mask=mask,dist_map=dist_map,margin=dymask,/bspline,deg2=(n_slits_a lt 2)? 1 : (n_slits_a lt 4? 2 : 3))
    flat_a-=flat_a_sc
endif else flat_a_sc=0.0

flat_orig_a=flat_a

flat_r_a=poly_2d(flat_a,dist_map.kxwrap,dist_map.kywrap,1)

illum_corr_a = (n_elements(illum_corr_str) eq 1)? rebin(transpose(poly(dindgen(ny),illum_corr_str.illum_corr_coeff)),nx,ny) : 1.0

flat_a=flat_r_a
flat_r_a=0

flat_norm_a=flat_a+!values.f_nan
flat_sm_a=flat_norm_a

flat_ncoeff_a=flat_norm_a*0.0

fringe_pattern=flat_norm_a*0.0
flat_a_corr=flat_norm_a*0.0


for i=0,n_slits_a-1 do begin
    ymin_cur = y_slits_a[0,i]
    ymax_cur = y_slits_a[1,i]
    print,'Processing slit: ',i+1,'/',n_slits_a,ymin_cur,ymax_cur
    if(keyword_set(fringing)) then begin
        fringe_pattern_slit=mos_slit_create_fringe_pattern(flat_a[*,ymin_cur:ymax_cur])
        fringe_pattern[*,ymin_cur:ymax_cur]=fringe_pattern_slit

        flat_a_slit_corr=mos_slit_correct_fringes(flat_a[*,ymin_cur:ymax_cur],fringe_pattern[*,ymin_cur:ymax_cur],smw=smw_fringes)
        flat_a_corr[*,ymin_cur:ymax_cur]=flat_a_slit_corr
    endif else flat_a_corr[*,ymin_cur:ymax_cur]=flat_a[*,ymin_cur:ymax_cur]

    for y=ymin_cur,ymax_cur do begin
        line_cur=median(flat_a_corr[*,y],15)
        line_cur[0:9]=0.0    ;;; cut the edges
        line_cur[nx-10:nx-1]=0.0
        flat_sm_a[*,y]=smooth(line_cur,31,/nan)
    endfor
    
;    writefits,wdir+'fringe_pattern.fits',0
;    mwrfits,fringe_pattern,wdir+'fringe_pattern.fits',ha
    maxv=(n_elements(norm_slit) eq 1 or ymin_cur eq ymax_cur)? 1.0 : max(median(flat_sm_a[*,ymin_cur:ymax_cur],dim=2),/nan)
    flat_norm_a[*,ymin_cur:ymax_cur]=flat_a_corr[*,ymin_cur:ymax_cur]/maxv
    flat_ncoeff_a[*,ymin_cur:ymax_cur]=maxv

    if(keyword_set(fringing)) then begin
        flat_fringes_r_a=flat_a-flat_a_corr
        flat_fringes_a=poly_2d(flat_fringes_r_a,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
    endif else flat_fringes_a=0.0
endfor

if(n_elements(norm_slit) eq 1) then begin
    if(norm_slit lt 0) then begin
        max_vals=dblarr(n_slits_a)+!values.d_nan
        for i=0,n_slits_a-1 do $
            if(strcompress(mask[i].type,/remove_all) eq 'TARGET') then $
                max_vals[i]=max(flat_sm_a[*,y_slits_a[0,i]:y_slits_a[1,i]],/nan)
        d_ncoeff=median(max_vals)
    endif else d_ncoeff=max(flat_sm_a[*,y_slits_a[0,norm_slit]:y_slits_a[1,norm_slit]],/nan)
    flat_ncoeff_a=flat_ncoeff_a*d_ncoeff
endif

flat_ncoeff_geom_a=poly_2d(flat_ncoeff_a/illum_corr_a,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
flat_norm_a=(flat_orig_a-flat_fringes_a)/flat_ncoeff_geom_a

if(n_elements(flat_thr) ne 1) then flat_thr=0.03 ;; 0.05
low_flat_a=where(median(flat_norm_a,5) lt flat_thr,clf_a)
mask_flat_a=byte(flat_norm_a*0.0)
if(clf_a gt 0) then begin
    mask_flat_a[low_flat_a]=1
    mask_flat_a=mask_flat_a+shift(mask_flat_a,0,dymask)+shift(mask_flat_a,0,-dymask)
    flat_norm_a[where(mask_flat_a gt 0)]=!values.f_nan
endif

;clean_hdr,h
sxaddpar,ha,'datamin',0.8
sxaddpar,ha,'datamax',1.2
sxaddpar,hb,'datamin',0.8
sxaddpar,hb,'datamax',1.2
;mkhdr,h_fl,float(flat_norm_a),/extend
;h_fl=[h_fl[0:5],h]

writefits,wdir+'flat_norm.fits',0
mwrfits,float(flat_norm_a),wdir+'flat_norm.fits',ha
writefits,wdir+'flat_ncoeff.fits',0
mwrfits,float(flat_ncoeff_geom_a),wdir+'flat_ncoeff.fits',ha

for n=0,n_elements(image_type)-1 do begin
    print,'reading  '+wdir+image_type[n]+'_dark.fits'
    h_pri=headfits(wdir+image_type[n]+'_dark.fits')
    obj_a=mrdfits(wdir+image_type[n]+'_dark.fits',1,ha,/silent)

    if((keyword_set(sub_sc_sci) and ((strmid(image_type[n],0,3) eq 'obj') or (image_type[n] eq 'stdstar') or (image_type[n] eq 'star'))) $
;       or (keyword_set(sub_sc_flat) and (strmid(image_type[n],0,4) eq 'flat')) $
    ) then begin
        obj_a_sc=mos_model_sc_light(obj_a,mask=mask,dist_map=dist_map,margin=dymask,/bspline,deg2=(n_slits_a lt 2)? 1 : 2)
        obj_a-=obj_a_sc
    endif

    if(keyword_set(fringing)) then begin
        obj_r_a=poly_2d(obj_a,dist_map.kxwrap,dist_map.kywrap,1)
        obj_r_a_corr=obj_r_a*0.0 

        for i=0,n_slits_a-1 do begin
            ymin_cur = y_slits_a[0,i]
            ymax_cur = y_slits_a[1,i]
            print,'Processing slit: ',i+1,'/',n_slits_a,ymin_cur,ymax_cur
            if(keyword_set(fringing)) then begin
                obj_r_a_slit_corr=mos_slit_correct_fringes(obj_r_a[*,ymin_cur:ymax_cur],fringe_pattern[*,ymin_cur:ymax_cur],smw=smw_fringes)
                obj_r_a_corr[*,ymin_cur:ymax_cur]=obj_r_a_slit_corr
            endif
        endfor
        obj_fringes_r_a=obj_r_a-obj_r_a_corr
        obj_fringes_a=poly_2d(obj_fringes_r_a,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
        obj_a=obj_a-obj_fringes_a
    endif

    h_out_a=ha
    sxaddhist,'flat-field corrected',h_out_a
    writefits,wdir+image_type[n]+'_ff.fits',0,h_pri
    mwrfits,float(obj_a/flat_norm_a),wdir+image_type[n]+'_ff.fits',h_out_a
endfor

return

end

