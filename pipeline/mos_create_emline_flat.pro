function mos_create_emline_flat,wl_line_arr_inp,model2d=model2d,win_line_arr=win_line_arr,line_group=line_group,nowlshift=nowlshift,$
    wdir=wdir,dist_map=dist_map,mask=mask,image_type=image_type,slitsuff=slitsuff,outsuff=outsuff,pady=pady,$
    slit_poly=slit_poly,slit_medw=slit_medw,slit_bspl=slit_bspl,slit_nbkpts=slit_nbkpts,$
    auto_slit_poly=auto_slit_poly,fit=fit,adj=adj,nterms=nterms

    if(n_elements(slitsuff) ne 1) then slitsuff=''
    if(n_elements(outsuff) ne 1) then outsuff=''
    if(n_elements(image_type) ne 1) then image_type='obj'
    file_dark=(file_test(wdir+image_type+'_dark.fits'))? wdir+image_type+'_dark.fits' : wdir+'obj_dark.fits'
    h0=headfits(file_dark,ext=1)

    wl_line_arr=wl_line_arr_inp
    n_slits=n_elements(mask)
    if (n_slits eq 1) and not tag_exist(mask, 'slit') then begin
      n_slits=0
    endif

    if n_slits gt 0 then h_slit=headfits(wdir+image_type+slitsuff+'_lin.fits',ext=1) else h_slit=''
    ; ny=sxpar(h0,'NAXIS2')
    nx_lin=(n_elements(h_slit) gt 1)? sxpar(h_slit,'NAXIS1') : sxpar(h0,'NAXIS1')
    parse_spechdr,n_elements(h_slit) gt 1? h_slit : h0,wl=wl_lin
    disper=mrdfits(wdir+'disper_table.fits',1,/silent)
    ny_full=n_elements(disper)

    n_wl_lines = n_elements(wl_line_arr)
    if(n_elements(line_group) ne n_wl_lines) then line_group=lindgen(n_wl_lines)
    for i=0,n_wl_lines-1 do begin
        line_flux_data=mos_measure_emline(image_type,wl_line_arr[i],mask=mask,wdir=wdir,slitsuff=slitsuff,$
            win_line=(n_elements(win_line_arr) eq n_wl_lines)? win_line_arr[i] : win_line_arr,nowlshift=nowlshift,slit_medw=slit_medw,$
            slit_poly=slit_poly,auto_slit_poly=auto_slit_poly,slit_bspl=slit_bspl,slit_nbkpts=slit_nbkpts, nterms=nterms)

        if(i eq 0) then skyline_flux_data_arr=replicate(line_flux_data,n_wl_lines)
        skyline_flux_data_arr[i]=line_flux_data
        ny=n_elements(line_flux_data.slits)
        if(i eq 0) then begin
            slits_data_all=dblarr(n_elements(line_flux_data.slits.sigma),n_wl_lines)
        endif
        slits_data_all[*,i]=line_flux_data.slits.sigma*line_flux_data.slits.flux/median(line_flux_data.slits.sigma*line_flux_data.slits.flux)
        if(keyword_set(fit)) then begin
            if n_slits gt 0 then begin
                good=where((finite(slits_data_all[*,i]) eq 1) and $
                             (finite(line_flux_data.slits.x_mask) eq 1), c_good)
                fit_ydeg=5
                x_uni_idx=uniq(line_flux_data.slits[good].x_mask,sort(line_flux_data.slits[good].x_mask))
                fit_xdeg=(n_elements(x_uni_idx) ge 3)? 2 : 0
                t=robust_poly_fit(double(good),slits_data_all[good,i],fit_ydeg)
                if(fit_xdeg gt 0) then begin
                    good_i2=where(abs(slits_data_all[*,i]-poly(dindgen(ny),t)) lt $
                                    5.0*robust_sigma(slits_data_all[good,i]-poly(good,t)),cgood_i2)
                    data_s=dblarr(3,cgood_i2)
                    data_s[0,*]=line_flux_data.slits[good_i2].x_mask
                    data_s[1,*]=line_flux_data.slits[good_i2].y_mask
                    data_s[2,*]=transpose(slits_data_all[good_i2,i])
                    pp=sfit_2deg(data_s,fit_xdeg,fit_ydeg,/irreg,kx=kx)
                    slits_data_all[*,i]=poly2d(line_flux_data.slits.x_mask,line_flux_data.slits.y_mask,kx,$
                                                 /irreg,deg1=fit_xdeg,deg2=fit_ydeg)
                endif else slits_data_all[*,i]=poly(dindgen(ny),t)
            endif
        endif
    endfor

    model2d_flag=0
    if(keyword_set(model2d) and n_wl_lines ge 3) then begin
        model2d_flag=1
        uq_line_group_idx=uniq(line_group,sort(line_group))
        if(n_elements(uq_line_group_idx) ne n_wl_lines) then begin
            slits_data_all_tmp=fltarr(n_elements(slits_data_all[*,0]),n_elements(uq_line_group_idx))
            wl_line_arr_tmp=dblarr(n_elements(uq_line_group_idx))
            for q=0,n_elements(uq_line_group_idx)-1 do begin
                qidx=where(line_group eq line_group[uq_line_group_idx[q]],cqidx)
                slits_data_all_tmp[*,q]=(cqidx eq 1)? slits_data_all[*,line_group[qidx]] : median(slits_data_all[*,line_group[qidx]],dim=2)
                wl_line_arr_tmp[q]=(cqidx eq 1)? wl_line_arr[line_group[qidx]] : total(wl_line_arr[line_group[qidx]])/cqidx
            endfor
            slits_data_all=temporary(slits_data_all_tmp)
            wl_line_arr=temporary(wl_line_arr_tmp)
            n_wl_lines=n_elements(uq_line_group_idx)
        endif
        slits_data=fltarr(nx_lin,ny_full)+!values.f_nan
        for y=0,n_elements(slits_data_all[*,0])-1 do begin
            good_lines=where(finite(slits_data_all[y,*]) eq 1, c_good_lines)
            if(c_good_lines ge 3) then begin
                yy=spl_init(wl_line_arr[good_lines],transpose(slits_data_all[y,good_lines]),yp0=0d,ypn_1=0d,/double)
                min_wl_lines_cur=min(wl_line_arr[good_lines],max=max_wl_lines_cur)
                wl_seg=where(wl_lin ge min_wl_lines_cur and wl_lin le max_wl_lines_cur, c_wl_seg)
                if(c_wl_seg gt 0) then begin
                    slits_data[wl_seg,y]=spl_interp(wl_line_arr[good_lines],transpose(slits_data_all[y,good_lines]),yy,wl_lin[wl_seg])
                    if(wl_seg[0] gt 0) then slits_data[0:wl_seg[0]-1,y]=slits_data[wl_seg[0],y]
                    if(wl_seg[c_wl_seg-1L] lt nx_lin-1L) then slits_data[wl_seg[c_wl_seg-1L]+1:*,y]=slits_data[wl_seg[c_wl_seg-1L],y]
                endif
            endif
        endfor
    endif else $
        slits_data=(n_wl_lines eq 1)? slits_data_all[*,0] : median(slits_data_all,dim=2)

    flat_corr_data={slits:slits_data}

    hpri=headfits(wdir+image_type+slitsuff+'_ff.fits')

    f_out=wdir+'flat_emline'+outsuff+slitsuff+'.fits'
    writefits,f_out,0,hpri

    mask_cur=mask
    h_obj=h0

    nx=sxpar(h_obj,'NAXIS1')
    ny=sxpar(h_obj,'NAXIS2')

    f_dt=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
    disp_rel=mrdfits(wdir+f_dt,1,/silent)
    line_flux_data_side=line_flux_data.slits
    flat_corr_data_side=flat_corr_data.slits

    xx=findgen(nx)

    y_scl=24.555832d
    if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl

    nslit=n_slits
    nslitoff=0

    slit_reg=get_slit_region(mask_cur,nx=nx,ny=ny,pady=pady,dist_map=dist_map,slit_trace=slit_trace)

    for j=0,nslit-1 do begin
        print,'Writing corrected flat for slit: ',j+1+nslitoff
        h_slit=headfits(wdir+image_type+slitsuff+'_ff.fits',ext=j+1+nslitoff)
        nx_cur=long(sxpar(h_slit,'NAXIS1'))
        ny_cur=long(sxpar(h_slit,'NAXIS2'))
        dy = (slit_trace[j].y_trace - (slit_reg[0,j]+slit_reg[1,j])/2d)/y_scl

        yoff=sxpar(h_slit,'YOFFSET')
        ymap=(dblarr(nx_cur)+1d) # (disp_rel[yoff:yoff+ny_cur-1].y_mask)
        ymap += (dy) # (dblarr(ny_cur)+1d)
        if(model2d_flag eq 1) then begin
            xmap=ymap*0d
            for y=yoff,yoff+ny_cur-1 do begin
                xmap[*,y-yoff]=interpol(dindgen(nx_cur),wl_lin,poly(dindgen(nx_cur),disp_rel[y].wl_sol))
            endfor
            slit_map=reform(interpolate(flat_corr_data_side,reform(xmap,nx_cur*ny_cur),reform(ymap,nx_cur*ny_cur),missing=!values.f_nan),nx_cur,ny_cur)
        endif else begin
            slit_map=interpol(flat_corr_data_side,line_flux_data_side.y_mask,ymap)
        endelse

        mwrfits,float(slit_map),f_out,h_slit,/silent
    endfor

    return,skyline_flux_data_arr
end
