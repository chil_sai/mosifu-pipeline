pro reduce_deimos_ls,rawfilename,bias=rawbias,flat=rawflat,arc_krxecdzn=rawarc_krxecdzn,arc_nearkrxe=rawarc_nearkrxe,tmpdir=tmpdir,$
    dist_map_frame=dist_map_frame,stdstar=rawstdstar,stddata=stddata,vidinp=vidinp,oh=oh,skipsteps=skipsteps,$
    n_stack=n_stack,debug=debug,skysubalgorithm=skysubalgorithm,telluric=telluric,Vmag_tell=Vmag_tell,abscal=abscal,$
	mid_slitlet=mid_slitlet

    if(n_elements(n_stack) eq 0) then n_stack=1

    t=where(skipsteps eq 'pri',skippri)
    t=where(skipsteps eq 'wl',skipwl)
    t=where(skipsteps eq 'skymod',skipskymod)

for l=0, n_elements(vidinp)-1  do begin

    tmpdir_cur = tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'
    file_mkdir, tmpdir_cur

	ext=vidinp[l]/2
	det_type = (where(vidinp[l] eq [10, 12, 14, 16]) ne -1); 1 -- red, 0 -- blue

    n_obj_total=n_elements(rawfilename)
    n_obj=n_obj_total/n_stack
    n_bias=n_elements(rawbias)
    n_arc_krxecdzn=n_elements(rawarc_krxecdzn)
    n_arc_nearkrxe=n_elements(rawarc_nearkrxe)
    n_flat=n_elements(rawflat)
    n_skyflat=n_elements(rawskyflat)
    n_dist_map=n_elements(dist_map_frame)
    n_stdstar=n_elements(rawstdstar)
	if(n_elements(stddata) eq 0) then stddata=''

;    rotdir=1
	if(keyword_set(mid_slitlet)) then begin 
    	trimx1=650
    	trimx2=700
	endif else begin
        trimx1=0
        trimx2=0
	endelse

    ndeg=(n_elements(ndeg_wl) eq 1)? ndeg_wl : 4
	y_ndeg=7
	

    if(~keyword_set(skippri)) then begin
        rotdir = (det_type eq 1)? 1 : 3
        if(n_elements(nsig_clip) ne 1) then nsig_clip=5.
        if(n_bias gt 0) then begin
            if(keyword_set(verbose)) then print,'Processing bias frames'
            bias_0=float(readfits(rawbias[0],h_bias,/silent,exten_no=ext))
            bias_cube=fltarr(sxpar(h_bias,'NAXIS1'),sxpar(h_bias,'NAXIS2'),n_bias)
            gain=1.0 ;;;sxpar(h_bias,'GAIN')
            if(gain eq 0.0) then gain=1.0
            bias_cube[*,*,0]=bias_0*gain
            for i=1,n_bias-1 do bias_cube[*,*,i]=float(readfits(rawbias[i],/silent,exten_no=ext))*gain
            if(n_bias gt 1) then begin
                bias_cube = bias_cube[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*,*]
                bias = median(bias_cube,dim=3) ;should be resistant_mean instead
            endif else bias=bias_cube[trimx1:sxpar(h_bias,'NAXIS1')-trimx2-1,*]
            ;bias=rotate(bias,rotdir)
            writefits,tmpdir_cur+'bias_i.fits',bias,h_bias
            bias_cube=0.0
        endif else bias=0.0
;        readn_bias=(n_elements(bias) eq 1)? 0.0 : robust_sigma(bias)
        if(n_obj gt 1) then begin
			print, 'n_obj'
            if(n_stack gt 1) then begin
                for i=0,n_obj-1 do $
                    ccd_combine,rawfilename[findgen(n_stack)+n_stack*i+1],tmpdir_cur+'obj_clean'+string(i,format='(i3.3)')+'_dark.fits',nsig=15.0,readn=readn,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
                ccd_combine,tmpdir_cur+'obj_dark_'+string(findgen(n_obj_total),format='(i2.2)')+'.fits',tmpdir_cur+'obj_dark.fits',nsig=10.0,readn=readn,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
            endif else begin
                ccd_combine,rawfilename,tmpdir_cur+'obj_dark.fits',nsig=5.0,series=series,n_img_clean=n_img_clean,readn=readn,bias=bias,trimx1=trimx1,trimx2=trimx2,rot=rotdir,ext=ext
            endelse
        endif else ccd_combine,rawfilename,tmpdir_cur+'obj_dark.fits',nsig=5.0,bias=bias,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
;stop
        if(n_flat gt 0) then ccd_combine,rawflat,tmpdir_cur+'flat_dark.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_arc_krxecdzn gt 0) then ccd_combine,rawarc_krxecdzn,tmpdir_cur+'arc_krxecdzn_dark.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_arc_nearkrxe gt 0) then ccd_combine,rawarc_nearkrxe,tmpdir_cur+'arc_nearkrxe_dark.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_skyflat gt 0) then ccd_combine,rawskyflat,tmpdir_cur+'skyflat_dark.fits',nsig=7.0,/norm,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_dist_map gt 0) then ccd_combine,dist_map_frame,tmpdir_cur+'distortion_frame.fits',nsig=10.0,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
        if(n_stdstar gt 0) then ccd_combine,rawstdstar,tmpdir_cur+'stdstar_dark.fits',nsig=10.0,readn=readn,bias=bias,dark_expnorm=dark_expnorm,trimx1=trimx1,trimx2=trimx2,ext=ext,rot=rotdir
    endif

    dist_map=distortion_map_generic(tmpdir_cur+'stdstar_dark.fits',extnum=1,maskentry=mask,/longslit,deg1=7,nxgrid=(det_type eq 1? 30 : 30));min_slit_length=1)

;	ha = headfits(tmpdir_cur+'obj_dark.fits', ext=1)
;	ny = sxpar(ha,'NAXIS2')
;	mask={slit:0,ra:0d,dec:0d,x:0d,y:52d,$
;                    target:0ll,object:sxpar(ha,'OBJECT'),type:'TARGET',$
;                    wstart:0.0,wend:0.0,height:float(ny)-20.,width:1.0,offset:0.0,theta:0.0,$
;                    bbox:dblarr(8),$
;                    mask_id:-1l,mask_name:'',mask_ra:!values.d_nan,mask_dec:!values.d_nan,$
;                    mask_pa:0d,corners:[-0.5,-ny/2,0.5,ny/2]}

	exp_flat=['obj','arc_krxecdzn','arc_nearkrxe','flat']
    if(n_stdstar gt 0) then exp_flat=[exp_flat,'stdstar']
    flat_fielding_mosifu, exp_flat, wdir=tmpdir_cur, dist_map=dist_map, norm_slit=-1, mask=mask, sub_sc_sci=sub_sc_sci, sub_sc_flat=sub_sc_flat, flat_thr=0.01, /slit_edg_mask

    ha_cd0 = headfits(rawarc_krxecdzn[0], ext=0)
    ha_ne0 = headfits(rawarc_nearkrxe[0], ext=0)

	if(n_arc_krxecdzn ne 0 and n_arc_nearkrxe ne 0) then begin
        lamp_id = strtrim(sxpar(ha_cd0, 'lamps'), 2) + ' ' + strtrim(sxpar(ha_ne0, 'lamps'), 2)
        arc_krxecdzn = mrdfits(tmpdir_cur+'arc_krxecdzn_ff.fits', 1, ha_cd)
        arc_nearkrxe = mrdfits(tmpdir_cur+'arc_nearkrxe_ff.fits', 1, ha_ne)
		arc = arc_krxecdzn + arc_nearkrxe
		sxaddpar, ha_cd, 'lamps', lamp_id
		sxaddpar, ha_cd, 'el', sxpar()
        writefits, tmpdir_cur+'arc_ff.fits', 0
        mwrfits, arc, tmpdir_cur+'arc_ff.fits', ha_cd
;		stop
	endif else begin
		if (n_arc_nearkrxe eq 0) then begin
        	lamp_id = strtrim(sxpar(ha_cd0, 'lamps'), 2)
        	arc = mrdfits(tmpdir_cur+'arc_krxecdzn_ff.fits', 1, ha)
        	sxaddpar, ha, 'lamps', lamp_id
        	writefits, tmpdir_cur+'arc_ff.fits', 0
        	mwrfits, arc, tmpdir_cur+'arc_ff.fits', ha
    	endif
    	if (n_arc_krxecdzn eq 0) then begin
        	lamp_id = strtrim(sxpar(ha_ne0, 'lamps'), 2)
        	arc = mrdfits(tmpdir_cur+'arc_nearkrxe_ff.fits', 1, ha)
	        sxaddpar, ha, 'lamps', lamp_id
        	writefits, tmpdir_cur+'arc_ff.fits', 0
        	mwrfits, arc, tmpdir_cur+'arc_ff.fits', ha
    	endif
	endelse
;stop
    if (det_type eq 1) then $
;        wl_ini=[6642.4102d, 0.42692521d, 1.0081339d-05, -5.2889950d-09, 1.1361750d-12, -9.6471653d-17] $
;        wl_ini=[6639.8389, 0.43569523, 1.7201110e-07, -3.4041200d-10, 5.3434536d-15] $
;       wl_ini = [6645.13377d, 0.43500d, 6.13698d-07, -4.90542d-10, 2.37224d-14]/(1+8.34d-5) $
;        wl_ini=[6507.1665e, 0.44666430e, -8.9501009e-06, 2.6810538e-09, -3.4350360e-13] $
;        wl_ini=[6503.7720d, 0.45726338d, -2.1284126d-05, 9.3843013d-09, -2.0522676d-12, 1.6469116d-16] $
;        wl_ini=[6534.7510d, 0.38230902d, 4.0900293d-05, -1.3026829d-08, 1.3955841d-12] $
;        wl_ini=[6512.4307e, 0.43427494e, 7.3209213e-07, -3.9611675e-10]$
;       wl_ini=[6512.009d, 0.43445d, 1.14316d-06, -7.1056d-10, 5.630721d-14]/(1+8.34d-5) $;with correction from vac to air applied
       wl_ini=[6518.129d, 0.43445d, 1.14316d-06, -7.1056d-10, 5.630721d-14]/(1+8.34d-5) $;with correction from vac to air applied; for j1631
    else $
        wl_ini=[4758.682d, 0.41415d, 5.01940d-06, -5.3742d-10, 2.640442d-14]/(1+8.34d-5);with correction from vac to air applied
    if(~keyword_set(skipwl)) then begin
;        h_arc = headfits(rawarc[0], ext=0)
;        lamp_id = strtrim(sxpar(h_arc, 'lamps'), 2)
        if (keyword_set(oh)) then begin
                arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesOI_O2.tab','linesOH_R2k_1050nm_corr.tab','lines_sky_misc.tab'],label:['OI','OH','other'],weight:[1.,1.,1.]}
        endif else begin
            if (lamp_id eq 'Kr Xe Ar Ne') then arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesNe_NIST.tab', 'linesAr_NIST.tab', 'linesKr_NIST.tab', 'linesXeI_NIST_deimos.tab'],$
                weight:[1.0, 1.0, 1.0, 1.0, 1.0],label:['NeI','ArI', 'KrI', 'XeI']}
            if (lamp_id eq 'Kr Xe Cd Zn') then arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesCd_NIST_deimos.tab', 'linesZn_NIST_deimos.tab',$
			 	'linesKr_NIST.tab', 'linesXeI_NIST_deimos.tab'],weight:[1.0, 1.0, 1.0, 1.0, 1.0],label:['CdI', 'ZnI', 'KrI', 'XeI']}
            if (lamp_id eq 'Kr Xe Cd Zn Kr Xe Ar Ne') then arc_list_config = {linetab_list:getenv('MOSIFU_PIPELINE_PATH')+'calib_mosifu/linelists/'+['linesCd_NIST_deimos.tab', 'linesZn_NIST_deimos.tab',$
                'linesKr_NIST.tab', 'linesXeI_NIST_deimos.tab', 'linesNe_NIST.tab', 'linesAr_NIST.tab'],weight:[1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],label:['CdI', 'ZnI', 'KrI', 'XeI', 'NeI', 'ArI']}
        endelse

        status=crea_disper_generic_slit(tmpdir_cur+(keyword_set(oh)? 'obj_ff.fits' : 'arc_ff.fits'),0,$
            wl_ini,7,ndeg,arc_list_config=arc_list_config,$
            smy=10,ndegy=y_ndeg,dist_map=dist_map,use_oh=oh,debug=debug,plot=plot,fast=1,disper_table=disper_table,/fullslit,/high_curvature)

        writefits,tmpdir_cur+'disper_table.fits',0
        mwrfits,disper_table,tmpdir_cur+'disper_table.fits'
    endif else begin
        disper_table=mrdfits(tmpdir_cur+'disper_table.fits',1,/silent)
    endelse

    if (det_type eq 1) then begin
        disper_table_red = disper_table
        im_red = mrdfits(tmpdir_cur+'obj_ff.fits', 1)
        dist_map_red = dist_map
    endif else begin
        disper_table_blue = disper_table
        im_blue = mrdfits(tmpdir_cur+'obj_ff.fits', 1)
        dist_map_blue = dist_map
    endelse
endfor

    if(n_elements(im_red) ne 0) then begin
		s_red= size(im_red)
	    nx_red = s_red[1]
    	ny_red = s_red[2]
	    gg_red=where(finite(disper_table_red.y_mask) eq 1)
	    wl_first_red=poly(dindgen(nx_red),disper_table_red[gg_red[0]].wl_sol)
    	dwl_red = abs(wl_first_red[nx_red-1]-wl_first_red[0])/(nx_red-1d)
	    l_par_red = {grism:'', filter:'', $
    	           wl0:wl_first_red[0], dwl:dwl_red, npix:nx_red,$
        	       wl_min:wl_first_red[0], wl_max:wl_first_red[nx_red-1]}
	endif else dwl_red=-1
	
    if(n_elements(im_blue) ne 0)then begin
 		s_blue=size(im_blue)
    	nx_blue = s_blue[1]
    	ny_blue = s_blue[2]
    	gg_blue=where(finite(disper_table_blue.y_mask) eq 1)
    	wl_first_blue=poly(dindgen(nx_blue),disper_table_blue[gg_blue[0]].wl_sol)
    	dwl_blue = abs(wl_first_blue[nx_blue-1]-wl_first_blue[0])/(nx_blue-1d)
    	l_par_blue = {grism:'', filter:'', $
    	           wl0:wl_first_blue[0], dwl:dwl_blue, npix:nx_blue,$
	               wl_min:wl_first_blue[0], wl_max:wl_first_blue[nx_blue-1]}
	endif else dwl_blue=-1

    print, 'dwl_red = ', dwl_red
    print, 'dwl_blue = ', dwl_blue

	if((dwl_red ne -1) and (dwl_blue ne -1)) then begin
	    if (dwl_red ge dwl_blue) then dwl = dwl_blue else dwl = dwl_red
	
	    l_par_red.wl0 = wl_first_blue[0] + dwl*ceil((wl_first_red[0]-wl_first_blue[0])/dwl)
	    l_par_red.npix = fix((wl_first_red[nx_red-1]-l_par_red.wl0)/dwl)
	    l_par_red.wl_max = l_par_red.wl0 + dwl*(l_par_red.npix)
		l_par_red.dwl = dwl
		l_par_blue.dwl = dwl
	endif
		
for l=0, n_elements(vidinp)-1 do begin
    det_type = (where(vidinp[l] eq [10, 12, 14, 16]) ne -1); 1 -- red, 0 -- blue
    tmpdir_cur = tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'

    if (det_type eq 1) then begin
        l_par = l_par_red
        disper_table = disper_table_red
        dist_map = dist_map_red
        print, '###############################################'
        print, 'RED'
        print, '###############################################'
    endif else begin
        l_par = l_par_blue
        disper_table = disper_table_blue
        dist_map=dist_map_blue
        print, '###############################################'
        print, 'BLUE'
        print, '###############################################'
    endelse

	obs_type_arr=['obj','arc']
    if(n_stdstar gt 0) then obs_type_arr=[obs_type_arr,'stdstar']
	for i=0, n_elements(obs_type_arr)-1 do begin
        hh_pri=headfits(tmpdir_cur+obs_type_arr[i]+'_ff.fits')
		if(obs_type_arr[i] eq 'obj') then hh_raw=headfits(rawfilename[0], ext=0)
        if(obs_type_arr[i] eq 'arc') then hh_raw=headfits(tmpdir_cur+'arc_ff.fits', ext=1)
        if(obs_type_arr[i] eq 'stdstar') then hh_raw=headfits(rawstdstar[0], ext=0)
        im=mrdfits(tmpdir_cur+obs_type_arr[i]+'_ff.fits',1,hh)
		im_lin=linearisation_simple(im,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir_cur,lin_param=l_par)		
		
		sxaddpar, hh_out, 'MOUNT_EL', sxpar(hh_raw, 'EL')
        writefits,tmpdir_cur+obs_type_arr[i]+'_lin.fits',0
        mwrfits,im_lin,tmpdir_cur+obs_type_arr[i]+'_lin.fits',hh_out
		
		if(obs_type_arr[i] eq 'arc') then continue
	
	    if(skysubalgorithm eq 2) then begin ;; simple sky subtraction using skyreg
	        ;; simple sky subtraction
	        hh_pri=headfits(tmpdir_cur+obs_type_arr[i]+'_lin.fits')
	        obj_lin=mrdfits(tmpdir_cur+obs_type_arr[i]+'_lin.fits',1,hlin)
	        nx_lin=sxpar(hlin,'NAXIS1')
			ny_lin=sxpar(hlin,'NAXIS2')
	        parse_spechdr,hlin,wl=wl

	        if(n_elements(skyreg) eq 0) then begin
	            p_obj=median(obj_lin[Nx_lin*0.4:Nx_lin*0.6,*],dim=1)
	            p_zero=where(p_obj eq 0,cp_zero)
	            if(cp_zero gt 0) then p_obj[p_zero]=!values.f_nan
	            outer_p_obj=median([p_obj[0:0.25*ny_lin],p_obj[0.75*ny_lin:*]])
	            min_p_obj=min(p_obj,/nan)
	            nbin_p=11
	            h_prof=histogram(p_obj,min=min_p_obj,max=2*outer_p_obj-min_p_obj,nbin=nbin_p)
	            p_fit=mpfitpeak(min_p_obj+dindgen(nbin_p)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.0),h_prof,g_coeff)
	
	            skyreg=where(p_obj gt min_p_obj and $
	                          p_obj le min_p_obj+(max(where(p_fit ge p_fit[0]))+1)*(2*outer_p_obj-2*min_p_obj)/(nbin_p-1.), csky)
	        endif
	        resistant_mean,obj_lin[*,skyreg],3.0,skyvec,dim=2
	        writefits,tmpdir_cur+obs_type_arr[i]+'-sky_lin.fits',0;,hh_pri
	        mwrfits,obj_lin-rebin(skyvec,nx_lin,sxpar(hlin,'NAXIS2')),tmpdir_cur+obs_type_arr[i]+'-sky_lin.fits',hlin
   		 endif
	endfor

    flat_norm=mrdfits(tmpdir_cur+'flat_norm.fits',1,hh,/silent)
    flat_lin=linearisation_simple(flat_norm,disper_table,hdrin=hh,hdrout=hh_out,dist_map=dist_map,wdir=tmpdir_cur,skyline_flux_data_arr=skyline_flux_data_arr,lin_param=l_par,flatcorr_emline=flatcorr_emline)
    flatflux=median(flat_lin[*,sxpar(hh,'NAXIS2')/2-20:sxpar(hh,'NAXIS2')/2+20],dim=2)
    gfl=where(finite(flatflux) eq 1,cgfl,compl=bfl,ncompl=cbfl)
    if(cbfl gt 0 and cgfl gt 10) then flatflux=interpol(flatflux[gfl],float(gfl),findgen(cbfl+cgfl))
    flatflux[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]=(poly_smooth(flatflux,(cbfl+cgfl)/20.0,deg=2))[0.05*(cbfl+cgfl):0.95*(cbfl+cgfl)]

    hh_pri=headfits(tmpdir_cur+'obj_lin.fits')
    obj_lin=mrdfits(tmpdir_cur+'obj_lin.fits',1,hlin_o,/silent)
    err_obj_lin=sqrt((sxpar(hlin_o,'RDNOISE')+obj_lin*flat_lin)>0)/flat_lin
    writefits,tmpdir_cur+'err_obj_lin.fits',0,hh_pri
    mwrfits,err_obj_lin,tmpdir_cur+'err_obj_lin.fits',hlin_o

    hh_pri=headfits(tmpdir_cur+'stdstar_lin.fits')
    stdstar_lin=mrdfits(tmpdir_cur+'stdstar_lin.fits',1,hlin_s,/silent)
    err_stdstar_lin=sqrt((sxpar(hlin_s,'RDNOISE')+stdstar_lin*flat_lin)>0)/flat_lin
    writefits,tmpdir_cur+'err_stdstar_lin.fits',0,hh_pri
    mwrfits,err_obj_lin,tmpdir_cur+'err_stdstar_lin.fits',hlin_s

    if(n_stdstar gt 0 and n_elements(stddata) eq 1 and keyword_set(abscal)) then begin
        el_s=float(sxpar(hlin_s,'MOUNT_EL',count=n_el_s))
        el_o=float(sxpar(hlin_o,'MOUNT_EL',count=n_el_o))
        print,'Computing a throughput curve using a standard star spectrum'
        fluxcal_data=mos_measure_throughput(tmpdir_cur+'stdstar_lin.fits',$;'stdstar-sky_lin.fits',stddata,$
            stddata,mirr=(3*sqrt(3)*90^2/2)*36,/hdrext,n_wl_seg=((det_type eq 1) ? 3 : 10),$
            flatflux=flatflux,blue_thr=wl0_lin,expcorr=1.0/n_stdstar,/lintails,telluric=telluric,Vmag_tell=Vmag_tell,/plot)

       parse_spechdr,hlin_o,wl=wllin
       ext_vec_o=calc_extinction(wllin,90.0-el_o)
       ext_vec_s=calc_extinction(wllin,90.0-el_s)
       sxaddpar,hlin,'BUNIT','1.0E-17 erg/cm^2/s/A'
       obj_lin_abs=obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_o,sxpar(hlin_o,'NAXIS1'),sxpar(hlin_o,'NAXIS2'))*1e17
       writefits,tmpdir_cur+'obj_lin_abs.fits',0
       mwrfits,obj_lin_abs,tmpdir_cur+'obj_lin_abs.fits',hlin_o

       err_obj_abs=err_obj_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_o,sxpar(hlin_o,'NAXIS1'),sxpar(hlin_o,'NAXIS2'))*1e17
       writefits,tmpdir_cur+'err_obj_abs.fits',0
       mwrfits,err_obj_abs,tmpdir_cur+'err_obj_abs.fits',hlin_o

       err_stdstar_abs=err_stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_s,sxpar(hlin_s,'NAXIS1'),sxpar(hlin_s,'NAXIS2'))*1e17
       writefits,tmpdir_cur+'err_stdstar_abs.fits',0
       mwrfits,err_obj_abs,tmpdir_cur+'err_stdstar_abs.fits',hlin_s

       if(file_test(tmpdir_cur+'obj-sky_lin.fits')) then begin
           obj_sky=mrdfits(tmpdir_cur+'obj-sky_lin.fits',1,hsky,/silent)
           sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
           obj_abs=obj_sky*rebin(fluxcal_data.c_flux_erg/ext_vec_o,sxpar(hlin_o,'NAXIS1'),sxpar(hlin_o,'NAXIS2'))*1e17
           writefits,tmpdir_cur+'obj_abs.fits',0
           mwrfits,obj_abs,tmpdir_cur+'obj_abs.fits',hsky
       endif

       if(file_test(tmpdir_cur+'stdstar_lin.fits')) then begin;-sky
           stdstar_lin=mrdfits(tmpdir_cur+'stdstar_lin.fits',1,hsky,/silent);-sky
           sxaddpar,hsky,'BUNIT','1.0E-17 erg/cm^2/s/A'
           stdstar_abs=stdstar_lin*rebin(fluxcal_data.c_flux_erg/ext_vec_s,sxpar(hlin_s,'NAXIS1'),sxpar(hlin_s,'NAXIS2'))*1e17
           writefits,tmpdir_cur+'stdstar_abs.fits',0
           mwrfits,stdstar_abs,tmpdir_cur+'stdstar_abs.fits',hsky
       endif
    endif

endfor

	if(n_elements(vidinp) eq 2) then begin
		obs_type_arr=['obj', 'stdstar']
		for i=0, n_elements(obs_type_arr)-1 do begin
		    for l=0, n_elements(vidinp)-1 do begin
				det_type = (where(vidinp[l] eq [10, 12, 14, 16]) ne -1); 1 -- red, 0 -- blue
		        if (det_type eq 1) then begin
		            im_red = mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'+obs_type_arr[i]+'_abs.fits', 1, hred)
                    err_im_red = mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/err_'+obs_type_arr[i]+'_abs.fits', 1, hred_e)
                    im_red_lin = mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'+obs_type_arr[i]+'_lin.fits', 1, hredl)
                    err_im_red_lin = mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/err_'+obs_type_arr[i]+'_lin.fits', 1, hredl_e)
		        endif else begin
		            im_blue =mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'+obs_type_arr[i]+'_abs.fits', 1, hblue)
                    err_im_blue =mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/err_'+obs_type_arr[i]+'_abs.fits', 1, hblue_e)
                    im_blue_lin =mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/'+obs_type_arr[i]+'_lin.fits', 1, hbluel)
                    err_im_blue_lin =mrdfits(tmpdir+'/vidinp_'+strtrim(string(vidinp[l]), 2)+'/err_'+obs_type_arr[i]+'_lin.fits', 1, hbluel_e)
		        endelse
		    endfor
		
		    parse_spechdr, hred, wl=wl_red
		    parse_spechdr, hblue, wl=wl_blue
		
		    print, 'red wl: from ', min(wl_red), ' to ', max(wl_red)
		    print, 'blue wl: from ', min(wl_blue), ' to ', max(wl_blue)
		    print, 'red dwl: ', sxpar(hred, 'cdelt1')
		    print, 'blue dwl: ', sxpar(hblue, 'cdelt1')
		
		    if(sxpar(hred, 'cdelt1')/sxpar(hblue, 'cdelt1') ne 1.) then message, 'dwl_red NE dwl_blue. cannot continue'
		    dwl = sxpar(hblue, 'cdelt1')
		
		    pp_red = linslit_find_fit_sources(im_red[0:200, *], err_im_red[0:200, *], nsig=10)
		    pp_blue = linslit_find_fit_sources(im_blue[3800:4000, *], err_im_blue[3800:4000, *], nsig=10)
		    max_red = max(pp_red.norm_bestfit, midx_red)
		    max_blue = max(pp_blue.norm_bestfit, midx_blue)
		
		    im_red = shift(im_red, 0, -(pp_red[midx_red].x-pp_blue[midx_blue].x-1))
			err_im_red = shift(err_im_red, 0, -(pp_red[midx_red].x-pp_blue[midx_blue].x-1))

            im_red_lin = shift(im_red_lin, 0, -(pp_red[midx_red].x-pp_blue[midx_blue].x-1))
            err_im_red_lin = shift(err_im_red_lin, 0, -(pp_red[midx_red].x-pp_blue[midx_blue].x-1))

			print, 'yshift = ', -(pp_red[midx_red].x-pp_blue[midx_blue].x-1)
;stop		
		    wl_gap = min(wl_red) - max(wl_blue) - dwl
		    pix_gap = wl_gap/dwl
		    print, 'pix_gap = ', pix_gap
		    pix_gap = fix(pix_gap)
		
		    if (wl_gap ge 0.) then begin
		        gap_arr = findgen(pix_gap, ny_red)*!values.f_nan
		        im_merged = [im_blue, gap_arr, im_red]
                err_im_merged = [err_im_blue, gap_arr, err_im_red]
                im_merged_lin = [im_blue_lin, gap_arr, im_red_lin]
                err_im_merged_lin = [err_im_blue_lin, gap_arr, err_im_red_lin]
		    endif else begin
		        message, 'WL solutions overlap. Something wrong. Check arc.ps.'
		    endelse
		
		    writefits, tmpdir+obs_type_arr[i]+'-sky_abs_merged.fits', 0
		    mwrfits, im_merged, tmpdir+obs_type_arr[i]+'-sky_abs_merged.fits', hblue

            writefits, tmpdir+'err_'+obs_type_arr[i]+'-sky_abs_merged.fits', 0
            mwrfits, err_im_merged, tmpdir+'err_'+obs_type_arr[i]+'-sky_abs_merged.fits', hblue_e



            writefits, tmpdir+obs_type_arr[i]+'_lin_merged.fits', 0
            mwrfits, im_merged_lin, tmpdir+obs_type_arr[i]+'_lin_merged.fits', hbluel

            writefits, tmpdir+'err_'+obs_type_arr[i]+'_lin_merged.fits', 0
            mwrfits, err_im_merged_lin, tmpdir+'err_'+obs_type_arr[i]+'_lin_merged.fits', hbluel_e
		endfor
	endif

;stop
end
