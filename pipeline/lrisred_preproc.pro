function clean_overscan_vector,overscan,w,nsig=nsig,rdnoise=rdnoise
    if(n_params() eq 1) then w=5
    if(w lt 3) then w=3
    if(n_elements(rdnoise) ne 1) then rdnoise=4.0
    if(n_elements(nsig) ne 1) then nsig=3.0

    m_overscan=median(overscan,w)
    bvec=where(abs(overscan-m_overscan) gt rdnoise*nsig, cbvec, compl=gvec, ncompl=cgvec)
    clean_overscan=overscan
    if(cbvec gt 0 and cgvec gt 0) then clean_overscan[bvec]=interpol(overscan[gvec],gvec,bvec)
    return, clean_overscan
end

function lrisred_preproc,image,header,gain=gain,median=median,sm_w=sm_w,noclean=noclean

    if(n_elements(gain) ne 2) then gain=[1.98,2.12687] ;; determined empirically from flat field frames taken on 20040317
    if(n_elements(sm_w) ne 1) then sm_w=0
    imtype=strcompress(sxpar(header,'IMTYPE'),/remove_all)
    if(imtype ne 'TWOAMPTOP') then message,'Only the TWOAMPTOP mode is supported!'

    namps=2
    prepix=sxpar(header,'PREPIX')
    postpix=sxpar(header,'POSTPIX')
    nx_orig=sxpar(header,'NAXIS1')
    ny_orig=sxpar(header,'NAXIS2')
    sxaddpar,header,'BZERO',0.0
    nx=nx_orig-namps*(prepix+postpix)
    ny=ny_orig

    image_new=fltarr(nx,ny)
    for i=0,namps-1 do begin
        img_xreg=prepix*2+lindgen(nx/namps)+nx/namps*i
        overscan_xreg=prepix*2+nx+postpix*i+lindgen(postpix)

        if(keyword_set(median)) then $
            overscan_x_vec=median(image[overscan_xreg,*],dim=1) $
        else begin
            resistant_mean,image[overscan_xreg,*],3.0,overscan_x_vec,dim=1
            overscan_x_vec=reform(overscan_x_vec,n_elements(overscan_x_vec))
        endelse
        if(~keyword_set(noclean)) then overscan_x_vec=clean_overscan_vector(overscan_x_vec,5)
        if(sm_w gt 0) then overscan_x_vec=smooth(overscan_x_vec,sm_w,/nan)
        image_new[img_xreg-prepix*2,*]=(image[img_xreg,*]-((fltarr(n_elements(img_xreg))+1) # overscan_x_vec))*gain[i]
    endfor

    if(~keyword_set(noclean)) then begin
        badcol_x=[945,997,1001]-1 ;;[957,961]-1
        badcol_y1=[0,0,0]
        badcol_y2=[723,1488,1491]-1
        for i=0,n_elements(badcol_x)-1 do image_new[badcol_x[i]-2*prepix,badcol_y1[i]:badcol_y2[i]]=!values.f_nan
    endif

    ;if(~keyword_set(longslit)) then begin
    ;    image_new=rotate(image_new,1)
    ;endif
    return,image_new
end
