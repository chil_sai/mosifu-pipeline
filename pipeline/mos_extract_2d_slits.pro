pro mos_extract_2d_slits,image_type,$
    wdir=wdir,mask=mask,dist_map=dist_map,dithpos=dithpos,$
    unproc=unproc,nflat=nflat,out_img_type=out_img_type, noflat=noflat, pady=pady

;if(n_params() eq 1) then image_type='obj'
if(keyword_set(dithpos) ne 1) then dithpos=0.0
if(n_elements(pady) ne 1) then pady=0

if(keyword_set(nflat)) then begin
    image_type='flatn'
    img=mrdfits(wdir+'flat_norm.fits',1,ha)
endif else begin
    img=(keyword_set(unproc))? mrdfits(wdir+image_type[0]+'_dark.fits',1,ha) : mrdfits(wdir+image_type[0]+'_ff.fits',1,ha)
endelse
nx=sxpar(ha,'NAXIS1')
ny=sxpar(ha,'NAXIS2')

n_slits=n_elements(mask)
if n_slits eq 1 and not tag_exist(mask, 'slit') then begin
  good_sides[0]=0
  n_slits=0
endif

slit_reg=get_slit_region(mask,nx=nx,ny=ny,dist_map=dist_map,slit_trace=slit_trace, pady=pady) ;, obj_trace=obj_trace)
obj_trace=slit_trace

if(n_elements(out_img_type) ne 1) then out_img_type=image_type[0]
f_out=wdir+out_img_type+'_slits.fits'

h=ha
sxdelpar,h,'NAXIS1'
sxdelpar,h,'NAXIS2'
sxaddpar,h,'NAXIS',0

writefits,f_out,0,h

for i=0,n_slits-1 do begin
    if((i gt 0) and (n_elements(image_type) eq n_slits)) then begin
        if(image_type[i] ne image_type[i-1]) then begin
            img=(keyword_set(unproc))? mrdfits(wdir+image_type[i]+'_dark.fits',1) : mrdfits(wdir+image_type[i]+'_ff.fits',1,ha)
        endif
    endif

    ny_img=n_elements(img[0,*])

;    print,'Processing ',i+1,'/',n_slits
    cur_y_tr=slit_trace[i].y_trace
    cur_objy_tr=obj_trace[i].y_trace
    slit_h_cur=fix((slit_reg[1,i]-slit_reg[0,i])/2.0)
    slit_h_cur=slit_h_cur+pady
    ymean=fix(cur_y_tr[nx/2-1])
    yobjmean=mean(cur_objy_tr-cur_y_tr)
    print,'Processing ',i+1,'/',n_slits,' ymean=',ymean
    yhsize=max(ceil(abs(cur_y_tr-ymean)))+slit_h_cur
    slit_img=fltarr(nx,2*yhsize+1)+!values.f_nan
    y_off = slit_reg[0,i]
    ;y_off = ymean-yhsize

    for x=0,nx-1 do begin
        y_cur=cur_y_tr[x]
        ymin_cur=(y_cur-slit_h_cur) ;;; > 0
        ymax_cur=(y_cur+slit_h_cur) ;;; < (ny-1)
        dymin = (ymin_cur-y_off lt 0)?  -(ymin_cur-y_off) : 0.0
        dymax = (ymax_cur ge ny)? ymax_cur-ny-1 : 0.0
        
        slit_img_y0=fix(1e-5+y_cur-y_off-slit_h_cur+dymin)
        slit_img_y1=fix(1e-5+y_cur-y_off+slit_h_cur-dymax)
        img_y0=fix(1e-5+ymin_cur+dymin)
        img_y1=fix(1e-5+ymax_cur-dymax)
        ;if (slit_img_y1-slit_img_y0) ne (img_y1- img_y0) then stop
        safe_array_bounds,2*yhsize+1,ny_img,slit_img_y0,slit_img_y1,img_y0,img_y1

        if((slit_img_y0 le slit_img_y1) and (img_y0 le img_y1) and $
           (slit_img_y1 lt 2*yhsize+1) and (img_y1 lt ny_img)) then slit_img[x,slit_img_y0:slit_img_y1]=img[x,img_y0:img_y1]
    endfor
    mkhdr,hdr_out,slit_img,/image
    maskentry=mask[i]
    sxaddpar,hdr_out,'EXTNAME',strupcase(maskentry.type)+string(i+1,format='(i3.3)')
    sxaddpar,hdr_out,'YOFFSET',y_off 
    sxaddpar,hdr_out,'AIRMASS',sxpar(h,'AIRMASS')
;    sxaddpar,hdr_out,'SIDE',((i lt n_slits) and (n_slits gt 0))? 'A' : 'B'
    sxaddpar,hdr_out,'SLITID',maskentry.slit,' slit id'
    sxaddpar,hdr_out,'SLITRA',maskentry.ra,' ra target coordinate'
    sxaddpar,hdr_out,'SLITDEC',maskentry.dec,' dec target coordinate'
;    sxaddpar,hdr_out,'SLITX',maskentry.x,' x slit coordinate'
    sxaddpar,hdr_out,'SLITYTAR',maskentry.y,'target y mask coord (mm)'
;   quick fix for tilted slits
    sxaddpar,hdr_out,'SLITX',total(maskentry.bbox[[0,2,4,6]])/4.0,' x slit coord (mm)'
    sxaddpar,hdr_out,'SLITY',total(maskentry.bbox[[1,3,5,7]])/4.0,' y slit coord (m)'
    sxaddpar,hdr_out,'SLITTARG',maskentry.target,' target'
    sxaddpar,hdr_out,'SLITOBJ',maskentry.object,' object'
    sxaddpar,hdr_out,'SLITTYPE',maskentry.type,' type'
    sxaddpar,hdr_out,'SLITHEIG',(maskentry.bbox[5]-maskentry.bbox[1]),' slit height (mm)'
    sxaddpar,hdr_out,'SLITWIDT',maskentry.width,' slit width (mm)'
    sxaddpar,hdr_out,'SLITOFFS',maskentry.offset,' slit offset'
    sxaddpar,hdr_out,'SLITTHET',maskentry.theta,' slit PA (theta)'
    target_offset=(maskentry.y-(maskentry.bbox[5]+maskentry.bbox[1])/2.0)*5.98802
    ccdscl=0.24436 ;0.24
    slitypix=(dithpos+target_offset)/ccdscl+yhsize ;-1.0 ;;WHY -1?
    print, yobjmean+ymean-y_off, slitypix
    sxaddpar,hdr_out,'SLITYPIX',slitypix,' target pos in pixels from bottom of slit'
    ;sxaddpar,hdr_out,'SLITYPIX',((maskentry.y-maskentry.bbox[1])*5.98802)/0.24,' target pos in pixels from bottom of slit'
    sxaddpar,hdr_out,'MASKID',maskentry.mask_id,' Mask ID from the database'
    sxaddpar,hdr_out,'MASKNAME',maskentry.mask_name,' Mask name from the database'
    sxaddpar,hdr_out,'MASKRA',maskentry.mask_ra,' RA of the field center'
    sxaddpar,hdr_out,'MASKDEC',maskentry.mask_dec,' Dec of the field center'
    sxaddpar,hdr_out,'MASKPA',maskentry.mask_pa,' PA of the mask'
    sxaddpar,hdr_out,'TILTEDSL',((abs(maskentry.bbox[4]-maskentry.bbox[0]) gt 1.5*maskentry.width)? 1 : 0),' tilted slit 1=Yes/0=No'
    mwrfits,slit_img,f_out,hdr_out,/silent
endfor

end
